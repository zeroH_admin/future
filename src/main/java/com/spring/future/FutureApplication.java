package com.spring.future;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;

@SpringBootApplication
@EnableCaching
public class FutureApplication {

	private static ApplicationContext applicationContext;


	public static void main(String[] args) {
		final ApplicationContext applicationContext = SpringApplication.run(FutureApplication.class, args);
		FutureApplication.applicationContext = applicationContext;
		//清空redis
		RedisTemplate redisTemplate = (RedisTemplate) applicationContext.getBean("redisTemplate");
		redisTemplate.getConnectionFactory().getConnection().flushDb();
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}
}
