package com.spring.future.common.domain;


import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.mybatis.annotation.GeneratedValue;
import com.spring.future.common.mybatis.annotation.GenerationType;
import com.spring.future.common.utils.StringUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by liuj on 2016-07-25.
 */
public class IdEntity<T> implements Serializable {
    private static final long serialVersionUID = -864662154371775680L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected String id;
    @Transient
    protected Map<String,Object> extendMap = Maps.newLinkedHashMap();
    @Transient
    private List<String> actionList = Lists.newArrayList();
    @Transient
    private String currentAction;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean isCreate() {
        return StringUtils.isBlank(id);
    }

    public Map<String, Object> getExtendMap() {
        return extendMap;
    }

    public void setExtendMap(Map<String, Object> extendMap) {
        this.extendMap = extendMap;
    }

    public List<String> getActionList() {
        return actionList;
    }

    public void setActionList(List<String> actionList) {
        this.actionList = actionList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((StringUtils.isEmpty(id)) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IdEntity other = (IdEntity) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public String getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(String currentAction) {
        this.currentAction = currentAction;
    }
}
