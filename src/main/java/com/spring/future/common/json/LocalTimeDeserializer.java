package com.spring.future.common.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.spring.future.common.utils.StringUtils;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by wzm on2016-09-10.
 */
public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {
    @Override
    public LocalTime deserialize(JsonParser p, DeserializationContext ctx) throws IOException, JsonProcessingException {
        LocalTime localTime = null;
        if(StringUtils.isNotBlank(p.getText())) {
            String pattern = "HH:mm:ss";
            if(p.getText().length() != pattern.length()) {
                pattern = "HH:mm";
            }
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
            localTime = LocalTime.parse(p.getText(),formatter);
        }
        return localTime;
    }
}
