package com.spring.future.common.config;

import com.google.common.collect.Maps;
import com.spring.future.common.service.ServiceException;
import com.spring.future.common.utils.ObjectMapperUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by admin on 2016/9/8.
 */
@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(value = ServiceException.class)
    @ResponseBody
    public String defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
        Map<String,Object> map = Maps.newHashMap();
        map.put("code",500);
        map.put("message",e.getMessage());
        return ObjectMapperUtils.writeValueAsString(map);
    }

}
