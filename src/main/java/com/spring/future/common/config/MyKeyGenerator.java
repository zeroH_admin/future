package com.spring.future.common.config;

import com.google.common.collect.Lists;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.common.utils.ThreadLocalContext;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

/**
 * Created by liuj on 2016/12/1.
 */
public class MyKeyGenerator implements KeyGenerator {
    @Override
    public Object generate(Object target, Method method, Object... params) {
        Object[] paramList = params;
        return generateKey(paramList);
    }

    /**
     * Generate a key based on the specified parameters.
     */
    public static Object generateKey(Object... params) {
        if (params.length == 0) {
            return SimpleKey.EMPTY;
        }
        if (params.length == 1) {
            Object param = params[0];
            if (param != null && !param.getClass().isArray()) {
                return param;
            }
        }
        return new SimpleKey(params);
    }
}
