package com.spring.future.common.config;

import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.utils.Encodes;
import com.spring.future.common.utils.ExcelUtils;
import org.springframework.web.servlet.view.AbstractView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public class ExcelView extends AbstractView {
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		SimpleExcelBook simpleExcelBook= (SimpleExcelBook) model.get("simpleExcelBook");
		ExcelUtils.doWrite(simpleExcelBook.getWorkbook(),simpleExcelBook.getSimpleExcelSheets());
		response.setContentType("application/octet-stream; charset=utf-8");
		response.setHeader("Content-Disposition", "attachment; filename=" + Encodes.urlEncode(simpleExcelBook.getName()));
		simpleExcelBook.getWorkbook().write(response.getOutputStream());
	}
}
