package com.spring.future.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.http.HttpSessionStrategy;

/**
 * Created by liuj on 2016-07-11.
 */

@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1800)
public class HttpSessionConfig {
    @Bean
    public HttpSessionStrategy sessionStrategy() {
        return new CookieAndHeaderHttpSessionStrategy();
    }
}
