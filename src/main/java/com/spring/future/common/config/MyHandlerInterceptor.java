package com.spring.future.common.config;

import com.spring.future.FutureApplication;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.security.CustomUserDetails;
import com.spring.future.modules.ih.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by liuj on 2016-08-20.
 */
public class MyHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Account account= null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal().getClass().isAssignableFrom(CustomUserDetails.class)) {
            account =  ((CustomUserDetails) authentication.getPrincipal()).getAccount();
        }
        AccountService accountService = FutureApplication.getApplicationContext().getBean(AccountService.class);
        ThreadLocalContext.get().setAccount(account);
        if(account!=null){
            account = accountService.findOne(account.getId());
            ThreadLocalContext.get().setAccount(account);

            //加入公司后信息完善的提交
            if(request.getRequestURI().contains("competer-info") || request.getRequestURI().contains("getMobileCode")|| request.getRequestURI().contains("competerInfo") || request.getRequestURI().contains("checkMobileCode")){
                return true;
            }
            //账户未审核
            if(account!=null && account.getLocked()){
                if(Const.COMPANY.equals(account.getType())){
                    if(StringUtils.isNoneBlank(account.getCompanyId())){
                        //判断信息是否完善,信息不完善重新跳转到信息完善页面
                        if(StringUtils.isEmpty(account.getMobilePhone())){
                            response.sendRedirect("/register/competer-info");
                            return false;
                        }
                        //信息已完善，请求url判断跳转
                        if (!request.getRequestURI().contains("auditNotice") && request.getRequestURI().contains("-index")){
                            response.sendRedirect("/auditNotice");
                            return false;
                        }
                    }
                }else{
                    if(StringUtils.isNoneBlank(account.getUserId())){
                        if (!request.getRequestURI().contains("auditNotice") && request.getRequestURI().contains("-index")){
                            response.sendRedirect("/auditNotice");
                            return false;
                        }
                    }

                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        ThreadLocalContext.get().remove();
    }
}