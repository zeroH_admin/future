package com.spring.future.common.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.future.common.utils.ObjectMapperUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by liuj on 2017/2/20.
 */
@Configuration
public class JacksonConfig {
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = ObjectMapperUtils.getObjectMapper();
        return objectMapper;
    }
}
