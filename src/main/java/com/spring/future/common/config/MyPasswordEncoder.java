package com.spring.future.common.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by liuj on 2016/11/19.
 */
public class MyPasswordEncoder  extends BCryptPasswordEncoder {

    //自动登陆，只要OPENID在系统已经绑定就可以，无需再验证密码
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return true;
    }
}
