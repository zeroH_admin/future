package com.spring.future.common.config;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by liuj on 2016-08-09.
 */
@Configuration
public class MybatisConfig {

    @Bean
    public DataSource dataSource() throws Exception {
        Properties props = new Properties();
        props.put("driverClassName", "com.mysql.jdbc.Driver");
//        props.put("url", "jdbc:mysql://rm-uf60g1tbliz47c39oo.mysql.rds.aliyuncs.com:3306/future_test?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&useSSL=false");
        props.put("url", "jdbc:mysql://rm-uf60g1tbliz47c39oo.mysql.rds.aliyuncs.com:3306/future?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&useSSL=false");
        props.put("username", "internadmin");
        props.put("password", "admin@123456");
        return DruidDataSourceFactory.createDataSource(props);
    }
}
