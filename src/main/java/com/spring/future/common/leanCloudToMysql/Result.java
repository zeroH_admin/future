package com.spring.future.common.leanCloudToMysql;

import com.google.common.collect.Lists;
import com.spring.future.modules.ih.domain.ResumeCloud;
import com.spring.future.modules.ih.domain.UserCloud;
import com.spring.future.modules.ih.model.CompanyCloud;

import java.util.List;

/**
 * Created by zh on 2017/5/4.
 */
public class Result {
    private List<ResumeCloud> results = Lists.newArrayList();
//    private List<CompanyCloud> results = Lists.newArrayList();

    public List<ResumeCloud> getResults() {
        return results;
    }

    public void setResults(List<ResumeCloud> results) {
        this.results = results;
    }
}
