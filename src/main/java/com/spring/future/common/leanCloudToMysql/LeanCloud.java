package com.spring.future.common.leanCloudToMysql;

import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.AVUser;
import com.spring.future.common.utils.Collections3;
import com.spring.future.common.utils.FileUtils;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.modules.ih.domain.UserCloud;
import com.spring.future.modules.ih.mapper.UserCloudMapper;
import com.spring.future.modules.ih.service.UserCloudService;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/5/3.
 */
public class LeanCloud {

    private static String userPath = "D:\\workspaces\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\_User.json";
    private static String resumePath = "D:\\workspaces\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\Resume.json";
    private static String imgPath = "D:\\workspaces\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\img.json";

    public static void main(String[] args) throws Exception{

        String imgStr = ReadFile(imgPath);

    }

    public static String ReadFile(String path) {
        File file = new File(path);
        BufferedReader reader = null;
        String laststr = "";
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                laststr = laststr + tempString;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
        return laststr;
    }


}
