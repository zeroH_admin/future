package com.spring.future.common.leanCloudToMysql;

import com.google.common.collect.Lists;
import com.spring.future.modules.ih.domain.ResumeCloud;
import com.spring.future.modules.ih.domain.UserCloud;
import com.spring.future.modules.ih.model.JobPositionCloud;

import java.util.List;

/**
 * Created by zh on 2017/5/4.
 */
public class UserResult {
    private List<UserCloud> results = Lists.newArrayList();

    public List<UserCloud> getResults() {
        return results;
    }

    public void setResults(List<UserCloud> results) {
        this.results = results;
    }
}
