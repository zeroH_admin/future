package com.spring.future.common.validateCodeUtils;

import com.google.common.collect.Maps;
import com.spring.future.common.utils.HtmlGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by zh on 2017/6/8.
 */
@Component
public class EmailUtils {
    @Autowired
    private JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String emailUserName;
    @Value("${root-path}")
    private String rootPath;


    public void send(String templete,String emailTitle,String email,Map<String,Object> map){
        try {
            String html = HtmlGenerator.generate(rootPath+"template/", templete, map);

            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            String nick="";
            try {
                nick=javax.mail.internet.MimeUtility.encodeText("IH招聘");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            helper.setFrom(new InternetAddress(nick+" <"+emailUserName+">"));
            helper.setTo(email);
            helper.setSubject(emailTitle);
            helper.setText(html, true);
            FileSystemResource fileSystemResource = new FileSystemResource(new File(rootPath+"template/bac.png"));
            helper.addInline("weixin", fileSystemResource);
            mailSender.send(mimeMessage);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
