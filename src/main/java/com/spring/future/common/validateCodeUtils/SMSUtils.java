package com.spring.future.common.validateCodeUtils;

import com.google.common.collect.Maps;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

import java.util.Map;

/**
 * Created by zh on 2017/4/21.
 */
public class SMSUtils {

    public static void main(String[] args) {
//        sendMobileCode("17621336004","456456");

        Map<String,String> map = Maps.newHashMap();
        map.put("name","套路深");
        map.put("companyName","上海音拓");
        map.put("companyName","上海音拓");
        map.put("date","2017-05-22");
        map.put("positionName","产品经理");
        map.put("address","上海市汾阳路3号1号");
        map.put("mobilePhone","123456789");
        sendMobileMessage("17621336004", Const.SEND_VIEW,map);
    }


    private static String appkey = "23854153";
    private static String secret = "ff9c4d144dd251668e69fdbf49d80d19";

   public static void sendMobileCode(String mobilePhone,String code){
       //官网的URL
       String url="http://gw.api.taobao.com/router/rest";
        //成为开发者，创建应用后系统自动生成
        //短信模板的内容
       Map<String,String> map = Maps.newHashMap();
       map.put("code",code);
       String json = ObjectMapperUtils.writeValueAsString(map);
       TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
       AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
       req.setExtend("123456");
       req.setSmsType("normal");
       req.setSmsFreeSignName("IH招聘");
       req.setSmsParamString(json);
       req.setRecNum(mobilePhone);
       req.setSmsTemplateCode("SMS_69750279");
       try {
           AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
           System.out.println(rsp.getBody());
       } catch (Exception e) {
           e.printStackTrace();
       }
   }

    public static void sendMobileMessage(String mobilePhone,String templateCode,Map<String,String> map){
        //官网的URL
        String url="http://gw.api.taobao.com/router/rest";
        //短信模板的内容
        String json = ObjectMapperUtils.writeValueAsString(map);
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend("123456");
        req.setSmsType("normal");
        req.setSmsFreeSignName("IH招聘");
        req.setSmsParamString(json);
        req.setRecNum(mobilePhone);
        req.setSmsTemplateCode(templateCode);
        try {
            AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
            System.out.println(rsp.getBody());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
