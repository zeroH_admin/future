package com.spring.future.common.thymeleaf.function.dialect;

import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;


public class ThymeleafFunctionDialect extends AbstractDialect implements IExpressionObjectDialect {

    private final IExpressionObjectFactory THYMELEAF_FUNCTION_EXPRESSION_OBJECTS_FACTORY = new ThymeleafFunctionExpressionFactory();

    public ThymeleafFunctionDialect() {
        super("thymeleafFunction");
    }

    @Override
    public IExpressionObjectFactory getExpressionObjectFactory() {
        return THYMELEAF_FUNCTION_EXPRESSION_OBJECTS_FACTORY;
    }

}
