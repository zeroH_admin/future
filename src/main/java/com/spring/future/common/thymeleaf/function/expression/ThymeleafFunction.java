package com.spring.future.common.thymeleaf.function.expression;

import com.google.common.collect.Lists;
import com.spring.future.FutureApplication;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.domain.Sort;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public final class ThymeleafFunction {

    private final Logger logger = LoggerFactory.getLogger(ThymeleafFunction.class);

    private ApplicationContext applicationContext = FutureApplication.getApplicationContext();


    public String formatLocalDateTime(LocalDateTime localDateTime) {
        String string = "";
        if (localDateTime != null) {
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(Const.FORMATTER_LONG_DATE_TIME);
            string = localDateTime.format(dateTimeFormatter);
        }
        return string;
    }

    public boolean isNotBlank(String value) {
        return StringUtils.isNotBlank(value);
    }

    public boolean isEquals(String value1,String value2) {
        if(StringUtils.isNotBlank(value1)&&StringUtils.isNotBlank(value2)){
            return value1.equals(value2);
        }
        return false;
    }

    public String getPageInput(Pageable pageable) {
        StringBuilder sb = new StringBuilder();
        sb.append("<input type='hidden' name='searchFromPage' value='true'/>").append(Const.CHAR_ENTER);
        sb.append("<input type='hidden' name='pageNumber'  value='").append(pageable.getPageNumber()).append("'/>").append(Const.CHAR_ENTER);
        Iterator<Sort.Order> iterator = pageable.getSort().iterator();
        Sort.Direction direction = Sort.Direction.DESC;
        while (iterator.hasNext()) {
            Sort.Order sortOrder = iterator.next();
            String order = sortOrder.getProperty() + ": " + sortOrder.getDirection().name();
            sb.append("<input type='hidden' name='order'  value='").append(order).append("'/>").append(Const.CHAR_ENTER);
        }
        return sb.toString();
    }

    public String getStaticFile(String url) {
        RedisTemplate<String, String> redisTemplate = (RedisTemplate<String, String>) FutureApplication.getApplicationContext().getBean("redisTemplate");
        String key = "STATIC_FILE_VERSION_" + url;
        String value = redisTemplate.opsForValue().get(key);
        if (StringUtils.isBlank(value)) {
            try {
                File file = ResourceUtils.getFile("src/main/resources/static" + url);
                Long lastModified = file.lastModified();
                if (lastModified != null) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyMMddHHmmss");
                    value = simpleDateFormat.format(lastModified);
                    redisTemplate.opsForValue().set(key, value, 1, TimeUnit.HOURS);
                }
            } catch (FileNotFoundException e) {
                logger.error("file not found:" + url);
            }
        }
        if (StringUtils.isNotBlank(value)) {
            url = url + "?version=" + value;
        }
        return url;
    }

}