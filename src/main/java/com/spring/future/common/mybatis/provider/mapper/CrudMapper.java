package com.spring.future.common.mybatis.provider.mapper;

import com.spring.future.common.mybatis.provider.MybatisProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;

import java.io.Serializable;
import java.util.List;

/**
 * Created by liuj on 2016/11/14.
 */
public interface CrudMapper<T, ID extends Serializable> extends BaseMapper<T, ID> {

    @InsertProvider(type=MybatisProvider.class, method = MybatisProvider.SAVE)
    @Options(useGeneratedKeys = true)
    int save(T entity);

    @UpdateProvider(type=MybatisProvider.class, method = MybatisProvider.UPDATE)
    int update(T entity);

    @InsertProvider(type=MybatisProvider.class,method = MybatisProvider.BATCH_SAVE)
    int batchSave(List<T> entities);

    @SelectProvider(type=MybatisProvider.class,method = MybatisProvider.FIND_ALL)
    List<T> findAll();

    @SelectProvider(type=MybatisProvider.class,method = MybatisProvider.FIND_ONE)
    T findOne(ID id);

    @SelectProvider(type=MybatisProvider.class,method = MybatisProvider.FIND_BY_IDS)
    List<T> findByIds(List<ID> ids);

    @SelectProvider(type=MybatisProvider.class,method = MybatisProvider.COUNT)
    Long count();

    @UpdateProvider(type=MybatisProvider.class,method = MybatisProvider.DELETE_ONE)
    int deleteOne(ID id);

    @UpdateProvider(type=MybatisProvider.class,method = MybatisProvider.DELETE_BY_IDS)
    int deleteByIds(List<ID> ids);

    @UpdateProvider(type=MybatisProvider.class,method = MybatisProvider.LOGIC_DELETE_ONE)
    int logicDeleteOne(ID id);

    @UpdateProvider(type=MybatisProvider.class,method = MybatisProvider.LOGIC_DELETE_BY_IDS)
    int logicDeleteByIds(List<ID> ids);
}
