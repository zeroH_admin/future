package com.spring.future.common.mybatis.dialect;


import com.spring.future.common.mybatis.domain.Pageable;

public class PostgreSQLDialect extends Dialect {
	
	final static String LIMIT_SQL_PATTERN = "%s limit %s offset %s";
	
	final static String LIMIT_SQL_PATTERN_FIRST = "%s limit %s";

	@Override
	public String getPageableSql(String sql, Pageable pageable) {
		if (pageable.getOffset() == 0) {
			return String.format(LIMIT_SQL_PATTERN_FIRST, sql, pageable.getPageSize());
		} else {
			return String.format(LIMIT_SQL_PATTERN, sql, pageable.getPageSize(), pageable.getOffset());
		}
	}

}
