package com.spring.future.common.mybatis.provider;

/**
 * Created by liuj on 2016/11/15.
 */
public class ProviderContext {
    private Class entityClass;
    private Class idClass;

    public Class getEntityClass() {
        return entityClass;
    }

    public void setEntityClass(Class entityClass) {
        this.entityClass = entityClass;
    }

    public Class getIdClass() {
        return idClass;
    }

    public void setIdClass(Class idClass) {
        this.idClass = idClass;
    }
}
