package com.spring.future.common.mybatis.provider;


import com.spring.future.common.mybatis.provider.model.MybatisTable;
import com.spring.future.common.utils.ThreadLocalContext;

/**
 * Created by liuj on 2016/11/12.
 */
public class BaseProvider {

    protected MybatisTable getMybatisTable() {
        return ProviderUtils.getMybatisTable(getEntityClass());
    }

    protected Class getEntityClass() {
        return ThreadLocalContext.get().getProviderContext().getEntityClass();
    }
}
