package com.spring.future.common.enums;

/**
 * Created by liuj on 2017/2/16.
 */
public enum ExcelCellStyle {
    HEADER,DATA,YELLOW,LIGHT_GREEN,RED,LIGHT_BLUE
}
