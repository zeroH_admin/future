package com.spring.future.common.enums;

/**
 * Created by zh on 2017/5/10.
 */
public enum ResumeStatusEnum {
    新简历,待沟通,待面试,待录用,待入职,淘汰
}
