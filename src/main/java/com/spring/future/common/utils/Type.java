package net.myspring.cloud.common.enums;

/**
 * Created by lihx on 2016/12/31.
 */
public enum Type {
    success, info, warning, danger
}
