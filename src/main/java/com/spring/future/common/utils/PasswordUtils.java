package com.spring.future.common.utils;

import org.apache.tomcat.util.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by zh on 2017/5/17.
 */
public class PasswordUtils {

    public static boolean checkCloudPassword(String salt,String password,String cloudPassword){
        boolean isSame = true;
        try {
            String result = encode(salt,password);
            if(!result.equals(cloudPassword)){
                isSame = false;
            }
        }catch (Exception e){
            return false;
        }
        return isSame;
    }

    private static String encode(String salt,String password )throws UnsupportedEncodingException, NoSuchAlgorithmException{
        password = salt + password;
        MessageDigest messageDisgest = MessageDigest.getInstance("SHA-512");
        messageDisgest.update(password.getBytes());

        byte byteBuffer[] = messageDisgest.digest();

        for(int i = 0; i < 512; i ++){
            byteBuffer = messageDisgest.digest(byteBuffer);
        }
        return new String(Base64.encodeBase64(byteBuffer), Const.CHAR_UTF8);
    }
//
//    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
//        String salt = "fgse2yymzwqid7ap1hmvh5ro0ssryddcjxjod1p0f00gcwg1";
//        String password = "12345678";
//        String result = encode(salt,password);
//        System.err.println(result);
//        System.err.println("asdkfhsd");
//    }
}
