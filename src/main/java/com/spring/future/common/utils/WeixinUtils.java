package com.spring.future.common.utils;

import com.fasterxml.jackson.databind.util.JSONPObject;
import okhttp3.*;

import java.util.Dictionary;
import java.util.Map;

/**
 * Created by zh on 2017/4/28.
 */
public class WeixinUtils {

    public WeixinUtils(){}

    private static final OkHttpClient okHttpClient = new OkHttpClient();

    public static String get_access_token() throws Exception{
        //获得配置信息
        String send_url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" +
                Const.oauth_app_id + "&secret=" + Const.oauth_app_key + "";
        //发送并接受返回值
        Request request=new Request.Builder().url(send_url).build();
        Response response=okHttpClient.newCall(request).execute();
        String result = response.body().string();
        if (result.contains("errmsg")){
            return null;
        }
       return result;
    }

    public static String get_access_token(String code, String state){
        String result = "";
        try {
            //获得配置信息
            String send_url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" +
                    Const.oauth_app_id + "&secret=" + Const.oauth_app_key + "&code="+code+"&grant_type=authorization_code";
            //发送并接受返回值
            Request request=new Request.Builder().url(send_url).build();
            Response response=okHttpClient.newCall(request).execute();
            result = response.body().string();
            if (result.contains("errmsg"))
            {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
    /// <summary>
    /// 根据access_token判断access_token是否过期
    /// </summary>
    /// <param name="access_token"></param>
    /// <returns>true表示未失效</returns>
    public static Boolean check_access_token(String access_token) {
        //获得配置信息
        String send_url = "https://api.weixin.qq.com/sns/auth?access_token=" + access_token + "&openid=" + Const.oauth_app_id;
        //发送并接受返回值
        Request request=new Request.Builder().url(send_url).build();
        String result = "";
        try
        {
            Response response=okHttpClient.newCall(request).execute();
            result = response.body().string();
            Map<String, Object> map = ObjectMapperUtils.readValue(result,Map.class);
            if (result.contains("errmsg")){
                if (map.get("errmsg")=="ok"){
                    return true;
                }else{
                    return false;
                }
            }
            return false;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }


    /// <summary>
    /// 若fresh_token已过期则根据refresh_token取得新的refresh_token
    /// </summary>
    /// <param name="refresh_token">refresh_token</param>
    /// <returns>Dictionary</returns>
    public static String get_refresh_token(String refresh_token){
        //获得配置信息
        String send_url =
                "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" +
                        Const.oauth_app_id + "&grant_type=refresh_token&refresh_token=" + refresh_token;
        //发送并接受返回值
        Request request=new Request.Builder().url(send_url).build();
        String result = "";
        try {
            Response response=okHttpClient.newCall(request).execute();
            result = response.body().string();
        }catch (Exception e){
            e.printStackTrace();
        }
        if (result.contains("errmsg")){
            return null;
        }
        return result;
    }
    /// <summary>
    /// 获取登录用户自己的基本资料
    /// </summary>
    /// <param name="access_token">临时的Access Token</param>
    /// <param name="open_id">用户openid</param>
    /// <returns>Dictionary</returns>
    public static String get_user_info(String access_token, String open_id) {
        String result = "";
        try {
            //获得配置信息
            //发送并接受返回值
            String send_url = "https://api.weixin.qq.com/sns/userinfo?access_token="+access_token+"&openid="+open_id;
            //发送并接受返回值
            Request request=new Request.Builder().url(send_url).build();
            Response response=okHttpClient.newCall(request).execute();
            result = response.body().string();
            if (result.contains("errmsg"))
            {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
}
