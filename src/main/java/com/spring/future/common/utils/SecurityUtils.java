package com.spring.future.common.utils;

import com.google.common.collect.Lists;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

/**
 * Created by liuj on 2017/2/9.
 */
public class SecurityUtils {
    public static List<String> getAuthorityList() {
        List<String> authorities = Lists.newArrayList();
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication authentication = context.getAuthentication();
            if (authentication != null && Collections3.isNotEmpty(authentication.getAuthorities())) {
                for(GrantedAuthority authority:authentication.getAuthorities()) {
                    authorities.add(authority.getAuthority().toString());
                }
            }
        }
        return authorities;
    }
}
