package com.spring.future.common.utils;


import com.spring.future.common.mybatis.dialect.Dialect;
import com.spring.future.common.mybatis.dialect.MySQLDialect;
import com.spring.future.common.mybatis.provider.ProviderContext;
import com.spring.future.modules.ih.domain.Account;

/**
 * Created by liuj on 2016-08-18.
 */
public class ThreadLocalContext {
    private String dataSourceType;
    private ProviderContext providerContext;
    private Account account;

    private static ThreadLocal<ThreadLocalContext> threadLocal = new ThreadLocal<ThreadLocalContext>();

    public static ThreadLocalContext get() {
        if (threadLocal.get() == null) {
            ThreadLocalContext threadLocalContext = new ThreadLocalContext();
            threadLocal.set(threadLocalContext);
        }
        return threadLocal.get();
    }

    public void remove() {
        this.dataSourceType = "";
        this.providerContext = null;
        this.account=null;
        threadLocal.remove();
    }

    public String getDataSourceType() {
        return dataSourceType;
    }

    public void setDataSourceType(String dataSourceType) {
        this.dataSourceType = dataSourceType;
    }

    public ProviderContext getProviderContext() {
        return providerContext;
    }

    public void setProviderContext(ProviderContext providerContext) {
        this.providerContext = providerContext;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Dialect getDialect() {
        return new MySQLDialect();
    }

}
