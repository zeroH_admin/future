package com.spring.future.common.utils;

import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.security.CustomUserDetails;
import com.spring.future.modules.ih.security.CustomUserDetailsService;
import com.spring.future.modules.ih.security.MyAuthenticationManager;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.translate.domain.Permission;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.service.PermissionService;
import com.spring.future.modules.translate.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by zh on 2017/5/13.
 */
@Component
public class SecurityAuothManager {
    @Autowired
    private MyAuthenticationManager myAuthenticationManager;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;
    @Autowired
    private AccountService accountService;

    public void login(Account account){
        account.setLastLoginDate(LocalDateTime.now());
        accountService.update(account);
        //登陆
        try {
            CustomUserDetails customUserDetails = new CustomUserDetails(
                    account.getUserName(),
                    account.getPassword(),
                    true,
                    true,
                    true,
                    true,
                    getAuthorities(account),
                    account);
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(customUserDetails,account.getPassword(), customUserDetails.getAuthorities());
            Authentication authentication = myAuthenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            ThreadLocalContext.get().setAccount(account);
            HttpSession session = request.getSession(true);
            session.setAttribute("SPRING_SECURITY_CONTEXT",SecurityContextHolder.getContext());
        }catch (Exception e){
            e.printStackTrace();
            System.err.println("认证失败");
        }
    }
    private Collection<? extends GrantedAuthority> getAuthorities(Account account) {
        Set<SimpleGrantedAuthority> authList = new TreeSet<SimpleGrantedAuthority>(new SimpleGrantedAuthorityComparator());
        Role role = roleService.findByName(account.getPosition());
        if(role!=null){
            authList.add(new SimpleGrantedAuthority(role.getPermission()));
            List<Permission> permissionList = permissionService.findByRole(role.getId());
            if(Collections3.isNotEmpty(permissionList)){
                for (Permission permission:permissionList){
                    authList.add(new SimpleGrantedAuthority(permission.getPermission()));
                }
            }
        }
        return authList;
    }

    private static class SimpleGrantedAuthorityComparator implements Comparator<SimpleGrantedAuthority> {
        @Override
        public int compare(SimpleGrantedAuthority o1, SimpleGrantedAuthority o2) {
            return o1.equals(o2) ? 0 : -1;
        }
    }
}
