package com.spring.future.common.utils;


import java.io.Serializable;

public class Message implements Serializable {
    private static final long serialVersionUID = 811433140528060176L;

    public enum Type {
        success, info, warning, danger
    }

    public Message() {
        this.code = "";
        this.type = Type.success;
    }

    public Message(String code) {
        this.code = code;
        this.type = Type.success;
    }

    public Message(String code, Type type) {
        this.code = code;
        this.type = type;
    }

    private String code;
    private Type type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void addCode(String code) {
        this.code = this.code + code;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (StringUtils.isBlank(code) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Message other = (Message) obj;
        if (StringUtils.isBlank(code)) {
            if (StringUtils.isNotBlank(other.getCode()))
                return false;
        } else if (!code.equals(other.getCode()))
            return false;
        return true;
    }
}
