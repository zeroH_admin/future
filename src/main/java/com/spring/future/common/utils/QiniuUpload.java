package com.spring.future.common.utils;



import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import com.qiniu.util.Base64;
import com.qiniu.util.StringMap;
import com.qiniu.util.UrlSafeBase64;
import com.spring.future.modules.ih.model.Education;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.util.Map;

/**
 * Created by zh on 2017/5/9.
 */
public class QiniuUpload {

    public static final String ACCESS_KEY = "AfI8P0B6lb3o5i5U9g9LsSQz7Jg3WCP1EZKQsqP1";
    public static final String SECRET_KEY = "wKb-mljZa42DIOjkzWkSzIOtpXX5i3VdO9xi2f9U";
    public static final String BUCKET_NAME = "internhuntertest";

    //密钥配置
    static Auth auth = Auth.create(ACCESS_KEY, SECRET_KEY);
    //创建上传对象
    static Configuration configuration = new Configuration();
    static UploadManager uploadManager = new UploadManager(configuration);

    //简单上传，使用默认策略，只需要设置上传的空间名就可以了
    private static String getFileUpToken(){
        return auth.uploadToken(BUCKET_NAME);
    }
    //普通上传
    public static void upload(File file,String fileName){
        try {
            //调用put方法上传
            Response res = uploadManager.put(file, fileName, getFileUpToken());
            //打印返回的信息
            System.out.println(res.bodyString());
        } catch (QiniuException e) {
            Response r = e.response;
            // 请求失败时打印的异常的信息
            System.out.println(r.toString());
            try {
                //响应的文本信息
                System.out.println(r.bodyString());
            } catch (QiniuException e1) {
                //ignore
            }
        }
    }




    public static String getImgStrUpToken(){
        return auth.uploadToken(BUCKET_NAME, null, 3600, new StringMap().put("insertOnly", 1));
    }

    public static Boolean upload64image(String imgStr,String key){
        try {
            imgStr = imgStr.replace("data:image/png;base64,","");
            String url = "http://upload.qiniu.com/putb64/-1/key/"+ UrlSafeBase64.encodeToString(key);
            //非华东空间需要根据注意事项 1 修改上传域名
            RequestBody rb = RequestBody.create(null, imgStr);
            Request request = new Request.Builder().
                    url(url).
                    addHeader("Content-Type", "application/octet-stream")
                    .addHeader("Authorization", "UpToken " + getImgStrUpToken())
                    .post(rb).build();
            System.out.println(request.headers());
            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            System.out.println(response);
            if("ok".equalsIgnoreCase(response.message())){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

//    public static String ReadFile(String path) {
//        File file = new File(path);
//        BufferedReader reader = null;
//        String laststr = "";
//        try {
//            reader = new BufferedReader(new FileReader(file));
//            String tempString = null;
//            while ((tempString = reader.readLine()) != null) {
//                laststr = laststr + tempString;
//            }
//            reader.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e1) {
//                }
//            }
//        }
//        return laststr;
//    }
//
//
    public static void main(String[] args){
        QiniuUpload qiniuUpload = new QiniuUpload();
        System.err.println(qiniuUpload.getImgStrUpToken());
    }
//

}
