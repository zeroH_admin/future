package com.spring.future.common.utils;

import com.spring.future.FutureApplication;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.ih.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by zh on 2017/5/19.
 */
@Component
public class AccountUtils {

    public static Account getAccount(){
        return ThreadLocalContext.get().getAccount();
    }

    public static String getCompanyId(){
        return getAccount().getCompanyId();
    }

    public static String getAccountId(){
        return getAccount() ==null?"":getAccount().getId();
    }

    public static Company getCompany(){
        CompanyService companyService = FutureApplication.getApplicationContext().getBean(CompanyService.class);
        Company company = companyService.findOne(getAccount().getCompanyId());
        return company;
    }

    public static User getUser(){
        UserService userService = FutureApplication.getApplicationContext().getBean(UserService.class);
        User user = userService.findOne(getAccount().getUserId());
        return user;
    }
}
