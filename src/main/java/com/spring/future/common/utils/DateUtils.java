package com.spring.future.common.utils;

import com.google.common.collect.Lists;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * Created by liuj on 2016/11/10.
 */
public class DateUtils {

    public static LocalDate parseLocalDate(String localDateStr) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(localDateStr, formatter);
        return localDate;
    }

    public static LocalDateTime parseLocalDateTime(String localDateTimeStr) {
        String format = "yyyy-MM-dd HH:mm";
        if(localDateTimeStr.length() != format.length()) {
            format = "yyyy-MM-dd HH:mm:ss";
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime localDateTime = LocalDateTime.parse(localDateTimeStr, formatter);
        return localDateTime;
    }

    public static String formatLocalDate(LocalDate localDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return localDate.format(formatter);
    }

    public static String formatLocalDate(LocalDate localDate,String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return localDate.format(formatter);
    }

    public static String formatLocalDateTime(LocalDateTime localDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return localDateTime.format(formatter);
    }

    public static LocalDate getFirstDayOfLastMonth(LocalDate date){
        LocalDate initial = date.minusMonths(1);
        return initial.withDayOfMonth(1);
    }

    public static LocalDateTime getFirstDayOfThisMonth(LocalDateTime localDateTime){
        return localDateTime.withDayOfMonth(1);
    }

    public static LocalDateTime getLastDayOfThisMonth(LocalDateTime localDateTime){
        return localDateTime.withDayOfMonth(localDateTime.toLocalDate().lengthOfMonth());
    }

    public static LocalDate getFirstDayOfThisMonth(LocalDate localDate){
        return localDate.withDayOfMonth(1);
    }

    public static LocalDate getLastDayOfLastMonth(LocalDate date){
        LocalDate initial = date.minusMonths(1);
        return initial.withDayOfMonth(initial.lengthOfMonth());
    }

    public static LocalDate getFirstDayOfLastWeek(LocalDate date){
        LocalDate lastMonday = date.with(DayOfWeek.MONDAY).minusWeeks(1) ;
        return lastMonday;
    }

    public static LocalDate getLastDayOfLastWeek(LocalDate date){
        LocalDate lastMonday = date.with(DayOfWeek.SUNDAY).minusWeeks(1);
        return lastMonday;
    }

    public static LocalDateTime getDateEnd(LocalDate localDate){
        String date = formatLocalDate(localDate);
        date = date + " 23:59:59";
        return parseLocalDateTime(date);
    }

    public static LocalDateTime getDateStart(LocalDate localDate){
        String date = formatLocalDate(localDate);
        date = date + " 00:00:00";
        return parseLocalDateTime(date);
    }

    // 时间是否交叉
    public static boolean isCross(LocalTime from1, LocalTime to1, LocalTime from2, LocalTime to2) {
        if (from1 == null || to1 == null || from2 == null || to2 == null) {
            return false;
        }
        LocalTime minTime = from1;
        LocalTime maxTime= to1;
        if (from2.isBefore(from1)) {
            minTime = from2;
        }
        if (to2.isAfter(to1)) {
            maxTime = to2;
        }
        Long maxDiff = ChronoUnit.MILLIS.between(minTime,maxTime);
        Long diff =ChronoUnit.MILLIS.between(from1,to1) + ChronoUnit.MILLIS.between(from2,to2);
        if (maxDiff >= diff) {
            return false;
        } else {
            return true;
        }
    }

    public static List<LocalDate> getDateList(LocalDate dateStart, LocalDate dateEnd) {
        List<LocalDate> dateList = Lists.newArrayList();
        if (dateEnd.isBefore(dateStart)) {
            return dateList;
        }
        Long step = dateEnd.toEpochDay() - dateStart.toEpochDay();
        if (step >= 1) {
            for (int i = 0; i <= step; i++) {
                dateList.add(dateStart.plusDays(i));
            }
        }
        return dateList;
    }

    public static Boolean inToday(LocalDateTime localDateTime){
        LocalDateTime start = LocalDateTime.now();
        LocalDateTime end = start.plusDays(1);
        return localDateTime.isAfter(start) && localDateTime.isBefore(end);
    }

    public static String getAgeByBirthDay(LocalDate localDate){
        int nowYear = LocalDate.now().getYear();
        int year = localDate.getYear();
        int age = nowYear-year+1;
        return String.valueOf(age);
    }
}
