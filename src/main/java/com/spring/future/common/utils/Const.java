package com.spring.future.common.utils;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by liuj on 2016-08-20.
 */
public class Const {

    public static final int SHIRO_HASH_INTERATIONS = 1024;
    public static final int SHIRO_SALT_SIZE = 8;

    public static final String ROOT_ID = "0";
    public static final String ROOT_PARENT_IDS = "0,";

    public static final String CHAR_SPACE = " ";
    public static final String CHAR_SPACE_FULL = "　";
    public static final String CHAR_COMMA = ",";
    public static final String CHAR_TAB = "	";
    public static final String CHAR_COMMA_FULL = "，";
    public static final String CHAR_ENTER = "\n";
    public static final String CHAR_UNDER_LINE = "_";
    public static final String CHAR_COLON = ":";
    public static final String CHAR_UTF8 = "UTF-8";

    public static final String FORMATTER_DATE = "yyyy-MM-dd";
    public static final String FORMATTER_SHORT_DATE = "yyyyMMdd";
    public static final String FORMATTER_DATE_TIME = "yyyy-MM-dd HH:mm";
    public static final String FORMATTER_LONG_DATE_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMATTER_TIME = "HH:mm";
    public static final String FORMATTER_LONG_TIME = "HH:mm:ss";

    public static final Integer DEFULT_LEVEL = 1;
    public static final int DEFAULT_PAGE_SIZE = 5000;

    public static final String USER = "user";
    public static final String COMPANY = "company";

    public static final String companyImg = "companyImg_";
    public static final String oauth_app_id = "wx6f220d9d21913982";
    public static final String oauth_app_key="548048c92dffab798cafe69391b2d885";
    public static final String return_uri = "http://www.internhunter.com/weixin/thirdLogin";
    public static final String QINIU_URL = "http://opo2ue3fr.bkt.clouddn.com/";

    public static final String SEND_VIEW = "SMS_67885001";

    public static final String COMPANY_AUDIT_INFO = "SMS_70400365";
    public static final String COMPANY_AUDIT_FAIL_INFO = "SMS_70305476";

    public static final String PUSH_RESUME_SMS = "";

    public static final String RESUME_AUDIT_SMS = " ";

    public static final String JOB_AUDIT_INFO = "SMS_71865236";
    public static final String JOB_AUDIT_FAIL_INFO = "SMS_71850388";


    public static final String AUDIT_INFO_TEMPLETE = "auditInfo";

}

