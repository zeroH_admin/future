package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.service.ActivityService;
import com.spring.future.modules.translate.service.ActivitySignService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Controller
public class ActivityController {

    @Autowired
    private ActivityService activityService;
    @Autowired
    private ActivitySignService activitySignService;

    @RequestMapping(value = "common/club")
    public String list(Model model,HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        map.put("tableName","近期活动");
        Page<Activity> page = activityService.findIndexPage(searchEntity.getPageable(),map);
        model.addAttribute("page",page);
        model.addAttribute("s", searchEntity);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",StringUtils.isNoneBlank(AccountUtils.getAccountId()));
        return "ih/club";
    }

    @RequestMapping(value = "common/lastClub")
    public String lastClub(Model model,HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        map.put("tableName","往期活动");
        Page<Activity> page = activityService.findIndexPage(searchEntity.getPageable(),map);
        model.addAttribute("page",page);
        model.addAttribute("s", searchEntity);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",StringUtils.isNoneBlank(AccountUtils.getAccountId()));
        return "ih/lastClub";
    }

    @RequestMapping(value = "common/signClub")
    public String signClub(Model model,HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Boolean isLogin = StringUtils.isNoneBlank(AccountUtils.getAccountId());
        map.put("tableName","已报名活动");
        if(isLogin){
            map.put("userId",AccountUtils.getAccount().getUserId());
            Page<Activity> page = activityService.findIndexPage(searchEntity.getPageable(),map);
            model.addAttribute("page",page);
        }
        model.addAttribute("s", searchEntity);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",isLogin);
        return "ih/signClub";
    }



    @RequestMapping(value = "common/clubDetail")
    public String clubDetail(Model model,String id){
        Activity activity = activityService.findOne(id);
        activityService.addViewNum(activity);
        Account account = AccountUtils.getAccount();
        Boolean isLogin = StringUtils.isNoneBlank(AccountUtils.getAccountId());
        ActivitySign activitySign=null;
        int status=0;
        if(isLogin){
            activitySign = activitySignService.findByUserAndActivity(account.getUserId(),id);
            if(activitySign!=null){
                if("待审核".equals(activitySign.getStatus())){
                    status=1;
                }else if("审核通过".equals(activitySign.getStatus())){
                    status=2;
                }else if("审核不通过".equals(activitySign.getStatus())){
                    status=3;
                }
            }
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        model.addAttribute("auditStatus",status);
        model.addAttribute("isSign",activitySign==null?false:true);
        model.addAttribute("canSign", localDateTime.isAfter(activity.getDateStart()) && localDateTime.isBefore(activity.getDateEnd()));
        model.addAttribute("activity",activity);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",isLogin);
        return "ih/club-show";
    }

    @RequestMapping(value = "regiestActivity")
    @ResponseBody
    public String regiest(String id){
        activitySignService.activitySign(id);
        return "ok";
    }
}
