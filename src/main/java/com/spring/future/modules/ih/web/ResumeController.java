package com.spring.future.modules.ih.web;

import com.google.common.collect.Maps;
import com.lowagie.text.pdf.BaseFont;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Resume;
import com.spring.future.modules.ih.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zh on 2017/4/18.
 */
@Controller
public class ResumeController {

    @Autowired
    private ResumeService resumeService;
    @Value("${root-path}")
    private String rootPath;


    @RequestMapping(value = "intern/resume")
    public String resume(Model model){
        model.addAttribute("account", AccountUtils.getAccount());
        return "ih/resume";
    }

    @RequestMapping(value = "intern/getAccountResume", method = RequestMethod.GET)
    @ResponseBody
    public String getAccountResume(){
        Resume resume = resumeService.getAccountResume(false);
        return ObjectMapperUtils.writeValueAsString(resume);
    }

    @RequestMapping(value = "intern/saveResume")
    @ResponseBody
    public String save(Resume resume){
        resumeService.save(resume);
        return ObjectMapperUtils.writeValueAsString(new RestResponse("保存成功"));
    }

    @RequestMapping(value = "intern/viewResume")
    public String viewResume(){
        return "ih/resume-show";
    }

    /**
     * 邮件中临时查看简历
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "/temporary/viewResume")
    public String viewResume(String id,Model model){
        model.addAttribute("id",id);
        return "ih/linshi-resume";
    }

    @RequestMapping(value = "/temporary/viewResumeData")
    @ResponseBody
    public String viewResumeData(String id,Model model){
        Resume resume = resumeService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(resume);
    }

    /**
     * 实习生下载简历
     * @param request
     * @param response
     * @throws Exception
     */
    @RequestMapping(value = "/intern/downLoadResume")
    public void dowmLoadResume(HttpServletRequest request, HttpServletResponse response) throws Exception{
        Resume resume = resumeService.getAccountResume(true);
        downLoadPdf(request,response,resume);
    }

    /**
     * 人事根据简历id下载pdf 的简历
     * @param request
     * @param response
     * @param id
     * @throws Exception
     */
    @RequestMapping(value = "/hr/downLoadResume")
    public void dowmLoadPdf(HttpServletRequest request, HttpServletResponse response,String id) throws Exception{
        Resume resume = resumeService.downLoadResume(id);
        downLoadPdf(request,response,resume);
    }

    private void downLoadPdf(HttpServletRequest request, HttpServletResponse response,Resume resume) throws Exception{
        String filePath =rootPath+"pdf/" + UUID.randomUUID().toString()+ ".pdf";
        OutputStream out = new FileOutputStream(filePath);
        Map<String, Object> map = Maps.newHashMap();
        map.put("resume", resume);

        String htmlStr = HtmlGenerator.generate(rootPath+"template/", "template", map);

        DocumentBuilder builder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")));
        ITextRenderer renderer = new ITextRenderer();

        ITextFontResolver fontResolver = renderer.getFontResolver();
        fontResolver.addFont(rootPath+"/fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        fontResolver.addFont(rootPath+"/fonts/simhei.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        renderer.setDocument(doc, null);
        renderer.layout();
        renderer.createPDF(out);
        out.close();
        // 下载文件
        String fileName = resume.getName()+"简历.pdf";
        ExcelUtil.downloadFile(request, response, fileName, filePath);
    }
}
