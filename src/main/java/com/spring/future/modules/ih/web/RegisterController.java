package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.enums.PositionEnum;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.ih.service.DictEnumService;
import com.spring.future.modules.ih.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

/**
 * Created by zh on 2017/4/20.
 */
@Controller
public class RegisterController {

    @Autowired
    private RegisterService registerService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private SecurityAuothManager securityAuothManager;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * 注册页面
     * @param model
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model) {
        return "ih/register";
    }

    /**
     * 注册时用户名的验证
     * @param userName
     * @return
     */
    @RequestMapping(value = "register/checkUserName")
    @ResponseBody
    public Boolean checkUserName(String userName){
        Account account = accountService.findByUserName(userName);
        Boolean result = true;
        if(account!=null){
            result = false;
        }
        return result;
    }

    /**
     * 注册时用户名的验证
     * @param userName
     * @return
     */
    @RequestMapping(value = "register/checkNameAndType")
    @ResponseBody
    public Boolean userCheck(String userName,String type){
        Account account = accountService.userCheck(userName,type);
        Boolean result = true;
        if(account!=null){
            result = false;
        }
        return result;
    }

    /**
     * 注册信息保存
     * @param account
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "register/save")
    public String save(Account account ,RedirectAttributes redirectAttributes){
        if(StringUtils.isEmpty(account.getUserName()) || StringUtils.isEmpty(account.getPassword())){
            redirectAttributes.addFlashAttribute("message", new Message("注册失败"));
            return "redirect:/register";
        }
        registerService.register(account);
        securityAuothManager.login(account);
        if(Const.USER.equals(account.getType())){
            return "redirect:/register/profile-step";
        }else if(Const.COMPANY.equals(account.getType())){
            //判断邮箱后缀是否在之前公司中存在
            String email = account.getUserName();
            List<Company> companyList = companyService.findByEmailLike(email.split("@")[1]);
            if(Collections3.isNotEmpty(companyList)){
                return "redirect:/register/settled";
            }
            return "redirect:/register/contact";
        }else{
            return "redirect:/index";
        }
    }

    /**
     * 用户信息填写页面
     * @return
     */
    @RequestMapping(value = "register/settled")
    public String settled(HttpServletRequest request, HttpServletResponse response,Model model){
        String email = AccountUtils.getAccount().getUserName();
        List<Company> companyList = companyService.findByEmailLike(email.split("@")[1]);
        model.addAttribute("companyList",companyList);
        return "ih/settled";
    }

    /**
     * 当企业邮箱后缀已存在的时候，点击加入该公司操作
     * @param companyId
     * @return
     */
    @RequestMapping(value = "register/jionCompany")
    public String jionCompany(String companyId){
        accountService.joinCompany(companyId);
        return "redirect:/register/competer-info";
    }

    /**
     * 加入公司的信息完善页面
     * @return
     */
    @RequestMapping(value = "register/competer-info")
    public String competerInfoForm(Model model){
        return "ih/contact-step";
    }

    /**
     * 加入公司的信息完善页面提交保存
     * @return
     */
    @RequestMapping(value = "register/competerInfo")
    public String competerInfo(HttpServletRequest request, HttpServletResponse response,Model model,Account account){
        accountService.competerInfo(account);
        return "redirect:/auditNotice";
    }
    /**
     * 用户信息填写页面
     * @return
     */
    @RequestMapping(value = "register/profile-step")
    public String saveUserForm(HttpServletRequest request, HttpServletResponse response,Model model){
        return "ih/profile-step";
    }

    /**
     * 公司信息填写页面
     * @return
     */
    @RequestMapping(value = "register/contact")
    public String saveCompanyForm(){
        return "ih/contact";
    }

    /**
     * 公司信息保存
     * @return
     */
    @RequestMapping(value = "register/saveContact")
    public String saveCompany(Company company){
        registerService.saveCompany(company);
        return "redirect:/auditNotice";
    }

    /**
     * 个人信息保存
     * @return
     */
    @RequestMapping(value = "register/saveUser")
    public String saveUser(HttpServletRequest request, HttpServletResponse response, User user, Device device){
        registerService.saveUser(user,device);
        return "redirect:/auditNotice";
    }

    /**
     * 重置密码
     * @return
     */
    @RequestMapping(value = "register/reset")
    public String internReset(){
        return "ih/reset-password";
    }

    /**
     * 重置密码保存
     * @param userName
     * @param password
     * @return
     */
    @RequestMapping(value = "register/resetPassword")
    public String resetPassword(String userName,String password){
        registerService.resetPassword(userName,password);
        return "ih/login";
    }


    @RequestMapping(value = "register/checkPassword")
    @ResponseBody
    public Boolean checkPassword(String userName,String password){
        Account account = accountService.findByUserName(userName);
        if(account!=null && StringUtils.isNotBlank(account.getSalt())&& account.getLastModifiedDate().isBefore(LocalDateTime.of(2017, Month.MAY,18,05, 10, 30))){
            boolean result = PasswordUtils.checkCloudPassword(account.getSalt(),password,account.getPassword());
            if(!result){
                return false;
            }else{
                return true;
            }
        }
        Boolean isSame = passwordEncoder.matches(password,account.getPassword());
        if(isSame){
            return true;
        }else{
            return false;
        }
    }


    @RequestMapping(value = "register/getToken")
    @ResponseBody
    public String getToken(Model model){
        String token = "UpToken "+ QiniuUpload.getImgStrUpToken();
        return token;
    }

}
