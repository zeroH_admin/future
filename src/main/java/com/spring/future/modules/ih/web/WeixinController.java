package com.spring.future.modules.ih.web;

import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Weixin;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.WeixinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zh on 2017/4/19.
 */
@Controller
public class WeixinController {
    @Autowired
    private WeixinService weixinService;
    @Autowired
    private AccountService accountService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private SecurityAuothManager securityAuothManager;


    @RequestMapping(value = "weixin/RedirectWechat")
    public void RedirectWechat(HttpServletRequest request, HttpServletResponse response)throws Exception{
        String sendUrl = "https://open.weixin.qq.com/connect/qrconnect?appid=" + Const.oauth_app_id +
                "&redirect_uri=" +URLEncoder.encode(Const.return_uri,"utf-8")+
                "&response_type=code&scope=snsapi_login&state=" + UUID.randomUUID().toString()+
                "#wechat_redirect";

        response.sendRedirect(sendUrl);
    }

    @RequestMapping(value = "weixin/thirdLogin")
    public String thirdLogin(HttpServletRequest request, HttpServletResponse response,String code,String state){
        if(StringUtils.isNoneBlank(code)){
            //登录获取access_token
            String result = WeixinUtils.get_access_token(code,state);
            if(result!=null){
                request.getSession().setAttribute("accessToken",result);
            }else{
                result =(String) request.getSession().getAttribute("accessToken");
            }
            Map<String,String> resultMap = ObjectMapperUtils.readValue(result,Map.class);
            String access_token = resultMap.get("access_token");
            String openid = resultMap.get("openid");
            //验证accessToken是否需要刷新
            Boolean checkCode = WeixinUtils.check_access_token(access_token);
            if(checkCode){
                //刷新accessToken
                WeixinUtils.get_refresh_token(resultMap.get("refresh_token"));
            }
            String userInfo = WeixinUtils.get_user_info(access_token,openid);
            Weixin tempWeixin = ObjectMapperUtils.readValue(userInfo,Weixin.class);
            Weixin weixin = weixinService.findByOpenid(tempWeixin.getOpenid());
            if(weixin!=null && StringUtils.isNotBlank(weixin.getAccountId())){
                Account account = accountService.findOne(weixin.getAccountId());
                if(account!=null){
                    //登陆
                    securityAuothManager.login(account);
                }
                if(account!=null && StringUtils.isBlank(account.getUserId())){
                    //信息完善页面
                    return "redirect:/register/profile-step";
                }else {
                    //用户首页
                    return "redirect:/intern/intern-index";
                }
            }else{
                //微信对象保存到session
                HttpSession session = request.getSession();
                session.setAttribute("seanWeixin",tempWeixin);
                //该微信没有绑定账号，跳转到账号绑定页面
                return "redirect:/weixin/weixin-bind";
            }
        }else{
            //扫码后未同意登录则跳转到登录页面
            return "redirect:/login";
        }

    }

    @RequestMapping(value = "weixin/weixin-bind")
    public String weixinBind(){
        return "ih/weixinbind";
    }

    @RequestMapping(value = "weixin/weixinBind")
    public String weixinBind(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes, String username, String password){
        HttpSession session = request.getSession();
        Account account =accountService.findByUserName(username);
        if(account==null){
            redirectAttributes.addFlashAttribute("message", new Message("请输入正确的账户名称"));
            //跳转绑定页面并提示密码错误
            return "redirect:/weixin/weixin-bind";
        }
        Weixin sessionWexin = (Weixin) session.getAttribute("seanWeixin");
        if(sessionWexin==null){
            return "redirect:/weixin/RedirectWechat";
        }
        Boolean isSame = passwordEncoder.matches(password,account.getPassword());
        if(!isSame){
            redirectAttributes.addFlashAttribute("message", new Message("密码错误"));
            //跳转绑定页面并提示密码错误
            return "redirect:/weixin/weixin-bind";
        }
        weixinService.weixinBind(account,sessionWexin);
        //登陆
        securityAuothManager.login(account);
        if( StringUtils.isBlank(account.getUserId())){
            //信息完善页面
            return "redirect:/register/profile-step";
        }else {
            //用户首页
            return "redirect:/intern/intern-index";
        }
    }

    @RequestMapping(value = "weixin/weixinRegister")
    public String weixinRegister(Account account,HttpServletRequest request, HttpServletResponse response){
        HttpSession session = request.getSession();
        Weixin sessionWexin = (Weixin) session.getAttribute("seanWeixin");
        if(sessionWexin==null){
            return "redirect:/weixin/RedirectWechat";
        }
        weixinService.weixinRegister(account,sessionWexin);
        //登陆
        securityAuothManager.login(account);
        return "redirect:/register/profile-step";
    }
}
