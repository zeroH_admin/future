package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.enums.JobStatusEnum;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.Job;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.ih.service.JobService;
import jdk.nashorn.internal.scripts.JO;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/25.
 */
@Controller
public class JobController {
    @Autowired
    private JobService jobService;
    @Autowired
    private CompanyService companyService;

    @RequestMapping(value = "hr/job-list")
    public String inRecruit(HttpServletRequest request,Model model, String status){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String, Object>map = searchEntity.getParams();
        map.put("companyId",AccountUtils.getCompanyId());
        map.put("accountId",AccountUtils.getAccountId());
        if(map.get("status")==null){
            map.put("status","招聘中");
        }
        Page<Job> page = jobService.findPage(searchEntity.getPageable(), searchEntity.getParams());
        model.addAttribute("page",page);
        model.addAttribute("company",companyService.findOne(AccountUtils.getCompanyId()));
        model.addAttribute("s", searchEntity);
        if(JobStatusEnum.已下线.name().equals(status)){
            return "ih/offline";
        }else if(JobStatusEnum.审核中.name().equals(status)){
            return "ih/examine";
        }else if(JobStatusEnum.审核不通过.name().equals(status)){
            return "ih/jobAuditFail";
        }else{
            return "ih/recruit";
        }
    }

    @RequestMapping(value = "hr/edit-job")
    public String editJob(Model model,String id){
        Job job = jobService.findOne(id);
        model.addAttribute("id",id);
        model.addAttribute("status",job.getStatus());
        return "ih/edit-position";
    }

    @RequestMapping(value = "hr/jobData")
    @ResponseBody
    public String findOne(String id){
        Job job = jobService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(job);
    }

    @RequestMapping(value = "hr/view-job")
    public String viewJob(Model model,String id){
        Job job = jobService.findOne(id);
        model.addAttribute("company",companyService.findOne(ThreadLocalContext.get().getAccount().getCompanyId()));
        model.addAttribute("job",job);
        return "ih/view-position";
    }

    @RequestMapping(value = "intern/view-job")
    public String internViewJob(Model model,String id){
        Job job = jobService.findOne(id);
        model.addAttribute("company",companyService.findOne(ThreadLocalContext.get().getAccount().getCompanyId()));
        model.addAttribute("job",job);
        return "ih/view-position";
    }

    @RequestMapping(value = "hr/post-release")
    public String postRelese(Model model){
        Account account = ThreadLocalContext.get().getAccount();
        Company company = companyService.findOne(account.getCompanyId());
        model.addAttribute("company",company);
        return "ih/post-release";
    }

    @RequestMapping(value = "hr/saveJob")
    @ResponseBody
    public String saveJob(Job job){
        jobService.save(job);
        return ObjectMapperUtils.writeValueAsString(new Message(job.getStatus()));
    }

    @RequestMapping(value = "hr/updateStatus")
    public String updateStatus(String id,String status,String urlType ,RedirectAttributes redirectAttributes){
        jobService.updateStatus(status,id);
        redirectAttributes.addFlashAttribute("message", new Message("职位状态更新成功"));
        redirectAttributes.addAttribute("status",urlType);
        return "redirect:/hr/job-list";
    }

}
