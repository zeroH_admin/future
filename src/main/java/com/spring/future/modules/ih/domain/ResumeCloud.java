package com.spring.future.modules.ih.domain;

import com.google.common.collect.Lists;
import com.spring.future.modules.ih.model.Photo;
import com.spring.future.modules.ih.model.UserCloudModel;
import com.spring.future.modules.ih.model.EducationCloud;
import com.spring.future.modules.ih.model.WorkExpCloud;

import java.util.List;

/**
 * Created by zh on 2017/5/4.
 */
public class ResumeCloud {

    private String id;
    private String email;
    private String updatedAt;
    private String name;
    private String objectId;
    private String nationality;
    private String isPassed;
    private List<String> expectedIndustry;
    private String startDate;
    private List<String> expectedPosition;
    private String createdAt;
    private String majorCategory;
    private String state;
    private List<String>  degree;
    private String daysPerWeek;
    private String internPeriod;
    private String gender;
    private String phoneNumber;
    private String birth;
    private List<EducationCloud> education;
    private List<WorkExpCloud> workExp = Lists.newArrayList();
    private List<String> tag;
    private String selfAssessment;
    private String oneWordIntroduction;

    private UserCloudModel user;
    private Photo photo;

    private String userId;

    public String getIsPassed() {
        return isPassed;
    }

    public void setIsPassed(String isPassed) {
        this.isPassed = isPassed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<String> getExpectedIndustry() {
        return expectedIndustry;
    }

    public void setExpectedIndustry(List<String> expectedIndustry) {
        this.expectedIndustry = expectedIndustry;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public List<String> getExpectedPosition() {
        return expectedPosition;
    }

    public void setExpectedPosition(List<String> expectedPosition) {
        this.expectedPosition = expectedPosition;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMajorCategory() {
        return majorCategory;
    }

    public void setMajorCategory(String majorCategory) {
        this.majorCategory = majorCategory;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<String>  getDegree() {
        return degree;
    }

    public void setDegree(List<String>  degree) {
        this.degree = degree;
    }

    public String getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(String daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public String getInternPeriod() {
        return internPeriod;
    }

    public void setInternPeriod(String internPeriod) {
        this.internPeriod = internPeriod;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public List<EducationCloud> getEducation() {
        return education;
    }

    public void setEducation(List<EducationCloud> education) {
        this.education = education;
    }

    public List<String> getTag() {
        return tag;
    }

    public void setTag(List<String> tag) {
        this.tag = tag;
    }

    public String getSelfAssessment() {
        return selfAssessment;
    }

    public void setSelfAssessment(String selfAssessment) {
        this.selfAssessment = selfAssessment;
    }

    public UserCloudModel getUser() {
        return user;
    }

    public void setUser(UserCloudModel user) {
        this.user = user;
    }

    public Photo getPhoto() {
        return photo;
    }

    public void setPhoto(Photo photo) {
        this.photo = photo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<WorkExpCloud> getWorkExp() {
        return workExp;
    }

    public void setWorkExp(List<WorkExpCloud> workExp) {
        this.workExp = workExp;
    }

    public String getOneWordIntroduction() {
        return oneWordIntroduction;
    }

    public void setOneWordIntroduction(String oneWordIntroduction) {
        this.oneWordIntroduction = oneWordIntroduction;
    }
}
