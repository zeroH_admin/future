package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.BaseMapper;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Mapper
public interface AccountMapper extends CrudMapper<Account,String> {

    Page<Account> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    Account findByUserName(String userName);

    Account findByNameAndType(@Param("userName") String userName,@Param("type") String type);

    List<Account> findByCompany(String companyId);

    List<Account> findData( @Param("p")Map<String,Object> map);

    Page<Account> findCompanyAccount(Pageable pageable , @Param("p")Map<String,Object> map);

    List<Account> findCompanyAccount(@Param("p")Map<String,Object> map);

    int updateLoginDate(String id);

    Integer findBtwDate(@Param("type")String type,@Param("dateStart")LocalDate dateStart,@Param("dateEnd") LocalDateTime dateEnd);
}
