package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.enums.JobStatusEnum;
import com.spring.future.common.enums.ResumeStatusEnum;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.service.ServiceException;
import com.spring.future.common.utils.*;
import com.spring.future.common.validateCodeUtils.EmailUtils;
import com.spring.future.common.validateCodeUtils.SMSUtils;
import com.spring.future.modules.ih.domain.*;
import com.spring.future.modules.ih.mapper.*;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class CompanyService{

    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private FolderFileMapper folderFileMapper;
    @Value("${root-path}")
    private String rootPath;
    @Autowired
    private JobResumeMapper jobResumeMapper;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private EmailUtils emailUtils;


    public List<Company> findByEmailLike(String email){
        return companyMapper.findByEmailLike(email);
    }

    public Company findOne(String id){
        List<String> urlList = folderFileMapper.findUrlByExtendId(Const.companyImg+id);
        Company company = companyMapper.findOne(id);
        if(StringUtils.isNotBlank(company.getCreatedById())){
            Account account = accountMapper.findOne(company.getCreatedById());
            company.setCreatedBy(account);
        }
        company.setImgList(urlList);
        return company;
    }

    public Page<Company> findPage(Pageable pageable, Map<String, Object>map){
        Page<Company> page = companyMapper.findPage(pageable,map);
        Map<String,Company> companyMap = Collections3.extractToMap(page.getContent(),"createdById");
        List<String> accountIds = new ArrayList<>(companyMap.keySet());
        if(Collections3.isNotEmpty(accountIds)){
            List<Account> accounts = accountMapper.findByIds(accountIds);
            Map<String,Account> accountMap = Collections3.extractToMap(accounts,"id");
            for(Company company:page.getContent()){
                if(accountMap.containsKey(company.getCreatedById())){
                    company.setCreatedBy(accountMap.get(company.getCreatedById()));
                }
            }
        }
        return page;
    }

    public Map<String,Object> getIndexNum(){
        Map<String,Object> map = Maps.newHashMap();
        String accountId = AccountUtils.getAccount().getId();
        List<JobResume> jobResumes = jobResumeMapper.findSelf(accountId);
        List<Job> jobs = jobMapper.findSelfJob(accountId);
        int newResumeNum = 0;
        int communicateNum = 0;
        int interViewNum = 0;
        int internJob = 0;
        for(JobResume jobResume:jobResumes){
            if(jobResume.getStatus().equals(ResumeStatusEnum.新简历.name())&& jobResume.getNewResume()){
                newResumeNum = newResumeNum+1;
            }
            if(jobResume.getStatus().equals(ResumeStatusEnum.待沟通.name())){
                communicateNum = communicateNum+1;
            }
            if(jobResume.getStatus().equals(ResumeStatusEnum.待面试.name())&& DateUtils.inToday(jobResume.getInterviewDate())){
                interViewNum = interViewNum +1;
            }
        }
        for(Job job:jobs){
            if(job.getStatus().equals(JobStatusEnum.招聘中.name())){
                internJob = internJob+1;
            }
        }
        map.put("newResumeNum",newResumeNum);
        map.put("communicateNum",communicateNum);
        map.put("interViewNum",interViewNum);
        map.put("internJob",internJob);
        return map;
    }

    @Transactional
    public void updateInfo(Company company){
        Company temp = companyMapper.findOne(company.getId());
        temp.setName(company.getName());
        temp.setSimpleName(company.getSimpleName());
        temp.setProperty(company.getProperty());
        temp.setSize(company.getSize());
        temp.setAddress(company.getAddress());
        temp.setFinancingStatus(company.getFinancingStatus());
        temp.setDescription(company.getDescription());
        companyMapper.update(temp);

        companyMapper.deleteByExtendId(Const.companyImg+company.getId());
        if(Collections3.isNotEmpty(company.getImgList())){
            for(String img:company.getImgList()){
                FolderFile folderFile = new FolderFile();
                folderFile.setUrl(img);
                folderFile.setContentType("image/png");
                folderFile.setName(company.getName());
                folderFile.setSize(0);
                folderFile.setPhysicalName(UUID.randomUUID().toString());
                folderFile.setExtendId(Const.companyImg+company.getId());
                folderFileMapper.save(folderFile);
            }
        }
    }

    @Transactional
    public List<String> companyLogo(Map<String, MultipartFile> fileMap) {
        List<String> list = Lists.newArrayList();
        try {
            for (MultipartFile multipartFile : fileMap.values()) {
                if (multipartFile.getSize() > 0) {
                    // 组装file
                    FolderFile folderFile = new FolderFile();
                    folderFile.setName(multipartFile.getOriginalFilename().replaceAll("/","."));
                    folderFile.setPhysicalName(UUID.randomUUID().toString() + "." + folderFile.getExtendType());
                    folderFile.setUrl(Const.QINIU_URL+folderFile.getPhysicalName());
                    list.add(folderFile.getUrl());
                    // 上传文件到七牛
                    String uploadPath =  rootPath+"upload/" + folderFile.getPhysicalName();
                    File file = new File(uploadPath);
                    multipartFile.transferTo(file);
                    QiniuUpload.upload(file,folderFile.getPhysicalName());
                    Company company = AccountUtils.getCompany();
                    company.setLogo(folderFile.getUrl());
                    companyMapper.update(company);
                }
            }
        } catch (Exception e) {
            throw new ServiceException("文件上传失败" + e.getMessage());
        }
        return list;
    }


    /**
     * 后台处理
     *
     */

    @Transactional
    public void batchAuditCompany(String[] ids,String status){
        List<Company> companyList = companyMapper.findByIds(Arrays.asList(ids));
        for(Company company:companyList){
            company.setStatus(status);
            companyMapper.update(company);
        }
    }

    @Transactional
    public void auditCompany(String id,String status,Boolean email,Boolean sms){
        Company company = companyMapper.findOne(id);
        company.setStatus(status);
        companyMapper.update(company);
        Account account = accountMapper.findOne(company.getCreatedById());
        account.setLocked(false);
        accountMapper.update(account);
        if(email){
            // TODO: 2017/6/7  邮件发送提醒
            Map<String,Object> map = Maps.newHashMap();
            String emailTitle = "企业审核通知";
            if("审核通过".equals(status)){
                String title = "恭喜您！";
                String content = "恭喜您，账号:"+account.getUserName()+"所在公司:"+company.getName()+",审核通过，快去发布职位吧！";
                map.put("title",title);
                map.put("content",content);
                emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
            }else{
                String title = "抱歉！";
                String content = "抱歉，账号:"+account.getUserName()+"所在公司:"+company.getName()+",审核未通过，请先完善公司信息！";
                map.put("title",title);
                map.put("content",content);
                emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
            }
        }
        if(sms){
            if(StringUtils.isNotBlank(account.getMobilePhone())){
                // TODO: 2017/6/7  短信发送提醒
                Map<String,String> map = Maps.newHashMap();
                map.put("accountName",account.getUserName());
                map.put("companyName",company.getName());
                if("审核通过".equals(status)){
                    SMSUtils.sendMobileMessage(account.getMobilePhone(),Const.COMPANY_AUDIT_INFO,map);
                }else{
                    SMSUtils.sendMobileMessage(account.getMobilePhone(),Const.COMPANY_AUDIT_FAIL_INFO,map);
                }
            }
        }
    }

    public List<Company> finExportData(Map<String,Object> map){
        List<Company> result = Lists.newArrayList();
        String ids =(String) map.get("ids");
        if(StringUtils.isNotBlank(ids)){
            List<String> idsList = Arrays.asList(ids);
            result = companyMapper.findByIds(idsList);
        }else{
            result= companyMapper.findData(map);
        }
        List<String> accountIds = Collections3.extractToList(result,"createdById");
        if(Collections3.isNotEmpty(accountIds)){
            List<Account> accounts = accountMapper.findByIds(accountIds);
            if(Collections3.isNotEmpty(accounts)){
                Map<String,Account> accountMap = Collections3.extractToMap(accounts,"id");
                for(Company company:result){
                    Account account = new Account();
                    if(accountMap.containsKey(company.getCreatedById())){
                        account = accountMap.get(company.getCreatedById());
                    }
                    company.getExtendMap().put("userName",account.getUserName());
                    company.getExtendMap().put("contact",account.getContact());
                    company.getExtendMap().put("position",account.getPosition());
                    company.getExtendMap().put("landLine",account.getLandLine());
                    company.getExtendMap().put("mobilePhone",account.getMobilePhone());
                    company.getExtendMap().put("lastLoginDate",account.getLastLoginDate());
                }
            }
        }
        return result;
    }

    public List<Company> finExportComanpanyData(Map<String,Object> map){
        List<Account> accounts = Lists.newArrayList();
        List<Company> result = Lists.newArrayList();
        String ids =(String) map.get("ids");
        if(StringUtils.isNotBlank(ids)){
            List<String> idsList = Arrays.asList(ids.split(","));
            accounts = accountMapper.findByIds(idsList);
        }else{
            accounts= accountMapper.findCompanyAccount(map);
        }
        List<String> companyIds = Collections3.extractToList(accounts,"companyId");
        if(Collections3.isNotEmpty(companyIds)){
            List<Company> companyList = companyMapper.findByIds(companyIds);
            if(Collections3.isNotEmpty(companyList)){
                Map<String,Company> companyMap = Collections3.extractToMap(companyList,"id");
                for(Account account:accounts){
                    Company company = companyMap.get(account.getCompanyId());
                    company.getExtendMap().put("userName",account.getUserName());
                    company.getExtendMap().put("contact",account.getContact());
                    company.getExtendMap().put("position",account.getPosition());
                    company.getExtendMap().put("landLine",account.getLandLine());
                    company.getExtendMap().put("mobilePhone",account.getMobilePhone());
                    company.getExtendMap().put("lastLoginDate",account.getLastLoginDate());
                    result.add(company);
                }
            }
        }
        return result;
    }

    public List<Company> findByNameLike(String key){
        return companyMapper.findByNameLike(key);
    }

    public Integer findAuditNum(){
        return companyMapper.findAuditNum();
    }

}
