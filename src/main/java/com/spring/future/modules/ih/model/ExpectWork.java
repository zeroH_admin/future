package com.spring.future.modules.ih.model;

/**
 * Created by zh on 2017/4/27.
 *
 */
public class ExpectWork {
    private String expectIndustry;
    private String expectPosition;
    private String availableTime;
    private String expectedPeriod;
    private String expectedSalary;
    private String type;

    public String getExpectIndustry() {
        return expectIndustry;
    }

    public void setExpectIndustry(String expectIndustry) {
        this.expectIndustry = expectIndustry;
    }

    public String getExpectPosition() {
        return expectPosition;
    }

    public void setExpectPosition(String expectPosition) {
        this.expectPosition = expectPosition;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public String getExpectedPeriod() {
        return expectedPeriod;
    }

    public void setExpectedPeriod(String expectedPeriod) {
        this.expectedPeriod = expectedPeriod;
    }

    public String getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(String expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
