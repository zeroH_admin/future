package com.spring.future.modules.ih.domain;

import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import org.springframework.data.annotation.Transient;

/**
 * Created by zh on 2017/4/25.
 */
@Entity
@Table(name="ih_job")
public class Job extends DataEntity<Job> {
    //职位名称
    private String name;
    //职位类别
    private String type;
    //工作性质
    private String property;
    //工作经验
    private String experience;
    //学历要求
    private String eduRequest;
    //岗位职责
    private String jobDescription;
    //岗位要求
    private String jobRequest;
    //办公地址
    private String jobAddress;
    //临近轨交
    private String nearSubway;
    //联系人
    private String contact;
    //联系电话
    private String mobilePhone;
    //职位状态、招聘中,已下线,审核中
    private String status;
    //薪资范围
    private String salary;
    //实习日薪
    private String internDaySalary;
    //每周工时
    private String workTimes;
    //实习周期
    private String internPeriod;
    //转正机会
    private String regular;
    //职位属于公司
    private String companyId;
    //发布人
    private String createdById;

    private Company company;
    private Account createdBy;

    private String outId;

    //候选人数
    @Transient
    private Integer candidateNum=0;
    //未面试人数
    @Transient
    private Integer unViewNum=0;
    //面试人数
    @Transient
    private Integer interviewNum=0;

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getExperience() {
        if("不限".equals(experience)){
            experience= "经验不限";
        }
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getEduRequest() {
        if("不限".equals(eduRequest)){
            eduRequest= "学历不限";
        }
        return eduRequest;
    }

    public void setEduRequest(String eduRequest) {
        this.eduRequest = eduRequest;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobRequest() {
        return jobRequest;
    }

    public void setJobRequest(String jobRequest) {
        this.jobRequest = jobRequest;
    }

    public String getJobAddress() {
        return jobAddress;
    }

    public void setJobAddress(String jobAddress) {
        this.jobAddress = jobAddress;
    }

    public String getNearSubway() {
        return nearSubway;
    }

    public void setNearSubway(String nearSubway) {
        this.nearSubway = nearSubway;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getCandidateNum() {
        return candidateNum;
    }

    public void setCandidateNum(Integer candidateNum) {
        this.candidateNum = candidateNum;
    }

    public Integer getUnViewNum() {
        return unViewNum;
    }

    public void setUnViewNum(Integer unViewNum) {
        this.unViewNum = unViewNum;
    }

    public Integer getInterviewNum() {
        return interviewNum;
    }

    public void setInterviewNum(Integer interviewNum) {
        this.interviewNum = interviewNum;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getInternDaySalary() {
        return internDaySalary;
    }

    public void setInternDaySalary(String internDaySalary) {
        this.internDaySalary = internDaySalary;
    }

    public String getWorkTimes() {
        return workTimes;
    }

    public void setWorkTimes(String workTimes) {
        this.workTimes = workTimes;
    }

    public String getInternPeriod() {
        return internPeriod;
    }

    public void setInternPeriod(String internPeriod) {
        this.internPeriod = internPeriod;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public Account getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Account createdBy) {
        this.createdBy = createdBy;
    }
}
