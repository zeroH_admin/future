package com.spring.future.modules.ih.domain;

import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.Const;
import org.springframework.data.annotation.Transient;

import java.time.LocalDateTime;

/**
 * Created by zh on 2017/4/18.
 */

@Entity
@Table(name="ih_account")
public class Account extends DataEntity<Account> {

    private String userName;
    private String password;
    private String type;
    private Integer level;

    private String parentId;
    private String companyId;
    private String userId;
    private String weixinId;

    private Account parent;
    private Company company;
    private User user;
    private Weixin weixin;
    private String outId;
    private String salt;

    //联系人姓名
    private String contact;
    //职位
    private String position;
    //座机号码
    private String landLine;
    //手机号码
    private String mobilePhone;

    private LocalDateTime lastLoginDate;

    @Transient
    private String formatId;
    @Transient
    private String accountType;



    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeixinId() {
        return weixinId;
    }

    public void setWeixinId(String weixinId) {
        this.weixinId = weixinId;
    }

    public Account getParent() {
        return parent;
    }

    public void setParent(Account parent) {
        this.parent = parent;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Weixin getWeixin() {
        return weixin;
    }

    public void setWeixin(Weixin weixin) {
        this.weixin = weixin;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getFormatId() {
        String formatId = "";
        if(Const.USER.equals(type)){
            formatId= "U000"+id;
        }else{
            formatId= "C000"+id;
        }
        return formatId;
    }

    public void setFormatId(String formatId) {
        this.formatId = formatId;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getAccountType() {
        accountType =  "子账号";
        if(level==1){
            accountType= "管理员";
        }
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public LocalDateTime getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(LocalDateTime lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
}
