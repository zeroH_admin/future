package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.spring.future.common.service.ServiceException;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.FileUtils;
import com.spring.future.common.utils.QiniuUpload;
import com.spring.future.common.utils.ThreadLocalContext;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.FolderFile;
import com.spring.future.modules.ih.mapper.FolderFileMapper;
import org.imgscalr.Scalr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class FolderFileService {

    @Value("${root-path}")
    private String rootPath;
    @Autowired
    private FolderFileMapper folderFileMapper;
    @Autowired
    private HttpServletRequest request;

    @Transactional
    public List<String> save(Map<String, MultipartFile> fileMap) {
        List<String> list = Lists.newArrayList();
        try {
            for (MultipartFile multipartFile : fileMap.values()) {
                if (multipartFile.getSize() > 0) {
                    // 组装file
                    FolderFile folderFile = new FolderFile();
                    folderFile.setName(multipartFile.getOriginalFilename().replaceAll("/","."));
                    folderFile.setPhysicalName(UUID.randomUUID().toString() + "." + folderFile.getExtendType());
                    folderFile.setUrl(Const.QINIU_URL+folderFile.getPhysicalName());
                    list.add(folderFile.getUrl());
                    // 上传文件到七牛
                    String uploadPath = getUploadPath(folderFile);
                    File file = new File(uploadPath);
                    multipartFile.transferTo(file);
                    QiniuUpload.upload(file,folderFile.getPhysicalName());
                }
            }
        } catch (Exception e) {
            throw new ServiceException("文件上传失败" + e.getMessage());
        }
        return list;
    }

    public FolderFile findOne(String id) {
        FolderFile folderFile = folderFileMapper.findOne(id);
        folderFile.setPath(getUploadPath(folderFile));
        return folderFile;
    }

    public String getUploadPath(FolderFile folderFile) {
        return  rootPath +"upload/"+ folderFile.getPhysicalName();
    }

    public String getPreviewUploadPath(FolderFile folderFile) {
        return rootPath +"upload/"+ folderFile.getPhysicalName().substring(0, folderFile.getPhysicalName().lastIndexOf(".")) + ".png";
    }

}
