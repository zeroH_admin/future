package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.spring.future.common.leanCloudToMysql.Result;
import com.spring.future.common.leanCloudToMysql.UserResult;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.*;
import com.spring.future.modules.ih.mapper.*;
import com.spring.future.modules.ih.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class UserCloudService {

    @Autowired
    private UserCloudMapper userCloudMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private ResumeMapper resumeMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JobMapper jobMapper;


//    private static String userPath = "D:\\workspaces\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\_User.json";
    private static String resumePath = "D:\\workspaces\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\Resume.json";
//
//    private static String companyPath = "E:\\tmp\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\Company.json";
//    private static String jobPath= "E:\\tmp\\future\\src\\main\\java\\com\\spring\\future\\common\\leanCloudToMysql\\JobPosition.json";
//    @Transactional
//    public void saveCompany(){
//        String companyJson = ReadFile(companyPath);
//        String positionJson = ReadFile(jobPath);
//        Result companyResult  = ObjectMapperUtils.readValue(companyJson,Result.class);
//        UserResult positionResult  = ObjectMapperUtils.readValue(positionJson,UserResult.class);
//
//        System.err.println("skdafj");
//        List<Company> companieList= Lists.newArrayList();
//        List<Job> jobList = Lists.newArrayList();
//        for(CompanyCloud companyCloud:companyResult.getResults()){
//            Company company = new Company();
//            company.setName(companyCloud.getFullName());
//            company.setSimpleName(companyCloud.getShortName());
//            company.setOutId(companyCloud.getObjectId());
//            companieList.add(company);
//        }
//        for(JobPositionCloud jobPositionCloud :positionResult.getResults()){
//            Job job = new Job();
//            job.setName(jobPositionCloud.getPositionName());
//            job.setType(jobPositionCloud.getPositionCategory());
//            job.setProperty("实习");
//            job.setExperience("不限");
//            job.setEduRequest("不限");
//            job.setJobDescription(jobPositionCloud.getDuty());
//            job.setJobRequest(jobPositionCloud.getRequirement());
//            job.setJobAddress(jobPositionCloud.getAddress());
//            job.setNearSubway("1号线"+jobPositionCloud.getMetroStation());
//            job.setStatus("招聘中");
//            job.setSalary(jobPositionCloud.getSalary());
//            job.setInternPeriod(jobPositionCloud.getInternPeriod());
//            job.setOutId(jobPositionCloud.getCompany().getObjectId());
//            job.setRegular("有");
//
//            jobList.add(job);
//        }
//        companyMapper.batchSave(companieList);
//        jobMapper.batchSave(jobList);
//    }

    @Transactional
    public void saveAccount(){
      String userJson = ReadFile(resumePath);
      UserResult userResult  = ObjectMapperUtils.readValue(userJson,UserResult.class);
        userCloudMapper.batchSave(userResult.getResults());
    }


    public void updateResume(){
        String resumeJson = ReadFile(resumePath);
        Result resumeResult  = ObjectMapperUtils.readValue(resumeJson,Result.class);
        List<ResumeCloud> resumeCloudList = resumeResult.getResults();
        List<Resume> resumeList = resumeMapper.findAll();
        Map<String,Resume> resumeMap = Collections3.extractToMap(resumeList,"outId");
        int i=0;
        int j=0;
        for(ResumeCloud resumeCloud:resumeCloudList){
            if(Collections3.isNotEmpty(resumeCloud.getWorkExp())){
                i=i+1;
                System.out.println("have:"+i);
            }
            Resume resume = resumeMap.get(resumeCloud.getUser().getObjectId());
            if(resume!=null){
                List<WorkExperience> workExperiences = Lists.newArrayList();
                for(WorkExpCloud expCloud:resumeCloud.getWorkExp()){
                    WorkExperience workExperience = new WorkExperience();
                    workExperience.setCompanyName(expCloud.getCompany());
                    workExperience.setPositionName(expCloud.getPosition());
                    workExperience.setWorkContent(expCloud.getIntroduction());
                    workExperience.setWorkDateStart(pareseDate(expCloud.getStartTime()));
                    workExperience.setWorkDateEnd(pareseDate(expCloud.getEndTime()));
                    workExperiences.add(workExperience);
                }
                if(Collections3.isNotEmpty(workExperiences)){
                    resume.setWorkExperienceJson(ObjectMapperUtils.writeValueAsString(workExperiences));
                    resumeMapper.update(resume);
                    j=j+1;
                    System.out.println("update:"+j);
                }
            }
        }



    }

//    @Transactional
//    public void save(){
////        String userJson = ReadFile(resumePath);
//        String resumeJson = ReadFile(resumePath);
//        Result resumeResult  = ObjectMapperUtils.readValue(resumeJson,Result.class);
////        UserResult userResult  = ObjectMapperUtils.readValue(userJson,UserResult.class);
//
//        List<ResumeCloud> resumeCloudList = resumeResult.getResults();
////        List<UserCloud> userCloudList = userResult.getResults();
//
//        List<Account> accounts = Lists.newArrayList();
//        List<User> userList = Lists.newArrayList();
//        List<Company> companyList = Lists.newArrayList();
//        List<Resume> resumeList = Lists.newArrayList();
//
//
//        for(ResumeCloud resumeCloud:resumeCloudList){
//            try {
//                Resume resume = new Resume();
//                resume.setName(resumeCloud.getName());
//                resume.setSex(resumeCloud.getGender()=="女士"?"女":"男");
//                resume.setBirthday(pareseDate(resumeCloud.getBirth()));
//                resume.setEmail(resumeCloud.getEmail());
//                resume.setAddress(" ");
//                resume.setNationality(resumeCloud.getNationality());
//                resume.setMobilePhone(resumeCloud.getPhoneNumber());
//                resume.setEducation(Collections3.isEmpty(resumeCloud.getDegree())?"":resumeCloud.getDegree().get(0));
//                resume.setAdvantage(resumeCloud.getOneWordIntroduction());
//                resume.setDescription(resumeCloud.getSelfAssessment());
//                resume.setImage(resumeCloud.getPhoto()==null?"":resumeCloud.getPhoto().getUrl());
//                resume.setOutId(resumeCloud.getUser().getObjectId());
//                List<Education> educations = Lists.newArrayList();
//                for(EducationCloud educationCloud:resumeCloud.getEducation()){
//                    Education education = new Education();
//                    education.setEduDateStart(pareseDate(educationCloud.getEnrollmentDate()));
//                    education.setEduDateEnd(pareseDate(educationCloud.getGraduationDate()));
//                    education.setSchool(educationCloud.getSchool());
//                    education.setMajor(educationCloud.getMajor());
//                    education.setLevel(educationCloud.getDegree());
//                    educations.add(education);
//                }
//                resume.setEducationJson(Collections3.isEmpty(educations)?null:ObjectMapperUtils.writeValueAsString(educations));
//
//                List<WorkExperience> workExperiences = Lists.newArrayList();
//                for(WorkExpCloud expCloud:resumeCloud.getWorkExp()){
//                    WorkExperience workExperience = new WorkExperience();
//                    workExperience.setCompanyName(expCloud.getCompany());
//                    workExperience.setPositionName(expCloud.getPosition());
//                    workExperience.setWorkContent(expCloud.getIntroduction());
//                    workExperience.setWorkDateStart(pareseDate(expCloud.getStartTime()));
//                    workExperience.setWorkDateEnd(pareseDate(expCloud.getEndTime()));
//                    workExperiences.add(workExperience);
//                }
//                resume.setExpectWorkJson(Collections3.isEmpty(workExperiences)?null:ObjectMapperUtils.writeValueAsString(workExperiences));
//
//                List<ExpectWork> expectWorkList = Lists.newArrayList();
//                ExpectWork expectWork = new ExpectWork();
//                expectWork.setExpectIndustry(Collections3.isNotEmpty(resumeCloud.getExpectedIndustry())?resumeCloud.getExpectedIndustry().get(0):"");
//                expectWork.setExpectedPeriod(resumeCloud.getInternPeriod());
//                expectWork.setExpectPosition(Collections3.isNotEmpty(resumeCloud.getExpectedPosition())?resumeCloud.getExpectedPosition().get(0):"");
//                expectWork.setType("实习");
//                expectWork.setAvailableTime(resumeCloud.getDaysPerWeek()+"天/周");
//                expectWorkList.add(expectWork);
//                resume.setExpectWorkJson(Collections3.isEmpty(expectWorkList)?null:ObjectMapperUtils.writeValueAsString(expectWorkList));
//
//                resumeList.add(resume);
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
//        Map<String,Resume> resumeMap = Collections3.extractToMap(resumeList,"outId");
//        for(UserCloud userCloud:userCloudList){
//            //账户
//            Account account = new Account();
//            account.setUserName(userCloud.getUsername());
//            account.setPassword(passwordEncoder.encode(userCloud.getUsername()));
//            account.setType(userCloud.getUserType());
//            account.setLevel(1);
//            account.setOutId(userCloud.getObjectId());
//            accounts.add(account);
//            //user
//            if("intern".equals(userCloud.getUserType())){
//                Resume resume = resumeMap.get(userCloud.getObjectId());
//                if(resume!=null){
//                    User user = new User();
//                    user.setName(userCloud.getName());
//                    user.setImage(resume.getImage());
//                    user.setSex(resume.getSex());
//                    user.setBirthday(resume.getBirthday());
//                    user.setNationality(resume.getNationality());
//                    user.setAddress("");
//                    user.setEmail(resume.getEmail());
//                    user.setMobilePhone(resume.getMobilePhone());
//                    user.setEducation(resume.getEducation());
//                    user.setAdvantage(resume.getAdvantage());
//                    user.setDescription(resume.getDescription());
//                    user.setType("实习");
//                    user.setOutId(userCloud.getObjectId());
//
//                    if(StringUtils.isNotBlank(resume.getEducationJson())){
//                        List<Education> educations = ObjectMapperUtils.readValueToBeanList(resume.getEducationJson(),Education.class);
//                        user.setSchool(educations.get(0).getSchool());
//                        user.setMajor(educations.get(0).getMajor());
//                        user.setEntranceDate(educations.get(0).getEduDateStart());
//                        user.setGraduationDate(educations.get(0).getEduDateEnd());
//                    }
//
//                    if(StringUtils.isNotBlank(resume.getExpectWorkJson())){
//                        List<ExpectWork> expectWorkList = ObjectMapperUtils.readValueToBeanList(resume.getExpectWorkJson(),ExpectWork.class);
//                        ExpectWork expectWork = expectWorkList.get(0);
//                        user.setExpectIndustry(expectWork.getExpectIndustry());
//                        user.setExpectPosition(expectWork.getExpectPosition());
//                        user.setInternTime(expectWork.getAvailableTime());
//                        user.setInternPeriod(expectWork.getExpectedPeriod());
//                    }
//                    userList.add(user);
//                }
//            }
//        }
//
//
//        accountMapper.batchSave(accounts);
//        resumeMapper.batchSave(resumeList);
//        userMapper.batchSave(userList);
//
//    }

    public static String ReadFile(String path) {
        File file = new File(path);
        BufferedReader reader = null;
        String laststr = "";
        try {
            reader = new BufferedReader(new FileReader(file));
            String tempString = null;
            while ((tempString = reader.readLine()) != null) {
                laststr = laststr + tempString;
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                }
            }
        }
        return laststr;
    }

    private LocalDate pareseDate(String dateStr){
        if(StringUtils.isNotBlank(dateStr)){
            if("至今".equals(dateStr)){
                return LocalDate.now();
            }
            dateStr = dateStr.replace(" ","");
            String[] dateArry = null;
            if(dateStr.contains("-")){
                dateArry = dateStr.split("-");
            }else if(dateStr.contains(".")){
                dateArry = dateStr.split("\\.");
            }else if(dateStr.contains("/")){
                dateArry = dateStr.split("\\/");
            }else{
                return LocalDate.now();
            }
            if(dateArry==null){
                System.err.println(dateStr);
            }
            if(dateArry.length==1){
                dateStr = dateArry[0]+"-01-01";
            }else if(dateArry.length==2){
                String month = dateArry[1];
                if(Integer.valueOf(month)<10 && !month.contains("0")){
                    month="0"+month;
                }
                dateStr = dateArry[0]+"-"+month+"-01";
            }else if(dateArry.length==3){
                String month = dateArry[1];
                if(Integer.valueOf(month)<10 && !month.contains("0")){
                    month="0"+month;
                }
                dateStr = dateArry[0]+"-"+month+"-"+dateArry[2];
            }
            return DateUtils.parseLocalDate(dateStr);
        }else{
            return LocalDate.now();
        }
    }

    public static void main(String [] args){
        List<String> list = Lists.newArrayList();
        String resumeJson = ReadFile(resumePath);
        Result result = ObjectMapperUtils.readValue(resumeJson,Result.class);
        for(ResumeCloud resumeCloud :result.getResults()){
            if(resumeCloud.getGender().contains("女士")){
                System.err.println(resumeCloud.getUser().getObjectId());
            }
        }
//        for(String e:list){
//            System.err.println(e);
//        }
    }
}
