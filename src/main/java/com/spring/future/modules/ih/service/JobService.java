package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.enums.JobStatusEnum;
import com.spring.future.common.enums.ResumeStatusEnum;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.*;
import com.spring.future.common.validateCodeUtils.EmailUtils;
import com.spring.future.common.validateCodeUtils.SMSUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.Job;
import com.spring.future.modules.ih.domain.JobResume;
import com.spring.future.modules.ih.mapper.AccountMapper;
import com.spring.future.modules.ih.mapper.CompanyMapper;
import com.spring.future.modules.ih.mapper.JobMapper;
import com.spring.future.modules.ih.mapper.JobResumeMapper;
import com.sun.org.apache.regexp.internal.RE;
import jdk.nashorn.internal.scripts.JO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/25.
 */
@Service
public class JobService {
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private JobResumeMapper jobResumeMapper;
    @Autowired
    private EmailUtils emailUtils;
    @Autowired
    private AccountMapper accountMapper;

    public List<Job> findByStatus(String status){
        return jobMapper.findAll();
    }

    public Page<Job> findPage(Pageable pageable, Map<String, Object>map){
        Page<Job> page = jobMapper.findPage(pageable,map);
        for(Job job:page.getContent()){
            String accountId = AccountUtils.getAccount().getId();
            int candidateNum = jobResumeMapper.findNumByJobAndStatus(job.getId(), ResumeStatusEnum.新简历.name(),accountId);
            int unViewNum = jobResumeMapper.findNumByJobAndStatus(job.getId(),ResumeStatusEnum.待沟通.name(),accountId);
            int interviewNum = jobResumeMapper.findNumByJobAndStatus(job.getId(),ResumeStatusEnum.待面试.name(),accountId);
            job.setCandidateNum(candidateNum);
            job.setUnViewNum(unViewNum);
            job.setInterviewNum(interviewNum);
        }
        return page;
    }

    public Job findOne(String id){
        Job job = jobMapper.findOne(id);
        job.setCompany(companyMapper.findOne(job.getCompanyId()));
        return job;
    }

    public List<Job> findSelfJob(){
        Account account = AccountUtils.getAccount();
        List<Job> jobList = jobMapper.findSelfJob(account.getId());
        return jobList;
    }


    @Transactional
    public void save(Job job){
        Boolean isCreat = job.isCreate();
        if(isCreat){
            Account account = ThreadLocalContext.get().getAccount();
            job.setStatus(JobStatusEnum.审核中.name());
            job.setCompanyId(account.getCompanyId());
            job.setCreatedById(account.getId());
            jobMapper.save(job);
        }else{
            jobMapper.update(job);
        }
    }

    @Transactional
    public void updateStatus(String status,String jobId){
        Job job = jobMapper.findOne(jobId);
        job.setStatus(status);
        jobMapper.update(job);
    }

    /**
     * 后台处理
     *
     */

    public Page<Job> findJobPage(Pageable pageable, Map<String, Object>map){
        String status = (String) map.get("status");
        if("待审核".equals(status)){
            map.put("status","审核中");
        }else if("审核通过".equals(status)){
            map.put("status","招聘中");
        }else{
            map.put("status",status);
        }
        Page<Job> page = jobMapper.findPage(pageable,map);
        if(Collections3.isNotEmpty(page.getContent())){
            List<String> companyIdList = Collections3.extractToList(page.getContent(),"companyId");
            List<Company> companyList = companyMapper.findByIds(companyIdList);
            Map<String,Company> companyMap = Collections3.extractToMap(companyList,"id");

            List<String> accountIds = Collections3.extractToList(page.getContent(),"createdById");
            List<Account> accounts = accountMapper.findByIds(accountIds);
            Map<String,Account> accountMap = Collections3.extractToMap(accounts,"id");
            for(Job job:page.getContent()){
                if(Collections3.isNotEmpty(companyMap)){
                    job.setCompany(companyMap.get(job.getCompanyId()));
                }
                if(Collections3.isNotEmpty(accountMap)){
                    job.setCreatedBy(accountMap.get(job.getCreatedById()));
                }
            }
        }
        return page;
    }


    @Transactional
    public void batchAudit(String[] ids,String status,Boolean email,Boolean sms){
        for(String id:ids){
            auditJob(id,status,email,sms);
        }
    }

    @Transactional
    public void auditJob(String jobId,String status,Boolean email,Boolean sms){
        Job job = jobMapper.findOne(jobId);
        if("审核通过".equals(status)){
            status ="招聘中";
        }
        job.setStatus(status);
        jobMapper.update(job);
        Account account = accountMapper.findOne(job.getCreatedById());
        if(email!=null && email){
            Map<String,Object> map = Maps.newHashMap();
            String emailTitle = "职位审核通知";
            if("审核通过".equals(status)){
                String title = "职位审核成功！";
                String content = "恭喜您，账号:"+account.getUserName()+"发布职位:"+job.getName()+",审核通过，你可以登录www.internhunter.com查看详情";
                map.put("title",title);
                map.put("content",content);
                emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
            }else{
                String title = "职位审核失败！";
                String content = "抱歉，账号:"+account.getUserName()+"发布职位:"+job.getName()+",审核未通过，你可以登录www.internhunter.com查看详情";
                map.put("title",title);
                map.put("content",content);
                emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
            }
        }
        if(sms!=null &&sms){
            if(StringUtils.isNotBlank(account.getMobilePhone())){
                Map<String,String> map = Maps.newHashMap();
                map.put("accountName",account.getUserName());
                map.put("jobName",job.getName());
                if("审核通过".equals(status)){
                    SMSUtils.sendMobileMessage(account.getMobilePhone(), Const.JOB_AUDIT_INFO,map);
                }else{
                    SMSUtils.sendMobileMessage(account.getMobilePhone(),Const.JOB_AUDIT_FAIL_INFO,map);
                }
            }
        }
    }

    public List<Job> finExportData(Map<String,Object> map){
        List<Job> result = Lists.newArrayList();
        String ids =(String) map.get("ids");
        if(StringUtils.isNotBlank(ids)){
            List<String> idsList = Arrays.asList(ids.split(","));
            result = jobMapper.findByIds(idsList);
        }else{
            String status = (String) map.get("status");
            if("待审核".equals(status)){
                map.put("status","审核中");
            }else if("审核通过".equals(status)){
                map.put("status","招聘中");
            }else{
                map.put("status",status);
            }
            result = jobMapper.findData(map);
        }
        if(Collections3.isNotEmpty(result)){
            List<String> companyIdList = Collections3.extractToList(result,"companyId");
            List<Company> companyList = companyMapper.findByIds(companyIdList);
            Map<String,Company> companyMap = Collections3.extractToMap(companyList,"id");

            List<String> accountIds = Collections3.extractToList(result,"createdById");
            List<Account> accounts = accountMapper.findByIds(accountIds);
            Map<String,Account> accountMap = Collections3.extractToMap(accounts,"id");
            for (Job job:result){
                if(Collections3.isNotEmpty(companyMap)){
                    job.getExtendMap().put("companyName",companyMap.get(job.getCompanyId()).getName());
                }
                if(Collections3.isNotEmpty(accountMap)){
                    Account account = accountMap.get(job.getCreatedById());
                    job.getExtendMap().put("userName",account.getUserName());
                    job.getExtendMap().put("contact",account.getContact());
                    job.getExtendMap().put("position",account.getPosition());
                    job.getExtendMap().put("landLine",account.getLandLine());
                    job.getExtendMap().put("mobilePhone",account.getMobilePhone());
                }
            }
        }
        return result;
    }

    public List<Job> findByCompanyIdAndStatus(String companyId,String status){
        return jobMapper.findByCompany(companyId,status);
    }

    public Integer findAuditNum(){
        return jobMapper.findAuditNum();
    }


}
