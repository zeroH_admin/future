package com.spring.future.modules.ih.service;

import com.google.common.collect.Maps;
import com.spring.future.common.enums.ResumeStatusEnum;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.*;
import com.spring.future.common.validateCodeUtils.SMSUtils;
import com.spring.future.modules.ih.domain.*;
import com.spring.future.modules.ih.mapper.*;
import com.spring.future.modules.ih.model.*;
import jdk.nashorn.internal.scripts.JO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class JobResumeService {

    @Autowired
    private JobResumeMapper jobResumeMapper;
    @Autowired
    private ResumeMapper resumeMapper;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private JavaMailSender mailSender;
    @Value("${root-path}")
    private String rootPath;
    @Value("${spring.mail.username}")
    private String emailUserName;


    public JobResume findOne(String id){
        return jobResumeMapper.findOne(id);
    }

    public void updateResumeFirst(String id){
        JobResume jobResume = jobResumeMapper.findOne(id);
        jobResume.setNewResume(false);
        jobResumeMapper.update(jobResume);
    }

    public int findNewResumeNum(){
        return jobResumeMapper.findNewResumeNum(AccountUtils.getAccount().getId());
    }

    public Page<JobResume> findPage(Pageable pageable, Map<String, Object> map){
        map.put("accountId",AccountUtils.getAccountId());
        Page<JobResume> page = jobResumeMapper.findPage(pageable,map);
        Map<String,Resume> resumeMap = Maps.newHashMap();
        Map<String,Job> jobMap = Maps.newHashMap();
        List<String> resumeIdList = Collections3.extractToList(page.getContent(),"resumeId");
        List<String> jobIdList = Collections3.extractToList(page.getContent(),"jobId");
        if(Collections3.isNotEmpty(resumeIdList)){
            List<Resume> resumeList = resumeMapper.findByIds(resumeIdList);
            resumeMap = Collections3.extractToMap(resumeList,"id");
        }
        if(Collections3.isNotEmpty(jobIdList)){
            List<Job> jobList = jobMapper.findByIds(jobIdList);
            jobMap = Collections3.extractToMap(jobList,"id");
        }
        for(JobResume jobResume:page.getContent()){
            jobResume.setAccount(AccountUtils.getAccount());
            if(resumeMap.containsKey(jobResume.getResumeId())){
                jobResume.setResume(resumeMap.get(jobResume.getResumeId()));
            }
            if(jobMap.containsKey(jobResume.getJobId())){
                jobResume.setJob(jobMap.get(jobResume.getJobId()));
            }
        }
        return page;
    }

    @Transactional
    public void failResume(String id){
        JobResume jobResume = jobResumeMapper.findOne(id);
        jobResume.setStatus(ResumeStatusEnum.淘汰.name());
        jobResumeMapper.update(jobResume);
    }

    @Transactional
    public void updateStatus(String id,String status){
        JobResume jobResume = jobResumeMapper.findOne(id);
        jobResume.setStatus(status);
        jobResumeMapper.update(jobResume);
    }

    @Transactional
    public void sendView(String id,String address,String contact,String mobilePhone,String date){
        JobResume jobResume = jobResumeMapper.findOne(id);
        jobResume.setStatus(ResumeStatusEnum.待面试.name());
        date = date+":00";
        jobResume.setInterviewDate(DateUtils.parseLocalDateTime(date));
        jobResumeMapper.update(jobResume);
        //发送短信
        Resume resume = resumeMapper.findOne(jobResume.getResumeId());
        Company company = companyMapper.findOne(jobResume.getCompanyId());
        Job job = jobMapper.findOne(jobResume.getJobId());
        //面试邀请电话短信通知
        if(StringUtils.isNotBlank(resume.getMobilePhone())){
            Map<String,String> map = Maps.newHashMap();
            map.put("name",resume.getName());
            map.put("companyName",company.getName());
            map.put("date", date);
            map.put("positionName",job.getName());
            map.put("address",address);
            map.put("mobilePhone",contact+":"+mobilePhone);
            SMSUtils.sendMobileMessage(resume.getMobilePhone(),Const.SEND_VIEW,map);
        }
        //面试邀请邮件通知
        if(StringUtils.isNotBlank(resume.getEmail())){
            String content = company.getName()+"邀请您于"+date+"面试"+job.getName()+"一职，地址："+address+"，联系电话："+mobilePhone+"，联系人："+contact;
            Map<String,Object> emailMap = Maps.newHashMap();
            emailMap.put("name",resume.getName());
            emailMap.put("companyName",company.getName());
            emailMap.put("content",content);
            try {
                String html = HtmlGenerator.generate(rootPath+"template/", "internview", emailMap);

                String title = company.getName()+"面试邀请";
                MimeMessage mimeMessage = mailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
                String nick="";
                try {
                    nick=javax.mail.internet.MimeUtility.encodeText("IH招聘");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                helper.setFrom(new InternetAddress(nick+" <"+emailUserName+">"));
                helper.setTo(resume.getEmail());
                helper.setSubject(title);
                helper.setText(html, true);
                FileSystemResource fileSystemResource = new FileSystemResource(new File(rootPath+"template/bac.png"));
                helper.addInline("weixin", fileSystemResource);
                mailSender.send(mimeMessage);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public JobResume viewResume(String id){
        JobResume jobResume = jobResumeMapper.findOne(id);
        Resume resume = resumeMapper.findOne(jobResume.getResumeId());
        Job job = jobMapper.findOne(jobResume.getJobId());
        job.setCreatedBy(accountMapper.findOne(job.getCreatedById()));
        jobResume.setResume(resume);
        jobResume.setJob(job);
        jobResume.setCompany(AccountUtils.getCompany());
        return jobResume;
    }

    @Transactional
    public void  forwardSend(String id,String email,String remarks){
        JobResume jobResume = jobResumeMapper.findOne(id);
        Resume resume = resumeMapper.findOne(jobResume.getResumeId());
        Job job = jobMapper.findOne(jobResume.getJobId());
        List<Account> accounts = accountMapper.findByCompany(AccountUtils.getCompanyId());
        Map<String,Account> map = Collections3.extractToMap(accounts,"userName");
        List<String> emailList = Collections3.extractToList(accounts,"userName");
        for(String e:email.split(",")){
            if(emailList.contains(e)){
                //输入的邮箱是已注册的账号
                Account account = map.get(e);
                JobResume temp = new JobResume();
                temp.setAccountId(account.getId());
                temp.setJobId(jobResume.getJobId());
                temp.setResumeId(jobResume.getResumeId());
                temp.setCompanyId(jobResume.getCompanyId());
                temp.setStatus("新简历");
                temp.setType("同事转发的简历");
                temp.setResumeFrom(jobResume.getResumeFrom());
                temp.setNewResume(true);
                temp.setForword(true);
                temp.setRemarks(remarks);
                jobResumeMapper.save(temp);
            }
            //发送通知邮件
            sendEmail(resume,job,e);
        }
    }

    private void sendEmail(Resume resume,Job job,String email){
        Map<String, Object> map = Maps.newHashMap();
        map.put("resume", resume);
        map.put("job", job);
        map.put("fromName", AccountUtils.getAccount().getContact());
        try {
            String html = HtmlGenerator.generate(rootPath+"template/", "forword", map);

            String title = AccountUtils.getAccount().getContact()+"推荐了候选人【"+job.getName()+"】"+resume.getName()+"的简历给你";
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            String nick="";
            try {
                nick=javax.mail.internet.MimeUtility.encodeText("IH招聘");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            helper.setFrom(new InternetAddress(nick+" <"+emailUserName+">"));
            helper.setTo(email);
            helper.setSubject(title);
            helper.setText(html, true);
            FileSystemResource fileSystemResource = new FileSystemResource(new File(rootPath+"template/bac.png"));
            helper.addInline("weixin", fileSystemResource);
            mailSender.send(mimeMessage);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<JobResume> findAll(){
        return jobResumeMapper.findAll();
    }

    private void initResume(Resume resume){
        if(StringUtils.isNotBlank(resume.getEducationJson())){
            resume.setEducationList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getEducationJson()),Education.class));
        }
        if(StringUtils.isNotBlank(resume.getWorkExperienceJson())){
            resume.setWorkExperienceList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getWorkExperienceJson()),WorkExperience.class));
        }
        if(StringUtils.isNotBlank(resume.getSchoolExperienceJson())){
            resume.setSchoolExperienceList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getSchoolExperienceJson()),SchoolExperience.class));
        }
        if(StringUtils.isNotBlank(resume.getSkillJson())){
            resume.setSkillList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getSkillJson()),Skill.class));
        }
        if(StringUtils.isNotBlank(resume.getLanguageJson())){
            resume.setLanguageList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getLanguageJson()),Language.class));
        }
        if(StringUtils.isNotBlank(resume.getCertificateJson())){
            resume.setCertificateList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getCertificateJson()),Certificate.class));
        }
        if(StringUtils.isNotBlank(resume.getExpectWorkJson())){
            resume.setExpectWorkList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getExpectWorkJson()),ExpectWork.class));
        }
    }

    /**
     * 后台逻辑
     */

    public Page<JobResume> findPushPage(Pageable pageable,Map<String,Object>map){
        Page<JobResume> page = jobResumeMapper.findPushPage(pageable,map);
        if(Collections3.isNotEmpty(page.getContent())){
            List<String> resumeIdList = Collections3.extractToList(page.getContent(),"resumeId");
            List<String> JobIds = Collections3.extractToList(page.getContent(),"jobId");
            List<String> companyIds = Collections3.extractToList(page.getContent(),"companyId");
            Map<String,Resume> resumeMap = Maps.newHashMap();
            Map<String,Job> jobMap = Maps.newHashMap();
            Map<String,Company> companyMap = Maps.newHashMap();
            if(Collections3.isNotEmpty(resumeIdList)){
                List<Resume> resumeList = resumeMapper.findByIds(resumeIdList);
                List<String> userIds = Collections3.extractToList(resumeList,"userId");
                if(Collections3.isNotEmpty(userIds)){
                    List<User> userList = userMapper.findByIds(userIds);
                    Map<String,User> userMap = Collections3.extractToMap(userList,"id");
                    for(Resume resume:resumeList){
                        resume.setUser(userMap.get(resume.getUserId()));
                    }
                }
                resumeMap = Collections3.extractToMap(resumeList,"id");
            }
            if(Collections3.isNotEmpty(JobIds)){
                List<Job> jobList = jobMapper.findByIds(JobIds);
                jobMap = Collections3.extractToMap(jobList,"id");
            }
            if(Collections3.isNotEmpty(companyIds)){
                List<Company> companyList = companyMapper.findByIds(companyIds);
                companyMap = Collections3.extractToMap(companyList,"id");
            }
            for(JobResume jobResume:page.getContent()){
                if(Collections3.isNotEmpty(resumeMap)){
                    jobResume.setResume(resumeMap.get(jobResume.getResumeId()));
                }
                if(Collections3.isNotEmpty(jobMap)){
                    jobResume.setJob(jobMap.get(jobResume.getJobId()));
                }
                if(Collections3.isNotEmpty(companyMap)){
                    jobResume.setCompany(companyMap.get(jobResume.getCompanyId()));
                }
            }
        }
        return page;
    }

    public List<JobResume> find2JobResume(){
        List<JobResume> list = jobResumeMapper.find2JobResume(AccountUtils.getAccountId());
        for(JobResume jobResume:list){
            Job job = jobMapper.findOne(jobResume.getJobId());
            Company company = companyMapper.findOne(jobResume.getCompanyId());
            jobResume.setJob(job);
            jobResume.setCompany(company);
        }
        return list;
    }

    public Page<JobResume> findInternPushList(Pageable pageable, Map<String, Object> map){
        Resume resume = resumeMapper.findByUserId(AccountUtils.getAccount().getUserId());
        map.put("resumeId",resume.getId());
        Page<JobResume> page = jobResumeMapper.findInternPage(pageable,map);
        Map<String,Company> companyMap = Maps.newHashMap();
        Map<String,Job> jobMap = Maps.newHashMap();
        List<String> companyIdList = Collections3.extractToList(page.getContent(),"companyId");
        List<String> jobIdList = Collections3.extractToList(page.getContent(),"jobId");
        if(Collections3.isNotEmpty(companyIdList)){
            List<Company> companyList = companyMapper.findByIds(companyIdList);
            companyMap = Collections3.extractToMap(companyList,"id");
        }
        if(Collections3.isNotEmpty(jobIdList)){
            List<Job> jobList = jobMapper.findByIds(jobIdList);
            jobMap = Collections3.extractToMap(jobList,"id");
        }
        for(JobResume jobResume:page.getContent()){
            jobResume.setAccount(AccountUtils.getAccount());
            if(companyMap.containsKey(jobResume.getCompanyId())){
                jobResume.setCompany(companyMap.get(jobResume.getCompanyId()));
            }
            if(jobMap.containsKey(jobResume.getJobId())){
                jobResume.setJob(jobMap.get(jobResume.getJobId()));
            }
        }
        return page;
    }

}
