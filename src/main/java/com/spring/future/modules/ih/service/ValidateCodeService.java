package com.spring.future.modules.ih.service;

import com.google.common.collect.Maps;
import com.spring.future.common.utils.HtmlGenerator;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.common.validateCodeUtils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Created by zh on 2017/4/20.
 */
@Service
public class ValidateCodeService {
    @Autowired
    private JavaMailSender mailSender;
    @Value("${root-path}")
    private String rootPath;
    @Value("${spring.mail.username}")
    private String emailUserName;

    /**
     * 验证码的发送
     * @param request
     * @param response
     * @param userName
     * @return
     * @throws Exception
     */
    public Boolean sendMobileCode(HttpServletRequest request, HttpServletResponse response, String userName)throws Exception{
        Boolean result = false;
        String code = getRandNum(1,999999);
        if(StringUtils.isEmail(userName)){
            //发送邮件验证码
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            String nick="";
            try {
                nick=javax.mail.internet.MimeUtility.encodeText("IH招聘");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            helper.setFrom(new InternetAddress(nick+" <"+emailUserName+">"));
            helper.setTo(userName);
            helper.setSubject("Intern Hunter验证码");
            helper.setText(getTemplateHtml(code), true);
            FileSystemResource file = new FileSystemResource(new File(rootPath+"template/bac.png"));
            helper.addInline("weixin", file);
            mailSender.send(mimeMessage);
        }else{
            //发送短信验证码
            SMSUtils.sendMobileCode(userName,code);
        }
        //保存到session
        HttpSession session =  request.getSession();
        Map<String,Object> timeCodeMap = Maps.newHashMap();
        timeCodeMap.put("code",code);
        timeCodeMap.put("expiredDate", LocalDateTime.now().plusMinutes(5));//设置超时时间为5分钟
        session.setAttribute(userName, timeCodeMap);
        //计算次数
        String signName = userName+"_num";
        if(session.getAttribute(signName)!=null){
            int num = (int) session.getAttribute(signName);
            session.setAttribute(signName,num+1);
            System.err.println("num:"+session.getAttribute(signName));
            if(num>4){result=true;}
        }else{
            session.setAttribute(signName,1);
        }
        return result;
    }


    public static String getRandNum(int min, int max) {
        int randNum = min + (int)(Math.random() * ((max - min) + 1));
        return Integer.toString(randNum);
    }

    private String getTemplateHtml(String code){
        String html  = "";
        Map<String, Object> map = Maps.newHashMap();
        map.put("code", code);
        try {
            html = HtmlGenerator.generate(rootPath+"template/", "yzm", map);
        }catch (Exception e){
            e.printStackTrace();
        }
        return html;
    }
}
