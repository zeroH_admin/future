package com.spring.future.modules.ih.web;

import com.spring.future.common.utils.StringUtils;
import com.spring.future.common.validateCodeUtils.ValidateCode;
import com.spring.future.modules.ih.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@RestController
public class ValidateCodeController {
    @Autowired
    private ValidateCodeService validateCodeService;

    /**
     * 图形验证码的生成
     * @param request
     * @param response
     * @param timestamp
     * @return
     * @throws Exception
     */
    @RequestMapping(value="register/validateCode")
    public String validateCode(HttpServletRequest request, HttpServletResponse response,String timestamp) throws Exception{
        // 设置响应的类型格式为图片格式
        response.setContentType("image/jpeg");
        //禁止图像缓存。
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        HttpSession session = request.getSession();
        ValidateCode vCode = new ValidateCode(120,40,5,100);
        session.setAttribute("imageCode", vCode.getCode());
        vCode.write(response.getOutputStream());
        return vCode.getCode();
    }

    /**
     * 验证图形验证码的对错
     * @param request
     * @param response
     * @param code
     * @return
     */
    @RequestMapping(value="register/checkCode")
    public Boolean checkCode(HttpServletRequest request, HttpServletResponse response,String code){
        HttpSession session = request.getSession();
        String sessionCode = (String) session.getAttribute("imageCode");
        Boolean result = true;
        //忽略验证码大小写
        if (!StringUtils.equalsIgnoreCase(code, sessionCode)) {
            result = true;
        }
        return result;
    }

    /**
     * 获取邮箱，手机验证码，超过三次的点击则显示图形验证码
     * @param request
     * @param response
     * @param userName
     * @return
     */
    @RequestMapping(value="register/getMobileCode")
    public Boolean getMobileCode(HttpServletRequest request, HttpServletResponse response,String userName) throws Exception{
        Boolean result = validateCodeService.sendMobileCode(request,response,userName);
        return result;
    }

    /**
     * 校验邮箱，手机号码的邮箱
     * @param request
     * @param response
     * @param userName
     * @param code
     * @return
     */
    @RequestMapping(value="register/checkMobileCode")
    public Boolean checkMobileCode(HttpServletRequest request, HttpServletResponse response,String userName,String code){
        Boolean result = true;
        HttpSession session = request.getSession();
        Map map =(Map)session.getAttribute(userName);
        String sessionCode = (String) map.get("code");
        LocalDateTime expiredDate = (LocalDateTime) map.get("expiredDate");
        if(!code.equalsIgnoreCase(sessionCode) || LocalDateTime.now().isAfter(expiredDate)){
            result = false;
        }
        return result;
    }



}
