package com.spring.future.modules.ih.model;

import java.time.LocalDate;

/**
 * Created by zh on 2017/4/27.
 */
public class Education {

    private LocalDate eduDateStart;
    private LocalDate eduDateEnd;
    private String school;
    private String major;
    private String level;

    public LocalDate getEduDateStart() {
        return eduDateStart;
    }

    public void setEduDateStart(LocalDate eduDateStart) {
        this.eduDateStart = eduDateStart;
    }

    public LocalDate getEduDateEnd() {
        return eduDateEnd;
    }

    public void setEduDateEnd(LocalDate eduDateEnd) {
        this.eduDateEnd = eduDateEnd;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
