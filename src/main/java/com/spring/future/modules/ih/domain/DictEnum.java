package com.spring.future.modules.ih.domain;


import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;

@Entity
@Table(name="ih_dict_enum")
public class DictEnum extends DataEntity<DictEnum> {
    private Integer sort;
    private String category;
    private String value;
    private Integer version = 0;

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}
