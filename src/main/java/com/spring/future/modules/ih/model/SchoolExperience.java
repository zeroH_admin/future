package com.spring.future.modules.ih.model;

import java.time.LocalDate;

/**
 * Created by zh on 2017/4/27.
 */
public class SchoolExperience {

    private LocalDate schoolDateStart;
    private LocalDate schoolDateEnd;
    private String itemName;
    private String itemAddress;
    private String itemContent;

    public LocalDate getSchoolDateStart() {
        return schoolDateStart;
    }

    public void setSchoolDateStart(LocalDate schoolDateStart) {
        this.schoolDateStart = schoolDateStart;
    }

    public LocalDate getSchoolDateEnd() {
        return schoolDateEnd;
    }

    public void setSchoolDateEnd(LocalDate schoolDateEnd) {
        this.schoolDateEnd = schoolDateEnd;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemAddress() {
        return itemAddress;
    }

    public void setItemAddress(String itemAddress) {
        this.itemAddress = itemAddress;
    }

    public String getItemContent() {
        return itemContent;
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
    }
}
