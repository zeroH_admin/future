package com.spring.future.modules.ih.domain;

import com.google.common.collect.Lists;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.DateUtils;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.model.*;
import org.springframework.data.annotation.Transient;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by zh on 2017/4/27.
 * 简历表
 */
@Entity
@Table(name="ih_resume")
public class Resume extends DataEntity<Resume> {
    //头像
    private String image;
    //姓名
    private String name;
    //性别
    private String sex;
    //生日
    private LocalDate birthday;
    //邮箱
    private String email;
    //国籍
    private String nationality;
    //手机号码
    private String mobilePhone;
    //最高学历
    private String education;
    //居住地
    private String address;
    //个人简介
    private String advantage;
    //自我描述
    private String description;
    //证件类型   身份证，护照
    private String cardType;
    //证件号码
    private String idcard;
    //QQ号码
    private String qq;
    //微信号
    private String wechat;

    //教育经历  json
    private String educationJson;
    //工作经验
    private String workExperienceJson;
    //校园经历
    private String schoolExperienceJson;
    //技能特长
    private String skillJson;
    //语言特长
    private String languageJson;
    //证书
    private String certificateJson;
    //期望工作
    private String expectWorkJson;
    //工作经验
    private String workYear;

    private String accountId;
    private String userId;
    private User user;

    private String outId;

    @Transient
    private String age;
    @Transient
    private String percent;

    private Account account;

    private String auditStatus;

    private LocalDateTime auditDate;

    private List<Education> educationList = Lists.newArrayList();
    private List<WorkExperience> workExperienceList = Lists.newArrayList();
    private List<SchoolExperience> schoolExperienceList = Lists.newArrayList();
    private List<Skill> skillList = Lists.newArrayList();
    private List<Language> languageList = Lists.newArrayList();
    private List<Certificate> certificateList = Lists.newArrayList();
    private List<ExpectWork> expectWorkList = Lists.newArrayList();

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getEducationJson() {
        return educationJson;
    }

    public void setEducationJson(String educationJson) {
        this.educationJson = educationJson;
    }

    public String getWorkExperienceJson() {
        return workExperienceJson;
    }

    public void setWorkExperienceJson(String workExperienceJson) {
        this.workExperienceJson = workExperienceJson;
    }

    public String getSchoolExperienceJson() {
        return schoolExperienceJson;
    }

    public void setSchoolExperienceJson(String schoolExperienceJson) {
        this.schoolExperienceJson = schoolExperienceJson;
    }

    public String getSkillJson() {
        return skillJson;
    }

    public void setSkillJson(String skillJson) {
        this.skillJson = skillJson;
    }

    public String getLanguageJson() {
        return languageJson;
    }

    public void setLanguageJson(String languageJson) {
        this.languageJson = languageJson;
    }

    public String getCertificateJson() {
        return certificateJson;
    }

    public void setCertificateJson(String certificateJson) {
        this.certificateJson = certificateJson;
    }

    public String getExpectWorkJson() {
        return expectWorkJson;
    }

    public void setExpectWorkJson(String expectWorkJson) {
        this.expectWorkJson = expectWorkJson;
    }

    public String getWorkYear() {
        return workYear;
    }

    public void setWorkYear(String workYear) {
        this.workYear = workYear;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Education> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<Education> educationList) {
        this.educationList = educationList;
    }

    public List<WorkExperience> getWorkExperienceList() {
        return workExperienceList;
    }

    public void setWorkExperienceList(List<WorkExperience> workExperienceList) {
        this.workExperienceList = workExperienceList;
    }

    public List<SchoolExperience> getSchoolExperienceList() {
        return schoolExperienceList;
    }

    public void setSchoolExperienceList(List<SchoolExperience> schoolExperienceList) {
        this.schoolExperienceList = schoolExperienceList;
    }

    public List<Skill> getSkillList() {
        return skillList;
    }

    public void setSkillList(List<Skill> skillList) {
        this.skillList = skillList;
    }

    public List<Language> getLanguageList() {
        return languageList;
    }

    public void setLanguageList(List<Language> languageList) {
        this.languageList = languageList;
    }

    public List<Certificate> getCertificateList() {
        return certificateList;
    }

    public void setCertificateList(List<Certificate> certificateList) {
        this.certificateList = certificateList;
    }

    public List<ExpectWork> getExpectWorkList() {
        return expectWorkList;
    }

    public void setExpectWorkList(List<ExpectWork> expectWorkList) {
        this.expectWorkList = expectWorkList;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getAge() {
        if(birthday!=null){
            age= DateUtils.getAgeByBirthDay(birthday);
        }
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public LocalDateTime getAuditDate() {
        return auditDate;
    }

    public void setAuditDate(LocalDateTime auditDate) {
        this.auditDate = auditDate;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
}
