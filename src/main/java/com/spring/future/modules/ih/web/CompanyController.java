package com.spring.future.modules.ih.web;

import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/19.
 */
@Controller
public class CompanyController {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private AccountService accountService;


    @RequestMapping(value = "hr/company-info")
    public String companyInfo(Model model){
        Account account = ThreadLocalContext.get().getAccount();
        Company company = companyService.findOne(account.getCompanyId());
        model.addAttribute("company",company);
        return "ih/company";
    }

    @RequestMapping(value = "hr/company-worker")
    public String companyWorker(Model model){
        List<Account> accountList = accountService.findCompanyAccount();
        model.addAttribute("accountList",accountList);
        Company company = companyService.findOne(AccountUtils.getCompanyId());
        model.addAttribute("company",company);
        model.addAttribute("self",AccountUtils.getAccount());
        return "ih/worker";
    }

    @RequestMapping(value = "hr/updateAccountStatus")
    public String updateAccountStatus(Model model,String id){
        Account account = accountService.findOne(id);
        if(account.getEnabled()){
            account.setEnabled(false);
        }else{
            account.setEnabled(true);
        }
        accountService.update(account);
        return "redirect:/hr/company-worker";
    }

    @RequestMapping(value = "/hr/updateCompanyInfo")
    public String updateCompanyInfo(Company company){
        companyService.updateInfo(company);
        return "redirect:/hr/company-info";
    }

    @RequestMapping(value = "/hr/uploadLogo")
    @ResponseBody
    public String uploadLogo(MultipartHttpServletRequest request, HttpServletResponse response){
        Map<String, MultipartFile> fileMap = request.getFileMap();
        List<String> logoUrl = companyService.companyLogo(fileMap);
        return ObjectMapperUtils.writeValueAsString(logoUrl);
    }

}
