package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Weixin;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by zh on 2017/4/19.
 */
@Mapper
public interface WeixinMapper extends CrudMapper<Weixin,String> {

    Weixin findByOpenid(String openid);

}
