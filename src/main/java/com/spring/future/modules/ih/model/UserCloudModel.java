package com.spring.future.modules.ih.model;

/**
 * Created by zh on 2017/5/4.
 */
public class UserCloudModel {
    private String objectId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
