package com.spring.future.modules.ih.model;

import java.time.LocalDate;

/**
 * Created by zh on 2017/4/27.
 */
public class WorkExperience {
    private LocalDate workDateStart;
    private LocalDate workDateEnd;
    private String companyName;
    private String positionName;
    private String workContent;

    public LocalDate getWorkDateStart() {
        return workDateStart;
    }

    public void setWorkDateStart(LocalDate workDateStart) {
        this.workDateStart = workDateStart;
    }

    public LocalDate getWorkDateEnd() {
        return workDateEnd;
    }

    public void setWorkDateEnd(LocalDate workDateEnd) {
        this.workDateEnd = workDateEnd;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getWorkContent() {
        return workContent;
    }

    public void setWorkContent(String workContent) {
        this.workContent = workContent;
    }
}
