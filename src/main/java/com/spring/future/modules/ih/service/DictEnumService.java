package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.spring.future.modules.ih.mapper.DictEnumMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class DictEnumService {

    @Autowired
    private DictEnumMapper dictEnumMapper;

    public List<String> findValueByCategory(String category){
        return  dictEnumMapper.findValueByCategory(category);
    }


}
