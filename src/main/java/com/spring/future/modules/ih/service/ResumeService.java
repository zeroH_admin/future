package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.enums.JobResumeFrom;
import com.spring.future.common.enums.JobResumeType;
import com.spring.future.common.enums.ResumeStatusEnum;
import com.spring.future.common.enums.UserTagEnum;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.*;
import com.spring.future.common.validateCodeUtils.EmailUtils;
import com.spring.future.common.validateCodeUtils.SMSUtils;
import com.spring.future.modules.ih.domain.*;
import com.spring.future.modules.ih.mapper.*;
import com.spring.future.modules.ih.model.*;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class ResumeService {

    @Autowired
    private ResumeMapper resumeMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private JobResumeMapper jobResumeMapper;
    @Autowired
    private JobMapper jobMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private EmailUtils emailUtils;

    public Resume getAccountResume(Boolean init){
        Account account = ThreadLocalContext.get().getAccount();
       Resume resume =  resumeMapper.findByUserId(account.getUserId());
       if(init){
           initResume(resume);
       }
       return resume;
    }

    public Resume findOne(String id){
        Resume resume = resumeMapper.findOne(id);
        initResume(resume);
        return resume;
    }

    @Transactional
    public void save(Resume resume){
        Account account = ThreadLocalContext.get().getAccount();
        User user = userMapper.findOne(account.getUserId());
        Resume temp = resumeMapper.findByUserId(account.getUserId());
        if(temp==null){
            temp = new Resume();
            temp.setUserId(account.getUserId());
        }
        temp.setImage(resume.getImage());
        temp.setName(resume.getName());
        temp.setSex(resume.getSex());
        temp.setWorkYear(resume.getWorkYear());
        temp.setBirthday(resume.getBirthday());
        temp.setEmail(resume.getEmail());
        temp.setNationality(resume.getNationality());
        temp.setMobilePhone(resume.getMobilePhone());
        temp.setEducation(resume.getEducation());
        temp.setAddress(resume.getAddress());
        temp.setAdvantage(resume.getAdvantage());
        temp.setDescription(resume.getDescription());
        temp.setCardType(resume.getCardType());
        temp.setIdcard(resume.getIdcard());
        temp.setQq(resume.getQq());
        temp.setWechat(resume.getWechat());
        //设置user信息
        user.setImage(resume.getImage());
        user.setName(resume.getName());
        user.setSex(resume.getSex());
        user.setBirthday(resume.getBirthday());
        user.setEmail(resume.getEmail());
        user.setNationality(resume.getNationality());
        user.setMobilePhone(resume.getMobilePhone());
        user.setEducation(resume.getEducation());
        user.setAddress(resume.getAddress());
        user.setAdvantage(resume.getAdvantage());
        user.setDescription(resume.getDescription());
        if(StringUtils.isNotBlank(resume.getEducationJson())){
            temp.setEducationJson(HtmlUtils.htmlUnescape(resume.getEducationJson()));
        }

        if(StringUtils.isNotBlank(resume.getWorkExperienceJson())){
            temp.setWorkExperienceJson(HtmlUtils.htmlUnescape(resume.getWorkExperienceJson()));
        }

        if(StringUtils.isNotBlank(resume.getSchoolExperienceJson())){
            temp.setSchoolExperienceJson(HtmlUtils.htmlUnescape(resume.getSchoolExperienceJson()));
        }

        if(StringUtils.isNotBlank(resume.getSkillJson())){
            temp.setSkillJson(HtmlUtils.htmlUnescape(resume.getSkillJson()));
        }

        if(StringUtils.isNotBlank(resume.getLanguageJson())){
            temp.setLanguageJson(HtmlUtils.htmlUnescape(resume.getLanguageJson()));
        }

        if(StringUtils.isNotBlank(resume.getCertificateJson())){
            temp.setCertificateJson(HtmlUtils.htmlUnescape(resume.getCertificateJson()));
        }

        if(StringUtils.isNotBlank(resume.getExpectWorkJson())){
            temp.setExpectWorkJson(HtmlUtils.htmlUnescape(resume.getExpectWorkJson()));
        }
        List<ExpectWork> list = ObjectMapperUtils.readValueToBeanList(temp.getExpectWorkJson(),ExpectWork.class);
        if(Collections3.isNotEmpty(list)){
            user.setExpectPosition(list.get(0).getExpectPosition());
            user.setExpectIndustry(list.get(0).getExpectIndustry());
        }
        userMapper.update(user);
        Boolean isCreate = temp.isCreate();
        if(isCreate){
            resumeMapper.save(temp);
        }else{
            resumeMapper.update(temp);
        }
    }

    public Resume downLoadResume(String id){
        Resume resume = resumeMapper.findOne(id);
        initResume(resume);
        return resume;
    }

    private void initResume(Resume resume){
        if(StringUtils.isNotBlank(resume.getEducationJson())){
            resume.setEducationList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getEducationJson()),Education.class));
        }
        if(StringUtils.isNotBlank(resume.getWorkExperienceJson())){
            resume.setWorkExperienceList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getWorkExperienceJson()),WorkExperience.class));
        }
        if(StringUtils.isNotBlank(resume.getSchoolExperienceJson())){
            resume.setSchoolExperienceList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getSchoolExperienceJson()),SchoolExperience.class));
        }
        if(StringUtils.isNotBlank(resume.getSkillJson())){
            resume.setSkillList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getSkillJson()),Skill.class));
        }
        if(StringUtils.isNotBlank(resume.getLanguageJson())){
            resume.setLanguageList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getLanguageJson()),Language.class));
        }
        if(StringUtils.isNotBlank(resume.getCertificateJson())){
            resume.setCertificateList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getCertificateJson()),Certificate.class));
        }
        if(StringUtils.isNotBlank(resume.getExpectWorkJson())){
            resume.setExpectWorkList(ObjectMapperUtils.readValueToBeanList(HtmlUtils.htmlUnescape(resume.getExpectWorkJson()),ExpectWork.class));
        }
    }


    /**
     * 后台
     */

    public Page<Resume> findPage(Pageable pageable,Map<String,Object> map){
        Page<Resume> page = resumeMapper.findPage(pageable,map);
        List<String> userIdList = Collections3.extractToList(page.getContent(),"userId");
        if(Collections3.isNotEmpty(page.getContent())&& Collections3.isNotEmpty(userIdList)){
            List<User> userList = userMapper.findByIds(userIdList);
            List<String> accountIds = Collections3.extractToList(userList,"accountId");
            if(Collections3.isNotEmpty(accountIds)){
                List<Account> accounts = accountMapper.findByIds(accountIds);
                Map<String,User> userMap = Collections3.extractToMap(userList,"id");
                Map<String,Account> accountMap = Collections3.extractToMap(accounts,"userId");
                if(Collections3.isNotEmpty(accountMap)){
                    for(Resume resume:page.getContent()){
                        resumeIntegrity(resume);
                        resume.setAccount(accountMap.get(resume.getUserId()));
                        resume.setUser(userMap.get(resume.getUserId()));
                    }
                }
            }
        }
        return page;
    }

    @Transactional
    public void setTag(String id,String tag){
        Resume resume = resumeMapper.findOne(id);
        User user = userMapper.findOne(resume.getUserId());
        user.setTag(tag);
        userMapper.update(user);
    }

    @Transactional
    public void audit(String id,String status,Boolean sms,Boolean email){
        Resume resume=resumeMapper.findOne(id);
        User user = userMapper.findOne(resume.getUserId());
        Account account = accountMapper.findOne(user.getAccountId());
        if(account.getLocked()){
            account.setLocked(false);
            accountMapper.update(account);
        }
        resume.setAuditStatus(status);
        resume.setAuditDate(LocalDateTime.now());
        resumeMapper.update(resume);
        //简历审核通知
        if(sms){
            Map<String,String> map = Maps.newHashMap();
            map.put("name",resume.getName());
            SMSUtils.sendMobileMessage(resume.getMobilePhone(),Const.RESUME_AUDIT_SMS,map);
        }
        if(email){
            Map<String,Object> map = Maps.newHashMap();
            String emailTitle = "Intern Hunter 简历审核结果通知";
            String title = account.getContact()+"同学，您好";
            String content = "感谢申请Intern Hunter会员。\n" +
                    "请等待Intern Club顾问添加您的微信并进行电话面试，面试通过后即可完善您的个人档案。";
            map.put("title",title);
            map.put("content",content);
            emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
        }
    }

    public Integer findAuditResume(){
        return resumeMapper.findAuditResume();
    }


    public List<Resume> finExportData(Map<String,Object> map){
        List<Resume> result = Lists.newArrayList();
        String ids =(String) map.get("ids");
        if(StringUtils.isNotBlank(ids)){
            result = resumeMapper.findByIds(Arrays.asList(ids.split(",")));
        }else{
            result = resumeMapper.findPage(map);
        }
        return result;
    }

    @Transactional
    public void push(String id,String companyId,String jobId,Boolean intern,Boolean company){
        JobResume jobResume = new JobResume();
        Job job = jobMapper.findOne(jobId);
        Company c = companyMapper.findOne(companyId);
        jobResume.setAccountId(job.getCreatedById());
        jobResume.setJobId(jobId);
        jobResume.setResumeId(id);
        jobResume.setCompanyId(companyId);
        jobResume.setStatus(ResumeStatusEnum.新简历.name());
        jobResume.setType(JobResumeType.投递给我的简历.name());
        jobResume.setResumeFrom(JobResumeFrom.猎头.name());
        jobResumeMapper.save(jobResume);
        //简历推送
        if(intern){
            Resume resume = resumeMapper.findOne(id);
            Map<String,String> map = Maps.newHashMap();
            map.put("name",resume.getName());
            map.put("companyName",c.getName());
            map.put("jobName",job.getName());
            SMSUtils.sendMobileMessage(resume.getMobilePhone(),Const.PUSH_RESUME_SMS,map);
        }
        if(company){
            Account account = accountMapper.findOne(c.getCreatedById());
            Map<String,Object> map = Maps.newHashMap();
            String emailTitle = "Intern Hunter给您推送了一份简历";
            String title = account.getContact()+"您好";
            String content = "根据公司职位，已为您推送Intern Club候选人。\n" +
                    "Club顾问已经和候选人进行了电话沟通，你可以直接与候选人联系。\n" +
                    "如有任何疑问请联系您的HR服务顾问。\n" +
                    "感谢您的支持\n" +
                    "祝您工作顺利";
            map.put("title",title);
            map.put("content",content);
            emailUtils.send(Const.AUDIT_INFO_TEMPLETE,emailTitle,account.getUserName(),map);
        }
    }

    /**
     * 简历完整度计算
     * @param resume
     */
    private void resumeIntegrity(Resume resume){
        int i=20;
        if(StringUtils.isNotBlank(resume.getEducationJson())){
            i=i+10;
        }
        if(StringUtils.isNotBlank(resume.getWorkExperienceJson()) && !"[]".equals(resume.getWorkExperienceJson())){
            i=i+20;
        }
        if(StringUtils.isNotBlank(resume.getSchoolExperienceJson()) && !"[]".equals(resume.getSchoolExperienceJson())){
            i=i+10;
        }
        if(StringUtils.isNotBlank(resume.getSkillJson()) && !"[]".equals(resume.getSkillJson())){
            i=i+10;
        }
        if(StringUtils.isNotBlank(resume.getLanguageJson()) && !"[]".equals(resume.getLanguageJson())){
            i=i+10;
        }
        if(StringUtils.isNotBlank(resume.getCertificateJson()) && !"[]".equals(resume.getCertificateJson())){
            i=i+10;
        }
        if(StringUtils.isNotBlank(resume.getExpectWorkJson()) && !"[]".equals(resume.getExpectWorkJson())){
            i=i+10;
        }
        resume.setPercent(i+"%");
    }


}
