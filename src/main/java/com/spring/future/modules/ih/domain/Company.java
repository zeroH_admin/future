package com.spring.future.modules.ih.domain;

import com.google.common.collect.Lists;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.StringUtils;
import org.springframework.data.annotation.Transient;

import java.util.List;

/**
 * Created by zh on 2017/4/19.
 */

@Entity
@Table(name="ih_company")
public class Company extends DataEntity<Company> {

    //公司名称
    private String name;
    //公司简称
    private String simpleName;
    //公司邮箱
    private String email;
    //融资情况
    private String financingStatus;
    //公司性质
    private String property;
    //公司规模
    private String size;
    //公司地址
    private String address;
    //公司简介
    private String description;
    //公司logo
    private String logo;
    //公司创建人
    //发布人
    private String createdById;

    private Account createdBy;
    //企业状态
    private String status;

    private String outId;
    //联系人姓名
    @Transient
    private String contact;
    //职位
    @Transient
    private String position;
    //座机号码
    @Transient
    private String landLine;
    //手机号码
    @Transient
    private String mobilePhone;

    private List<String> imgList = Lists.newArrayList();

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSimpleName() {
        return simpleName;
    }

    public void setSimpleName(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFinancingStatus() {
        return financingStatus;
    }

    public void setFinancingStatus(String financingStatus) {
        this.financingStatus = financingStatus;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContact() {
        if(StringUtils.isEmpty(contact) && createdBy!=null){
            contact = createdBy.getContact();
        }
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPosition() {
        if(StringUtils.isEmpty(position) && createdBy!=null){
            position = createdBy.getPosition();
        }
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLandLine() {
        if(StringUtils.isEmpty(landLine) && createdBy!=null){
            landLine = createdBy.getLandLine();
        }
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getMobilePhone() {
        if(StringUtils.isEmpty(mobilePhone) && createdBy!=null){
            mobilePhone = createdBy.getMobilePhone();
        }
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCreatedById() {
        return createdById;
    }

    public void setCreatedById(String createdById) {
        this.createdById = createdById;
    }

    public Account getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Account createdBy) {
        this.createdBy = createdBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
