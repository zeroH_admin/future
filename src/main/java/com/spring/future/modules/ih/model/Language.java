package com.spring.future.modules.ih.model;

/**
 * Created by zh on 2017/4/27.
 */
public class Language {
    private String name;
    private String level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
