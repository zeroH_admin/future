package com.spring.future.modules.ih.domain;

import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.Const;
import org.springframework.data.annotation.Transient;

import java.time.LocalDateTime;

/**
 * Created by zh on 2017/4/18.
 */

@Entity
@Table(name="ih_job_resume")
public class JobResume extends DataEntity<JobResume> {

   private String accountId;
   private String jobId;
   private String resumeId;
   private String companyId;
   private String status;
   private String type;
   private String resumeFrom;
   private LocalDateTime interviewDate;
   private Boolean newResume=true;
   private Boolean forword=false;

   private Account account;
   private Job job;
   private Resume resume;
   private Company company;


    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getResumeId() {
        return resumeId;
    }

    public void setResumeId(String resumeId) {
        this.resumeId = resumeId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResumeFrom() {
        return resumeFrom;
    }

    public void setResumeFrom(String resumeFrom) {
        this.resumeFrom = resumeFrom;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getInterviewDate() {
        return interviewDate;
    }

    public void setInterviewDate(LocalDateTime interviewDate) {
        this.interviewDate = interviewDate;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Boolean getNewResume() {
        return newResume;
    }

    public void setNewResume(Boolean newResume) {
        this.newResume = newResume;
    }

    public Boolean getForword() {
        return forword;
    }

    public void setForword(Boolean forword) {
        this.forword = forword;
    }
}
