package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.AccountUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.domain.Article;
import com.spring.future.modules.translate.service.ActivityService;
import com.spring.future.modules.translate.service.ActivitySignService;
import com.spring.future.modules.translate.service.ArticleService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Controller
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping(value = "common/article")
    public String list(Model model,HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        map.put("status","发布");
        String category = (String) map.get("category");
        Page<Article> page = articleService.findPage(searchEntity.getPageable(),map);
        model.addAttribute("page",page);
        model.addAttribute("s", searchEntity);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",StringUtils.isNoneBlank(AccountUtils.getAccountId()));
        return getUrl(category);
    }

    @RequestMapping(value = "common/articleDetail")
    public String clubDetail(Model model,String id){
        Article article = articleService.findOne(id);
        articleService.addViewNum(article);
        model.addAttribute("article",article);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("isLogin",StringUtils.isNoneBlank(AccountUtils.getAccountId()));
        return "ih/article-show";
    }

    private String getUrl(String category){
        String url = "ih/article";;
       if("简历指导".equals(category)){
            url = "ih/article-resume";
        }else if("面试宝典".equals(category)){
            url = "ih/article-interview";
        }else if("职场生涯".equals(category)){
            url = "ih/article-job";
        }else if("劳动法规".equals(category)){
            url = "ih/article-law";
        }
        return url;
    }

}
