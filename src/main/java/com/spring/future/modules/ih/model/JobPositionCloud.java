package com.spring.future.modules.ih.model;

import com.spring.future.common.leanCloudToMysql.ComCloud;

/**
 * Created by zh on 2017/5/16.
 */
public class JobPositionCloud {
    private String positionName;
    private String address;
    private String requirement;
    private String metroStation;
    private String zone;
    private String highlight;
    private String positionCategory;
    private String daysPerWeek;
    private String internPeriod;
    private String duty;
    private ComCloud company;
    private String district;
    private String salary;
    private String createdAt;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public String getMetroStation() {
        return metroStation;
    }

    public void setMetroStation(String metroStation) {
        this.metroStation = metroStation;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getPositionCategory() {
        return positionCategory;
    }

    public void setPositionCategory(String positionCategory) {
        this.positionCategory = positionCategory;
    }

    public String getDaysPerWeek() {
        return daysPerWeek;
    }

    public void setDaysPerWeek(String daysPerWeek) {
        this.daysPerWeek = daysPerWeek;
    }

    public String getInternPeriod() {
        return internPeriod;
    }

    public void setInternPeriod(String internPeriod) {
        this.internPeriod = internPeriod;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public ComCloud getCompany() {
        return company;
    }

    public void setCompany(ComCloud company) {
        this.company = company;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
