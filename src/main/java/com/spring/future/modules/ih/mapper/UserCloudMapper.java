package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.UserCloud;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by zh on 2017/4/18.
 */
@Mapper
public interface UserCloudMapper extends CrudMapper<UserCloud,String> {


}
