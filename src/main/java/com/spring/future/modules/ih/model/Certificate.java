package com.spring.future.modules.ih.model;

import java.time.LocalDate;

/**
 * Created by zh on 2017/4/27.
 * 证书
 */
public class Certificate {

    private String name;
    private LocalDate date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
