package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.DictMap;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

@Mapper
public interface DictMapMapper extends CrudMapper<DictMap,String> {


}
