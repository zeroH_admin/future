package com.spring.future.modules.ih.security;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zh on 2017/5/13.
 */
@Component
public class MyAuthenticationManager implements AuthenticationManager {

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        if (authentication.getName().equals(customUserDetails.getUsername())) {

            return new UsernamePasswordAuthenticationToken(customUserDetails,authentication.getCredentials(),authentication.getAuthorities());

        }

        throw new BadCredentialsException("Bad Credentials");
    }

    public boolean supports(Class<? extends Object> authentication) {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

}
