package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.enums.PositionEnum;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.text.Position;
import java.util.List;

/**
 * Created by zh on 2017/4/18.
 */
@Controller
public class AccountController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private PasswordEncoder passwordEncoder;



    @RequestMapping(value = "hr/reset-password")
    public String restPassword(Model model){
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("company",AccountUtils.getCompany());
        return "ih/hr-password";
    }

    @RequestMapping(value = "hr/updatePassword")
    public String updatePassword(String oldPassword,String newPassword,RedirectAttributes redirectAttributes){
        Account account = ThreadLocalContext.get().getAccount();
        if(account!=null && StringUtils.isNotBlank(account.getSalt())){
            boolean result = PasswordUtils.checkCloudPassword(account.getSalt(),oldPassword,account.getPassword());
            if(!result){
                redirectAttributes.addFlashAttribute("message", new Message("当前密码输入错误"));
            }
        }
        if(!passwordEncoder.matches(oldPassword,account.getPassword())){
            redirectAttributes.addFlashAttribute("message", new Message("当前密码输入错误"));
        }else{
            account.setPassword(passwordEncoder.encode(newPassword));
            accountService.save(account);
            redirectAttributes.addFlashAttribute("message", new Message("保存成功"));
        }
        return "redirect:/logout";
    }

    @RequestMapping(value = "hr/account")
    public String findOne(Model model,String id){
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("company",AccountUtils.getCompany());
        return "ih/personal";
    }

    @RequestMapping(value = "hr/saveAccount")
    public String save(String id,String contact,String position,String mobilePhone,String landLine,RedirectAttributes redirectAttributes){
        accountService.save(id,position,mobilePhone,contact,landLine);
        redirectAttributes.addFlashAttribute("message", new Message("保存成功"));
        return "redirect:/hr/account";
    }


    @RequestMapping(value = "/intern-center")
    public String internCenter(Model model){
        model.addAttribute("account",AccountUtils.getAccount());
        return "ih/intern-center";
    }

    @RequestMapping(value = "intern/updatePassword")
    public String internReset(String oldPassword,String newPassword,RedirectAttributes redirectAttributes){
        Account account = AccountUtils.getAccount();
        if(account!=null && StringUtils.isNotBlank(account.getSalt())){
            boolean result = PasswordUtils.checkCloudPassword(account.getSalt(),oldPassword,account.getPassword());
            if(!result){
                redirectAttributes.addFlashAttribute("message", new Message("当前密码输入错误"));
            }
        }
        if(!passwordEncoder.matches(oldPassword,account.getPassword())){
            redirectAttributes.addFlashAttribute("message", new Message("当前密码输入错误"));
        }else{
            account.setPassword(passwordEncoder.encode(newPassword));
            accountService.save(account);
            redirectAttributes.addFlashAttribute("message", new Message("保存成功"));
        }
        return "redirect:/logout";
    }


}
