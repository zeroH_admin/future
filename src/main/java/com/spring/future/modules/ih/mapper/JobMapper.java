package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Job;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/25.
 */
@Mapper
public interface JobMapper  extends CrudMapper<Job,String> {

    Page<Job> findPage(Pageable pageable ,@Param("p")Map<String,Object> map);

    List<Job> findSelfJob(String accountId);

    List<Job> findByCompany( @Param("companyId") String companyId,@Param("status") String status);

    List<Job> findData(@Param("p")Map<String,Object> map);

    Integer findAuditNum();
}
