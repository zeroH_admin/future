package com.spring.future.modules.ih.model;

/**
 * Created by zh on 2017/5/4.
 */
public class Photo {
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
