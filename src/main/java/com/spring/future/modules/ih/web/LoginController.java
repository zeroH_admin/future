package com.spring.future.modules.ih.web;

import com.google.common.collect.Lists;
import com.spring.future.common.enums.PositionEnum;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.JobResume;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.service.*;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.service.ActivitySignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by admin on 2017/1/14.
 */
@Controller
public class LoginController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private UserService userService;
    @Autowired
    private ActivitySignService activitySignService;
    @Autowired
    private JobResumeService jobResumeService;
    @Autowired
    private ResumeService resumeService;

    @RequestMapping({"/", "index"})
    public String index() {
        return "ih/index";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login() {
        return "ih/login";
    }

    /**
     * 用户登录成功后，判断信息的完整性进行不同的跳转
     * 如果信息完整则跳转到首页
     * @return
     */
    @RequestMapping({"/checkInfo"})
    public String successForwardUrl(HttpServletRequest request, HttpServletResponse response, RedirectAttributes redirectAttributes, Device device){
        Account account = ThreadLocalContext.get().getAccount();
        if(Const.COMPANY.equals(account.getType())){
            if( StringUtils.isBlank( account.getCompanyId())){
                //判断邮箱后缀是否在之前公司中存在
                String email = account.getUserName();
                List<Company> companyList = companyService.findByEmailLike(email.split("@")[1]);
                if(Collections3.isNotEmpty(companyList)){
                    redirectAttributes.addFlashAttribute("companyList",companyList);
                    return "redirect:/register/settled";
                }
                //信息完善页面
                return "redirect:/register/contact";
            }else {
                //企业个人中心
                return "redirect:/hr/hr-index";
            }
        }else if(Const.USER.equals(account.getType())){
            if( StringUtils.isBlank( account.getUserId())){
                //信息完善页面
                return "redirect:/register/profile-step";
            }else {
                //用户首页
                return "redirect:/intern/intern-index";
            }
        }else{
            return "redirect:/index";
        }
    }

    @RequestMapping({"/hr/hr-index"})
    public String hrIndex(Model model){
        model.addAttribute("account", AccountUtils.getAccount());
        model.addAttribute("company",AccountUtils.getCompany());
        model.addAttribute("map",companyService.getIndexNum());
        return "ih/hr-index";
    }

    @RequestMapping({"/intern/intern-index"})
    public String internIndex(Model model,Device device){
        if(device.isTablet()|| device.isMobile()){
            model.addAttribute("status","审核成功");
            model.addAttribute("accountName",AccountUtils.getAccount().getUserName());
            return "ih/intern-pending";
        }else{
            Account account = AccountUtils.getAccount();
            User user = userService.findOne(account.getUserId());
            account.setUser(user);
            model.addAttribute("signActivityNum",activitySignService.findNumByUser(user.getId()));
            model.addAttribute("account",account);
            List<JobResume> jobResumeList = jobResumeService.find2JobResume();
            model.addAttribute("hasJobPush",Collections3.isNotEmpty(jobResumeList));
            model.addAttribute("jobResumeList",jobResumeList);
            List<ActivitySign> activitySignList = activitySignService.find3ActivitySign();
            model.addAttribute("hasSign",Collections3.isNotEmpty(activitySignList));
            model.addAttribute("activitySignList",activitySignList);
            return "ih/intern-index";
        }
    }


    @RequestMapping({"/auditNotice"})
    public String auditNotice(Model model,Device device){
        Account account = ThreadLocalContext.get().getAccount();
        if(Const.COMPANY.equals(account.getType())){
            return "ih/company-pending";
        }else{
            if(device.isTablet()|| device.isMobile()){
                model.addAttribute("status","审核中");
                model.addAttribute("accountName",AccountUtils.getAccount().getUserName());
            }
            return "ih/intern-pending";
        }
    }


    @RequestMapping({"/coming-soon"})
    public String comingSoon(){
        return "ih/coming-soon";
    }

    @RequestMapping({"/aboutus"})
    public String aboutus(){
        return "ih/aboutus";
    }

    @RequestMapping({"/help"})
    public String help(){
        return "ih/help";
    }

}
