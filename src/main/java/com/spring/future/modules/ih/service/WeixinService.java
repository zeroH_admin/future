package com.spring.future.modules.ih.service;

import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ThreadLocalContext;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Weixin;
import com.spring.future.modules.ih.mapper.AccountMapper;
import com.spring.future.modules.ih.mapper.WeixinMapper;
import com.spring.future.modules.ih.security.CustomUserDetails;
import com.spring.future.modules.ih.security.MyAuthenticationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class WeixinService{

    @Autowired
    private WeixinMapper weixinMapper;
    @Autowired
    protected MyAuthenticationManager myAuthenticationManager;
    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public Weixin findByOpenid(String openid){
        return weixinMapper.findByOpenid(openid);
    }

    @Transactional
    public void weixinBind(Account account,Weixin weixin){
        Weixin temp = weixinMapper.findByOpenid(weixin.getOpenid());
        if(temp==null){
            weixin.setAccountId(account.getId());
            weixinMapper.save(weixin);
        }else{
            temp.setAccountId(account.getId());
            weixinMapper.save(temp);
        }
        account.setWeixinId(weixin.getId());
        accountMapper.update(account);
    }

    @Transactional
    public void weixinRegister(Account account,Weixin weixin){
        account.setLevel(Const.DEFULT_LEVEL);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        account.setType(Const.USER);
        //未审核前锁定
        account.setLocked(true);
        accountMapper.save(account);
        Weixin temp = weixinMapper.findByOpenid(weixin.getOpenid());
        if(temp==null){
            weixin.setAccountId(account.getId());
            weixinMapper.save(weixin);
            account.setWeixinId(weixin.getId());
        }else{
            temp.setAccountId(account.getId());
            weixinMapper.update(temp);
            account.setWeixinId(temp.getId());
        }
        accountMapper.update(account);
    }

}
