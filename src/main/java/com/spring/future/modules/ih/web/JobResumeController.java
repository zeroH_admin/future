package com.spring.future.modules.ih.web;

import com.google.common.collect.Maps;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Job;
import com.spring.future.modules.ih.domain.JobResume;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.JobResumeService;
import com.spring.future.modules.ih.service.JobService;
import com.sun.org.apache.bcel.internal.generic.NEW;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Controller
public class JobResumeController {

    @Autowired
    private JobResumeService jobResumeService;
    @Autowired
    private JobService jobService;
    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "hr/interview")
    public String interview(HttpServletRequest request, Model model){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> paramsMap = searchEntity.getParams();
        paramsMap.put("status","待面试");
        Page<JobResume> page = jobResumeService.findPage(searchEntity.getPageable(), paramsMap);
        model.addAttribute("page",page);
        model.addAttribute("company", AccountUtils.getCompany());
        model.addAttribute("jobList",jobService.findSelfJob());
        model.addAttribute("newResumeNum",jobResumeService.findNewResumeNum());
        model.addAttribute("s", searchEntity);
        return "ih/interview";
    }

    /**
     * 应聘管理，更加不同的参数跳转到不用的页面，根据status来跳转
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "hr/resumeManage")
    public String newResume(HttpServletRequest request, Model model){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> paramsMap = searchEntity.getParams();
        String status = request.getParameter("status");
        //JobId情况处理 其他页面跳转待jobId
        String jobId = (String) paramsMap.get("jobId");
        if(StringUtils.isNotBlank(jobId)){
            Job job = jobService.findOne(jobId);
            paramsMap.put("jobName",job.getName());
        }

        Page<JobResume> page = jobResumeService.findPage(searchEntity.getPageable(), paramsMap);
        model.addAttribute("page",page);
        model.addAttribute("account",AccountUtils.getAccount());
        model.addAttribute("company", AccountUtils.getCompany());
        model.addAttribute("jobList",jobService.findSelfJob());
        model.addAttribute("newResumeNum",jobResumeService.findNewResumeNum());
        model.addAttribute("s", searchEntity);
        return getReturen(status);
    }

    /**
     * 应聘管理，简历库
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "hr/resume-library")
    public String resumeLibrary(HttpServletRequest request, Model model){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> paramsMap = searchEntity.getParams();
        Page<JobResume> page = jobResumeService.findPage(searchEntity.getPageable(), paramsMap);
        model.addAttribute("page",page);
        model.addAttribute("company", AccountUtils.getCompany());
        model.addAttribute("jobList",jobService.findSelfJob());
        model.addAttribute("newResumeNum",jobResumeService.findNewResumeNum());
        model.addAttribute("s", searchEntity);
        return "ih/resume-library";
    }

    /**
     * 淘汰简历
     * @param id
     * @return
     */
    @RequestMapping(value = "hr/failResume")
    public String failResume(Model model,String id,String urlType,HttpServletRequest request, RedirectAttributes redirectAttributes){
        jobResumeService.failResume(id);
        redirectAttributes.addAttribute("status",urlType);
        return "redirect:/hr/resumeManage";
    }

    /**
     *操作简历
     * @param id
     * @param status
     * @param urlType
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "hr/operationResume")
    public String updateStatus(String id,String status,String urlType, RedirectAttributes redirectAttributes){
        jobResumeService.updateStatus(id,status);
        redirectAttributes.addAttribute("status",urlType);
        return "redirect:/hr/resumeManage";
    }

    /**
     * 发送面试邀请
     * @param id
     * @param address
     * @param contact
     * @param mobilePhone
     * @param date
     * @param redirectAttributes
     * @return
     */
    @RequestMapping(value = "hr/sendView")
    public String sendView(String id,String address,String contact,String mobilePhone,String date, RedirectAttributes redirectAttributes){
        jobResumeService.sendView(id,address,contact,mobilePhone,date);
        redirectAttributes.addAttribute("status","待面试");
        return "redirect:/hr/resumeManage";
    }

    /**
     * 查看简历
     * @param id
     * @param model
     * @return
     */
    @RequestMapping(value = "hr/view-resume")
    public String viewResume(String id,Model model){
        JobResume jobResume = jobResumeService.findOne(id);
        if(jobResume.getNewResume()) {
            jobResumeService.updateResumeFirst(id);
        }
        model.addAttribute("jobResume",jobResume);
        model.addAttribute("emailList",accountService.findEmailByCompany());
        return "ih/hr-resume";
    }

    /**
     * ajax获取简历信息
     * @param id
     * @return
     */
    @RequestMapping(value = "hr/viewResumeData")
    @ResponseBody
    public String viewResumeData(String id){
        JobResume jobResume = jobResumeService.viewResume(id);
        return ObjectMapperUtils.writeValueAsString(jobResume);
    }

    /**
     * ajax发送面试邀请
     * @param id
     * @param address
     * @param contact
     * @param mobilePhone
     * @param date
     * @return
     */
    @RequestMapping(value = "hr/sendInterView")
    @ResponseBody
    public String sendInterView(String id,String address,String contact,String mobilePhone,String date){
        jobResumeService.sendView(id,address,contact,mobilePhone,date);
        return "操作成功";
    }

    /**
     * 淘汰简历
     * @param id
     * @return
     */
    @RequestMapping(value = "hr/fail")
    @ResponseBody
    public String failResume(String id){
        jobResumeService.failResume(id);
        return "操作成功";
    }


    /**
     * 转发简历
     * @param id
     * @return
     */
    @RequestMapping(value = "hr/forwardSend")
    @ResponseBody
    public String forwardSend(String id,String email,String remarks){
        jobResumeService.forwardSend(id,email,remarks);
        return "操作成功";
    }

    /**
     * 批量淘汰简历
     * @param ids
     * @return
     */
    @RequestMapping(value = "hr/batchFail")
    @ResponseBody
    public String batchFail(@RequestParam(value = "ids[]") String[] ids){
        for(String id:ids){
            jobResumeService.failResume(id);
        }
        return ObjectMapperUtils.writeValueAsString(new Message("批量淘汰成功"));
    }

    @RequestMapping(value = "intern/pushJobList")
    public String pushJobList(Model model,HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<JobResume> page = jobResumeService.findInternPushList(searchEntity.getPageable(),searchEntity.getParams());
        model.addAttribute("page",page);
        model.addAttribute("s", searchEntity);
        model.addAttribute("account",AccountUtils.getAccount());
        return "ih/push-job-list";
    }


    private String getReturen(String status){
        Map<String,String> map = Maps.newHashMap();
        map.put("新简历","ih/new-resume");
        map.put("待沟通","ih/communicate");
        map.put("待面试","ih/wait-interview");
        map.put("待录用","ih/hire");
        map.put("待入职","ih/entry");
        return map.get(status);
    }

}
