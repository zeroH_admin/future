package com.spring.future.modules.ih.domain;

import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.StringUtils;
import org.springframework.data.annotation.Transient;

import java.time.LocalDate;

/**
 * Created by zh on 2017/4/19.
 */
@Entity
@Table(name="ih_user")
public class User extends DataEntity<User>{

    //头像
    private String image;
    //姓名
    private String name;
    //性别
    private String sex;
    //生日
    private LocalDate birthday;
    //国籍
    private String nationality;
    //地址
    private String address;
    //邮箱
    private String email;
    //手机号码
    private String mobilePhone;
    //学历
    private String education;
    //学校
    private String school;
    //专业
    private String major;
    //入学时间
    private LocalDate entranceDate;
    //毕业时间
    private LocalDate graduationDate;
    //期望行业
    private String expectIndustry;
    //期望岗位
    private String expectPosition;
    //实习时间
    private String internTime;
    //实习周期
    private String internPeriod;
    //期望薪资
    private String expectSalary;
    //优点特长
    private String advantage;
    //描述
    private String description;
    //类型  全职/实习
    private String type;

    private String accountId;

    private Account account;
    private String outId;
    private String tag;

    @Transient
    private String expectSalaryMin;
    @Transient
    private String expectSalaryMax;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public LocalDate getEntranceDate() {
        return entranceDate;
    }

    public void setEntranceDate(LocalDate entranceDate) {
        this.entranceDate = entranceDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public void setGraduationDate(LocalDate graduationDate) {
        this.graduationDate = graduationDate;
    }

    public String getExpectIndustry() {
        return expectIndustry;
    }

    public void setExpectIndustry(String expectIndustry) {
        this.expectIndustry = expectIndustry;
    }

    public String getExpectPosition() {
        return expectPosition;
    }

    public void setExpectPosition(String expectPosition) {
        this.expectPosition = expectPosition;
    }

    public String getInternTime() {
        return internTime;
    }

    public void setInternTime(String internTime) {
        this.internTime = internTime;
    }

    public String getInternPeriod() {
        return internPeriod;
    }

    public void setInternPeriod(String internPeriod) {
        this.internPeriod = internPeriod;
    }

    public String getExpectSalary() {
        if(StringUtils.isNotBlank(expectSalaryMin) && StringUtils.isNotBlank(expectSalaryMax)){
            expectSalary = expectSalaryMin+"-"+expectSalaryMax;
        }
        return expectSalary;
    }

    public void setExpectSalary(String expectSalary) {
        this.expectSalary = expectSalary;
    }

    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpectSalaryMin() {
        return expectSalaryMin;
    }

    public void setExpectSalaryMin(String expectSalaryMin) {
        this.expectSalaryMin = expectSalaryMin;
    }

    public String getExpectSalaryMax() {
        return expectSalaryMax;
    }

    public void setExpectSalaryMax(String expectSalaryMax) {
        this.expectSalaryMax = expectSalaryMax;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getOutId() {
        return outId;
    }

    public void setOutId(String outId) {
        this.outId = outId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
