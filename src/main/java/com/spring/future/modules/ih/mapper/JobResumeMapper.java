package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Job;
import com.spring.future.modules.ih.domain.JobResume;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Mapper
public interface JobResumeMapper extends CrudMapper<JobResume,String> {

    Page<JobResume> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    Page<JobResume> findPushPage(Pageable pageable , @Param("p")Map<String,Object> map);

    List<JobResume> findSelf(String accountId);

    List<JobResume> find2JobResume(String accountId);

    Page<JobResume> findInternPage(Pageable pageable , @Param("p")Map<String,Object> map);

    int findNewResumeNum(String accountId);

    int findNumByJobAndStatus(@Param("jobId")String jobId,@Param("status")String status,@Param("accountId")String accountId);

}
