package com.spring.future.modules.ih.web;

import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.ThreadLocalContext;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zh on 2017/4/19.
 */
@RestController
public class UserController {
    @Autowired
    private UserService userService;


    @RequestMapping(value = "intern/getUserInfo")
    @ResponseBody
    public String getUserInfo(){
        User user = userService.getUserInfo();
        return ObjectMapperUtils.writeValueAsString(user);
    }

    @RequestMapping(value = "register/getUserName")
    public String getUserName(){
        Account account = ThreadLocalContext.get().getAccount();
        return ObjectMapperUtils.writeValueAsString(account.getUserName());
    }
}
