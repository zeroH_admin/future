package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.FolderFile;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FolderFileMapper extends CrudMapper<FolderFile,String> {

    List<String> findUrlByExtendId(String extendId);

}
