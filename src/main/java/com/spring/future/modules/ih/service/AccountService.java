package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.mapper.AccountMapper;
import com.spring.future.modules.ih.mapper.CompanyMapper;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.mapper.RoleMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class AccountService {

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleMapper roleMapper;

    public Account findByUserName(String userName){
        return accountMapper.findByUserName(userName);
    }

    public Account userCheck(String userName,String type){
        return accountMapper.findByNameAndType(userName,type);
    }

    public Account findOne(String id){
        return accountMapper.findOne(id);
    }

    public List<String> findEmailByCompany(){
        List<String> list = Collections3.extractToList(accountMapper.findByCompany(AccountUtils.getCompanyId()),"userName");
        return list;
    }

    @Transactional
    public void update(Account account){
        accountMapper.update(account);
    }

    @Transactional
    public void updateLoginDate(){
        accountMapper.updateLoginDate(AccountUtils.getAccountId());
    }

    @Transactional
    public void joinCompany(String companyId){
        Account account = ThreadLocalContext.get().getAccount();
        account.setCompanyId(companyId);
        account.setLevel(2);
        accountMapper.update(account);
    }


    public List<Account> findCompanyAccount(){
        Account account = ThreadLocalContext.get().getAccount();
        Company company = companyMapper.findOne(account.getCompanyId());
        List<Account> accounts = accountMapper.findByCompany(account.getCompanyId());
        for(Account item:accounts){
            item.setCompany(company);
        }
        return accounts;
    }

    @Transactional
    public void save(String id,String position,String mobilePhone,String contact,String landLine){
        Account account = accountMapper.findOne(id);
        account.setPosition(position);
        account.setMobilePhone(mobilePhone);
        account.setContact(contact);
        account.setLandLine(landLine);
        accountMapper.update(account);
    }

    @Transactional
    public void save(Account account){
        accountMapper.update(account);
    }

    @Transactional
    public void competerInfo(Account account){
        Account temp = ThreadLocalContext.get().getAccount();
        temp.setContact(account.getContact());
        temp.setPosition(account.getPosition());
        temp.setLandLine(account.getLandLine());
        temp.setMobilePhone(account.getMobilePhone());
        accountMapper.update(temp);
    }

    /**
     * 后台处理
     */



    public Page<Account> findPage(Pageable pageable, Map<String, Object> map){
        return accountMapper.findPage(pageable,map);
    }

    @Transactional
    public void save(String userName,String password,String contact,Boolean enabled,String roleId,String id){
        Account account = new Account();
        if(id!=null){
            account = accountMapper.findOne(id);
        }
        account.setUserName(userName);
        account.setPassword(passwordEncoder.encode(password));
        account.setContact(contact);
        account.setEnabled(enabled);
        Role role = roleMapper.findOne(roleId);
        account.setPosition(role.getName());
        account.setType("admin");
        account.setLevel(1);
        if(id!=null){
            accountMapper.update(account);
        }else{
            accountMapper.save(account);
        }
    }


    public List<Account> finExportData(Map<String,Object> map){
        String ids =(String) map.get("ids");
        List<Account> result = Lists.newArrayList();
        if(StringUtils.isNotBlank(ids)){
            List<String> idsList = Arrays.asList(ids.split(","));
            result = accountMapper.findByIds(idsList);
        }else{
            result= accountMapper.findData(map);
        }
        return result;
    }

    /**
     * 企业列表   从企业账号发起
     * @param pageable
     * @param map
     * @return
     */
    public Page<Account> companyList(Pageable pageable, Map<String, Object>map){
        Page<Account> page = accountMapper.findCompanyAccount(pageable,map);
        List<String> companyIds = Collections3.extractToList(page.getContent(),"companyId");
        if(Collections3.isNotEmpty(companyIds)){
            List<Company> companyList = companyMapper.findByIds(companyIds);
            if(Collections3.isNotEmpty(companyList)){
                Map<String,Company> companyMap = Collections3.extractToMap(companyList,"id");
                for(Account account:page.getContent()){
                    Company company = new Company();
                    if(companyMap.containsKey(account.getCompanyId())){
                        company = companyMap.get(account.getCompanyId());
                    }
                    account.setCompany(company);
                }
            }
        }
        return page;
    }

    //首页图标数据
    public Map<String,List<Object>> findEchartsData(String type){
        LocalDate localDate = LocalDate.now();
        Map<String,List<Object>> map = Maps.newHashMap();
        List<Object> dateList =Lists.newArrayList();
        List<Object> accountNumList = Lists.newArrayList();
        for(int i=7;i>0;i--){
            LocalDate dateStart = localDate.minusDays(i);
            LocalDateTime dateEnd = DateUtils.parseLocalDateTime(DateUtils.formatLocalDate(dateStart) +" 23:59:59");
            Integer size = accountMapper.findBtwDate(type,dateStart,dateEnd);
            dateList.add(dateStart);
            accountNumList.add(size);
        }
        map.put(type+"DateStr",dateList);
        map.put(type+"NumList",accountNumList);
        return map;
    }
}
