package com.spring.future.modules.ih.service;

import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.utils.ThreadLocalContext;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.mapper.AccountMapper;
import com.spring.future.modules.ih.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by zh on 2017/4/18.
 */
@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;

    public User getUserInfo(){
        Account account = ThreadLocalContext.get().getAccount();
        User user = userMapper.findOne(account.getUserId());
        return user;
    }

    public User findOne(String id){
        return userMapper.findOne(id);
    }

}
