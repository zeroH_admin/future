package com.spring.future.modules.ih.model;

/**
 * Created by zh on 2017/5/16.
 */
public class FileCloud {
    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
