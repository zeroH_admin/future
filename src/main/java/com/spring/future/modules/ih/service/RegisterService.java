package com.spring.future.modules.ih.service;

import com.google.common.collect.Lists;
import com.spring.future.common.enums.UserTagEnum;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.Resume;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.mapper.AccountMapper;
import com.spring.future.modules.ih.mapper.CompanyMapper;
import com.spring.future.modules.ih.mapper.ResumeMapper;
import com.spring.future.modules.ih.mapper.UserMapper;
import com.spring.future.modules.ih.model.Education;
import com.spring.future.modules.ih.model.ExpectWork;
import org.apache.catalina.security.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.security.Security;
import java.util.List;
import java.util.UUID;

/**
 * Created by zh on 2017/4/20.
 */
@Service
public class RegisterService {

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private CompanyMapper companyMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ResumeMapper resumeMapper;

    @Transactional
    public void register(Account account){
        account.setLevel(Const.DEFULT_LEVEL);
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        //未审核前锁定
        account.setLocked(true);
        accountMapper.save(account);
    }

    @Transactional
    public void saveCompany(Company company){
        Account account = ThreadLocalContext.get().getAccount();
        if(account.getCompanyId()==null){
            company.setEmail(account.getUserName());
            if(company.isCreate()){
                company.setCreatedById(account.getId());
                company.setStatus("待审核");
                companyMapper.save(company);
            }else{
                companyMapper.update(company);
            }
            account.setCompanyId(company.getId());
            account.setContact(company.getContact());
            account.setLandLine(company.getLandLine());
            account.setPosition(company.getPosition());
            account.setMobilePhone(company.getMobilePhone());
            accountMapper.update(account);
        }
    }

    @Transactional
    public void saveUser(User user, Device device){
        Account account = AccountUtils.getAccount();
        user.setAccountId(account.getId());
        if(device.isNormal()){
            user.setExpectSalary(user.getExpectSalaryMin()+"-"+user.getExpectSalaryMax());
        }else{
            user.setExpectSalary(user.getExpectSalary());
        }
        user.setTag(UserTagEnum.普通会员.name());
        if(user.isCreate()){
            userMapper.save(user);
        }else{
            userMapper.update(user);
        }
        account.setUserId(user.getId());
        accountMapper.update(account);

        //保存信息时生成简历
        Resume resume = new Resume();
        resume.setAccountId(account.getId());
        resume.setUserId(user.getId());
        resume.setImage(user.getImage());
        resume.setName(user.getName());
        resume.setSex(user.getSex());
        resume.setBirthday(user.getBirthday());
        resume.setMobilePhone(user.getMobilePhone());
        resume.setEmail(user.getEmail());
        resume.setNationality(user.getNationality());
        resume.setAddress(user.getAddress());
        resume.setEducation(user.getEducation());
        resume.setAdvantage(user.getAdvantage());
        resume.setDescription(user.getDescription());
        resume.setAuditStatus("待审核");
        List<Education> educationList = Lists.newArrayList();
        Education education = new Education();
        education.setSchool(user.getSchool());
        education.setMajor(user.getMajor());
        education.setLevel(user.getEducation());
        education.setEduDateStart(user.getEntranceDate());
        education.setEduDateEnd(user.getGraduationDate());
        educationList.add(education);
        resume.setEducationJson(ObjectMapperUtils.writeValueAsString(educationList));

        List<ExpectWork> expectWorkList = Lists.newArrayList();
        ExpectWork expectWork = new ExpectWork();
        expectWork.setExpectPosition(user.getExpectPosition());
        expectWork.setExpectIndustry(user.getExpectIndustry());
        expectWork.setAvailableTime(user.getInternTime());
        expectWork.setExpectedPeriod(user.getInternPeriod());
        expectWork.setExpectedSalary(user.getExpectSalary());
        expectWorkList.add(expectWork);
        resume.setExpectWorkJson(ObjectMapperUtils.writeValueAsString(expectWorkList));

        resumeMapper.save(resume);
    }

    @Transactional
    public void resetPassword(String userName,String password){
        Account account = accountMapper.findByUserName(userName);
        account.setPassword(passwordEncoder.encode(password));
        accountMapper.update(account);
    }
}
