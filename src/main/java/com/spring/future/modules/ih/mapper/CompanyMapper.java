package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.Job;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/19.
 */
@Mapper
public interface CompanyMapper extends CrudMapper<Company,String> {

    List<Company> findByEmailLike(String email);

    List<Company> findByNameLike(String name);

    int deleteByExtendId(String extendId);

    Page<Company> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    List<Company> findData( @Param("p")Map<String,Object> map);

    Integer findAuditNum();


}
