package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * Created by zh on 2017/4/19.
 */
@Mapper
public interface UserMapper extends CrudMapper<User,String> {


}
