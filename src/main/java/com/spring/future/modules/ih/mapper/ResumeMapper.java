package com.spring.future.modules.ih.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Resume;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/4/18.
 */
@Mapper
public interface ResumeMapper extends CrudMapper<Resume,String> {

    Resume findByUserId(String userId);

    Page<Resume> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    List<Resume> findPage(@Param("p")Map<String,Object> map);

    Integer findAuditResume();
}
