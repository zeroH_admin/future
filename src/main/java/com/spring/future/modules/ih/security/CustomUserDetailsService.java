package com.spring.future.modules.ih.security;

import com.spring.future.FutureApplication;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.security.CustomUserDetails;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.translate.domain.Permission;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.service.PermissionService;
import com.spring.future.modules.translate.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

/**
 * Created by liuj on 2016-07-25.
 */

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private AccountService accountService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String loginName) throws UsernameNotFoundException {
        Account account = accountService.findByUserName(loginName);
        //更新最后登录时间
        accountService.updateLoginDate();
        String password = request.getParameter("password");
        System.err.println("password:"+password);
        String type = request.getParameter("type");
        PasswordEncoder passwordEncoder = FutureApplication.getApplicationContext().getBean(PasswordEncoder.class);
        //leanCloud原账号密码登录验证问题    **当账号里salt有值表示是之前leancloud导入的数据，加密方式不一样
        if(account!=null && StringUtils.isNotBlank(account.getSalt()) && account.getLastModifiedDate().isBefore(LocalDateTime.of(2017, Month.MAY,18,05, 10, 30))){
            Boolean checkResult = PasswordUtils.checkCloudPassword(account.getSalt(),password,account.getPassword());
            if(checkResult){
                account.setPassword(passwordEncoder.encode(password));
            }
        }
        if(account ==null || !type.equals(account.getType()) ){
            return null;
        }
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;
        UserDetails userDetails = new CustomUserDetails(
                account.getUserName(),
                account.getPassword(),
                enabled,
                accountNonExpired,
                credentialsNonExpired,
                accountNonLocked,
                getAuthorities(account),
                account);
        return userDetails;
    }

    /**
     * Retrieves a collection of {@link GrantedAuthority} based on a list of
     * roles
     *
     * @param account
     *            the assigned roles of the account
     * @return a collection of {@link GrantedAuthority}
     */
    private Collection<? extends GrantedAuthority> getAuthorities(Account account) {
        Set<SimpleGrantedAuthority> authList = new TreeSet<SimpleGrantedAuthority>(new SimpleGrantedAuthorityComparator());
        if("admin".equals(account.getType())){
            Role role = roleService.findByName(account.getPosition());
            authList.add(new SimpleGrantedAuthority(role.getPermission()));
            List<Permission> permissionList = permissionService.findByRole(role.getId());
            if(Collections3.isNotEmpty(permissionList)){
                for (Permission permission:permissionList){
                    authList.add(new SimpleGrantedAuthority(permission.getPermission()));
                }
            }
        }
        return authList;
    }


    private static class SimpleGrantedAuthorityComparator implements Comparator<SimpleGrantedAuthority> {
        @Override
        public int compare(SimpleGrantedAuthority o1, SimpleGrantedAuthority o2) {
            return o1.equals(o2) ? 0 : -1;
        }
    }

}
