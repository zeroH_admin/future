package com.spring.future.modules.ih.web;

import com.google.common.collect.Maps;
import com.spring.future.common.utils.ExcelUtil;
import com.spring.future.common.utils.FileUtils;
import com.spring.future.common.utils.HtmlGenerator;
import com.spring.future.modules.ih.domain.JobResume;
import com.spring.future.modules.ih.domain.Resume;
import com.spring.future.modules.ih.service.JobResumeService;
import com.spring.future.modules.ih.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zh on 2017/5/23.
 */
@Controller
public class PdfController {
    @Autowired
    private JobResumeService jobResumeService;
//    @Value("${templatePath}")
//    private String templatePath;


    @RequestMapping(value = "register/pdf")
    public void downLoad(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String path = request.getSession().getServletContext().getRealPath("");
//        String filePath = path + UUID.randomUUID().toString()+ ".pdf";
//        OutputStream out = new FileOutputStream(filePath);
//        List<JobResume> jobResumeList = jobResumeService.findAll();
//        Map<String, Object> map = Maps.newHashMap();
//        map.put("data", jobResumeList);
//
//        File file = ResourceUtils.getFile("src/main/resources/templates/template/template.html");
////        org.springframework.core.io.Resource fileRource = new ClassPathResource("test.txt");
//        String filePath1 = file.getPath();
//        String filepath2 = file.getAbsolutePath();
//        String fileTemplatePath = filepath2.replace("template.html","")+File.separator;
////       String fileTemplatePath = path + templatePath + File.separator;
//        String htmlStr = HtmlGenerator.generate(fileTemplatePath, "template", map);
//
//        DocumentBuilder builder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
//        Document doc = builder.parse(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")));
//        ITextRenderer renderer = new ITextRenderer();
//
//        ITextFontResolver fontResolver = renderer.getFontResolver();
//        fontResolver.addFont("C:/Windows/Fonts/simsun.ttc", "Identity-H",false);
//        fontResolver.addFont("C:/Windows/Fonts/simhei.ttf", "Identity-H",false);
//
//        renderer.setDocument(doc, null);
//        renderer.layout();
//        renderer.createPDF(out);
//        out.close();
//        // 下载文件
//        String fileName = "test.pdf";
//        ExcelUtil.downloadFile(request, response, fileName, filePath);

    }
}
