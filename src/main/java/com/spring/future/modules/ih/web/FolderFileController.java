package com.spring.future.modules.ih.web;

import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.service.ServiceException;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.FolderFile;
import com.spring.future.modules.ih.service.FolderFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;

@RestController
@RequestMapping(value = "ih/folderFile")
public class FolderFileController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private FolderFileService folderFileService;

    @ModelAttribute
    public FolderFile get(@RequestParam(required = false) String id) {
        return StringUtils.isBlank(id) ? new FolderFile() : folderFileService.findOne(id);
    }

    @RequestMapping(value = "findOne")
    public String findOne(String id){
        FolderFile folderFile = folderFileService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(folderFile);
    }

    @RequestMapping(value = "/upload")
    public List<String> upload(MultipartHttpServletRequest request, HttpServletResponse response) {
        String filePath = request.getSession().getServletContext().getRealPath("/") + "upload/";
        Map<String, MultipartFile> fileMap = request.getFileMap();
        List<String> list = folderFileService.save(fileMap);
        return list;
    }

    @RequestMapping(value = "/download", method = RequestMethod.GET)
    public void download(FolderFile folderFile, HttpServletResponse response) {
        folderFile = folderFileService.findOne(folderFile.getId());
        File file = new File(folderFile.getPath());
        try {
            response.setContentType(folderFile.getContentType());
            response.setHeader("Content-disposition", "attachment; filename=\"" + Encodes.urlEncode(folderFile.getName()) + "\"");
            FileCopyUtils.copy(FileUtils.readFileToByteArray(file), response.getOutputStream());
        } catch (IOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @RequestMapping(value = "/preview", method = RequestMethod.GET)
    public void preview(FolderFile folderFile, HttpServletResponse response) {
        folderFile = folderFileService.findOne(folderFile.getId());
        File file  = new File(folderFileService.getPreviewUploadPath(folderFile));
        if(file !=null) {
            try {
                response.setContentType("image/png");
                FileCopyUtils.copy(FileUtils.readFileToByteArray(file), response.getOutputStream());
            } catch (IOException e) {
                logger.info(folderFileService.getPreviewUploadPath(folderFile) + "不存在");
            }
        }
    }

    @RequestMapping(value = "/view", method = RequestMethod.GET)
    public void view(FolderFile folderFile, HttpServletResponse response) {
        folderFile = folderFileService.findOne(folderFile.getId());
        if(folderFile!=null){
            File file  = new File(folderFile.getPath());
            try {
                response.setContentType(folderFile.getContentType());
                FileCopyUtils.copy(FileUtils.readFileToByteArray(file), response.getOutputStream());
            } catch (IOException e) {
                throw new ServiceException(e.getMessage());
            }
        }
    }

    @RequestMapping(value = "/uploadFile")
    public String uploadFile(String imgStr, HttpServletRequest request, HttpServletResponse response){
        String key = UUID.randomUUID().toString()+".png";
        Boolean uploadResult = false;
        if(StringUtils.isNotBlank(imgStr)){
            uploadResult = QiniuUpload.upload64image(imgStr,key);
        }
        if(uploadResult){
            return ObjectMapperUtils.writeValueAsString(new RestResponse(Const.QINIU_URL+key));
        }else{
            return ObjectMapperUtils.writeValueAsString(new RestResponse("图片上传失败"));
        }
    }

    @RequestMapping(value = "/getToken")
    public String getToken(Model model){
        String token = "UpToken "+ QiniuUpload.getImgStrUpToken();
        return token;
    }
}
