package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Resume;
import com.spring.future.modules.ih.security.CustomUserDetails;
import com.spring.future.modules.ih.security.CustomUserDetailsService;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.ih.service.JobService;
import com.spring.future.modules.ih.service.ResumeService;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.service.ActivitySignService;
import com.spring.future.modules.translate.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/2.
 */
@RestController
@RequestMapping(value = "api")
public class MainController {
    @Autowired
    private SecurityAuothManager securityAuothManager;
    @Autowired
    private AccountService accountService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private MenuService menuService;
    @Autowired
    private ResumeService resumeService;
    @Autowired
    private ActivitySignService activitySignService;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private JobService jobService;


    @RequestMapping(value = "/login")
    public Boolean login(HttpServletRequest request, HttpServletResponse response,RedirectAttributes redirectAttributes) throws Exception{
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String type = request.getParameter("type");

        Account account = accountService.findByUserName(username);
        if(account==null){
            return false;
        }
        if(!type.equals(account.getType())){
            return false;
        }
        Boolean resume = passwordEncoder.matches(password,account.getPassword());
        if(resume){
            securityAuothManager.login(account);
        }else{
            return false;
        }
        return true;
    }

    @RequestMapping(value = "/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) throws Exception{
        ThreadLocalContext.get().remove();
    }

    @RequestMapping(value = "/isLogin")
    public Boolean isLogin() {
        String accountId = AccountUtils.getAccountId();
        if (StringUtils.isBlank(accountId)) {
            return false;
        } else {
            return true;
        }
    }

    @RequestMapping(value = "/login/getAccount")
    public String getAccount() {
        if (isLogin()) {
            Account account = AccountUtils.getAccount();
            return ObjectMapperUtils.writeValueAsString(account);
        } else {
            return "{}";
        }
    }

    @RequestMapping(value = "/login/getMenus")
    public String getMenus() {
        if (isLogin()) {
            List<Menu> menuList = menuService.getMenu();
            return ObjectMapperUtils.writeValueAsString(menuList);
        } else {
            return "{}";
        }
    }

    @RequestMapping(value = "/login/getChildMenu")
    public String getChildMenu(String id){
        List<Menu> menuList = menuService.findByParent(id);
        return ObjectMapperUtils.writeValueAsString(menuList);
    }

    @RequestMapping(value = "/login/getAuthorityList")
    public String getAuthorityList() {
        List<String> authorityList= SecurityUtils.getAuthorityList();
        return ObjectMapperUtils.writeValueAsString(authorityList);
    }

    @RequestMapping(value = "/login/getUserName")
    public String getAccountName(){
        return AccountUtils.getAccount().getUserName();
    }

    @RequestMapping(value = "/getIndexNum")
    public String getIndexNum(){
        Map<String,Object> map = Maps.newHashMap();
        Integer auditResumeNum= resumeService.findAuditResume();
        Integer auditActivityNum = activitySignService.findAuditNum();
        Integer auditCompanyNum = companyService.findAuditNum();
        Integer auditJobNum = jobService.findAuditNum();
        map.put("auditResumeNum",auditResumeNum);
        map.put("auditActivityNum",auditActivityNum);
        map.put("auditCompanyNum",auditCompanyNum);
        map.put("auditJobNum",auditJobNum);
        return ObjectMapperUtils.writeValueAsString(map);
    }

    @RequestMapping(value = "/findEchartsData")
    public String findEchartsData(){
        Map<String,List<Object>> internMap = accountService.findEchartsData("user");
        Map<String,List<Object>> companyMap = accountService.findEchartsData("company");
        internMap.put("companyNumList",companyMap.get("companyNumList"));
        return ObjectMapperUtils.writeValueAsString(internMap);
    }


    @RequestMapping(value = "/getToken")
    public String getQiNiuToken(){
        String token = QiniuUpload.getImgStrUpToken();
        return ObjectMapperUtils.writeValueAsString(token);
    }
}
