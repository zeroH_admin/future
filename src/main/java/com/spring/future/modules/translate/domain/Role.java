package com.spring.future.modules.translate.domain;

import com.google.common.collect.Lists;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;

import java.util.List;

/**
 * Created by zh on 2017/6/7.
 */
@Entity
@Table(name="ih_role")
public class Role extends DataEntity<Role> {
    private String name;
    private String permission;

    private List<Permission> permissionList = Lists.newArrayList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public List<Permission> getPermissionList() {
        return permissionList;
    }

    public void setPermissionList(List<Permission> permissionList) {
        this.permissionList = permissionList;
    }
}
