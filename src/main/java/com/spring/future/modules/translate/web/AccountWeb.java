package com.spring.future.modules.translate.web;

import com.alibaba.fastjson.serializer.FilterUtils;
import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.service.AccountService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/account")
public class AccountWeb {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Page<Account> page = accountService.findPage(searchEntity.getPageable(),map);
        return ObjectMapperUtils.writeValueAsString(page);
    }


    @RequestMapping(value = "/deleted")
    public String deleted(String id){
        Account account = accountService.findOne(id);
        account.setEnabled(false);
        accountService.update(account);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("禁用成功"));
    }

    @RequestMapping(value = "/add")
    public String add(String userName,String password,String contact,Boolean enabled,String roleId){
        accountService.save(userName,password,contact,enabled,roleId,null);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("账号添加成功"));
    }

    @RequestMapping(value = "/update")
    public String update(String id,String userName,String password,String contact,Boolean enabled,String roleId){
        accountService.save(userName,password,contact,enabled,roleId,id);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("账号更新成功"));
    }

    @RequestMapping(value = "exportData", method = RequestMethod.GET)
    public ModelAndView exportData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<Account> accounts = accountService.finExportData(searchEntity.getParams());
        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "userName", "注册账号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "createdDate", "注册时间"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "lastLoginDate", "最近登录时间"));
        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("账号列表", accounts, simpleExcelColumnList);
        simpleExcelSheetList.add(simpleExcelSheet);
        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"账号列表.xlsx",simpleExcelSheetList);

        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }

}
