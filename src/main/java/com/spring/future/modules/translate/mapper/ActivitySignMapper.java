package com.spring.future.modules.translate.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.domain.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Mapper
public interface ActivitySignMapper extends CrudMapper<ActivitySign,String> {


    Page<ActivitySign> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    ActivitySign findByUserIdAndActivity(@Param("userId")String userId,@Param("activityId")String activityId);

    List<ActivitySign> findByActivityId(String activityId);

    List<ActivitySign> find3ActivitySign(String userId);

    Integer findNumByUser(String userId);

    List<ActivitySign> findPage(@Param("p")Map<String,Object> map);

    Integer findAuditNum();
}
