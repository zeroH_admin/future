package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.service.ActivityService;
import com.spring.future.modules.translate.service.ActivitySignService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/activitySign")
public class ActivitySignWeb {

    @Autowired
    private ActivitySignService activitySignService;

    @ModelAttribute
    public ActivitySign get(@RequestParam(required = false) String id) {
        return StringUtils.isBlank(id) ? new ActivitySign() : activitySignService.findOne(id);
    }


    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Page<ActivitySign> page = activitySignService.findPage(searchEntity.getPageable(),map);
        return ObjectMapperUtils.writeValueAsString(page);
    }


    @RequestMapping(value = "/findOne")
    public String findOne(String id){
        ActivitySign activitySign = activitySignService.findOne(id);
        return  ObjectMapperUtils.writeValueAsString(activitySign);
    }

    public String audit(String id,String status,Boolean sms){
        activitySignService.audit(id,status,sms);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }

    @RequestMapping(value = "exportData", method = RequestMethod.GET)
    public ModelAndView exportData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<ActivitySign> activitySigns = activitySignService.finExportData(searchEntity.getParams());
        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.userName", "姓名"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.mobilePhone", "手机号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.email", "邮箱"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.activityName", "活动名称"));
        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("活动报名列表", activitySigns, simpleExcelColumnList);
        simpleExcelSheetList.add(simpleExcelSheet);
        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"活动报名列表"+ LocalDateTime.now()+".xlsx",simpleExcelSheetList);
        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }

}
