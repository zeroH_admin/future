package com.spring.future.modules.translate.mapper;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.provider.mapper.CrudMapper;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.Article;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Mapper
public interface ArticleMapper extends CrudMapper<Article,String> {

    Page<Article> findPage(Pageable pageable , @Param("p")Map<String,Object> map);

    List<Article> findByStatus(String status);

}
