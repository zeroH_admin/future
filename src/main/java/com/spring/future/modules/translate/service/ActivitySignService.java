package com.spring.future.modules.translate.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.AccountUtils;
import com.spring.future.common.utils.Collections3;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.User;
import com.spring.future.modules.ih.mapper.UserMapper;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.mapper.ActivityMapper;
import com.spring.future.modules.translate.mapper.ActivitySignMapper;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class ActivitySignService {
    @Autowired
    private ActivitySignMapper activitySignMapper;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private ActivityMapper activityMapper;

    public Page<ActivitySign> findPage(Pageable pageable, Map<String, Object> map){
        Page<ActivitySign> page = activitySignMapper.findPage(pageable,map);
        initDomain(page.getContent());
        return page;
    }

    public List<ActivitySign> finExportData( Map<String, Object> map){
        List<ActivitySign> result = Lists.newArrayList();
        String ids =(String) map.get("ids");
        if(StringUtils.isNotBlank(ids)){
            List<String> idsList = Arrays.asList(ids.split(","));
            result = activitySignMapper.findByIds(idsList);
        }else{
            result = activitySignMapper.findPage(map);
        }
        initDomain(result);
        if(Collections3.isNotEmpty(result)){
            List<String> userIds = Collections3.extractToList(result,"userId");
            List<String> activityIds = Collections3.extractToList(result,"activityId");
            Map<String,Activity> activityMap = Maps.newHashMap();
            Map<String,User> userMap = Maps.newHashMap();
            if(Collections3.isNotEmpty(userIds)){
                List<User> userList = userMapper.findByIds(userIds);
                userMap = Collections3.extractToMap(userList,"id");
            }
            if(Collections3.isNotEmpty(activityIds)){
                List<Activity> activities = activityMapper.findByIds(activityIds);
                activityMap = Collections3.extractToMap(activities,"id");
            }
            for(ActivitySign activitySign:result){
                Activity activity = activityMap.get(activitySign.getActivityId());
                User user = userMap.get(activitySign.getUserId());
                activitySign.getExtendMap().put("userName",user.getName());
                activitySign.getExtendMap().put("mobile",user.getMobilePhone());
                activitySign.getExtendMap().put("email",user.getEmail());
                activitySign.getExtendMap().put("activityName",activity.getName());
            }
        }
        return result;
    }

    public ActivitySign findByUserAndActivity(String userId,String activityId){
        return activitySignMapper.findByUserIdAndActivity(userId,activityId);
    }

    public List<ActivitySign> find3ActivitySign(){
        String userId = AccountUtils.getAccount().getUserId();
        List<ActivitySign> list = activitySignMapper.find3ActivitySign(userId);
        for(ActivitySign activitySign :list){
            Activity activity = activityMapper.findOne(activitySign.getActivityId());
            activitySign.setActivity(activity);
        }
        return list;
    }

    public Integer findNumByUser(String userId){
        return activitySignMapper.findNumByUser(userId);
    }

    public List<ActivitySign> findAll(){
        return activitySignMapper.findAll();
    }

    public ActivitySign findOne(String id){
        ActivitySign activitySign = activitySignMapper.findOne(id);
        initDomain(Lists.newArrayList(activitySign));
        return activitySign;
    }

    @Transactional
    public void audit(String id,String status,Boolean sms){
        ActivitySign activitySign = activitySignMapper.findOne(id);
        activitySign.setStatus(status);
        activitySignMapper.update(activitySign);
        if(sms){

        }
    }

    @Transactional
    public void activitySign(String activityId){
        Account account = AccountUtils.getAccount();
        ActivitySign activitySign = new ActivitySign();
        activitySign.setActivityId(activityId);
        activitySign.setUserId(account.getUserId());
        activitySign.setStatus("待审核");
        activitySignMapper.save(activitySign);
    }

    public Integer findAuditNum(){
        return activitySignMapper.findAuditNum();
    }


    private void initDomain(List<ActivitySign> list){
        if(Collections3.isNotEmpty(list)){
            List<String> userIds = Collections3.extractToList(list,"userId");
            List<String> activityIds = Collections3.extractToList(list,"activityId");
            Map<String,Activity> activityMap = Maps.newHashMap();
            Map<String,User> userMap = Maps.newHashMap();
            if(Collections3.isNotEmpty(userIds)){
                List<User> userList = userMapper.findByIds(userIds);
                userMap = Collections3.extractToMap(userList,"id");
            }
            if(Collections3.isNotEmpty(activityIds)){
                List<Activity> activities = activityMapper.findByIds(activityIds);
                activityMap = Collections3.extractToMap(activities,"id");
            }
            for(ActivitySign activitySign:list){
                if(Collections3.isNotEmpty(userMap)){
                    activitySign.setUser(userMap.get(activitySign.getUserId()));
                }
                if(Collections3.isNotEmpty(activityMap)){
                    activitySign.setActivity(activityMap.get(activitySign.getActivityId()));
                }
            }
        }
    }
}
