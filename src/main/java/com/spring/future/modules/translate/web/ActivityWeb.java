package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.Article;
import com.spring.future.modules.translate.service.ActivityService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/activity")
public class ActivityWeb {

    @Autowired
    private ActivityService activityService;

    @ModelAttribute
    public Activity get(@RequestParam(required = false) String id) {
        return StringUtils.isBlank(id) ? new Activity() : activityService.findOne(id);
    }


    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Page<Activity> page = activityService.findPage(searchEntity.getPageable(),map);
        return ObjectMapperUtils.writeValueAsString(page);
    }


    @RequestMapping(value = "/findOne")
    public String findOne(String id){
        Activity activity = activityService.findOne(id);
        return  ObjectMapperUtils.writeValueAsString(activity);
    }

    @RequestMapping(value = "/save")
    public String add(Activity activity){
        activityService.save(activity);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("活动添加成功"));
    }

    @RequestMapping(value = "/delete")
    public String delete(String id){
        Activity activity = activityService.findOne(id);
        activity.setEnabled(false);
        activityService.save(activity);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("活动删除成功"));
    }

    @RequestMapping(value = "/setStatus")
    public String setStatus(String id,String status){
        Activity activity = activityService.findOne(id);
        activity.setStatus(status);
        activityService.save(activity);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("操作成功"));
    }

    @RequestMapping(value = "/findAll")
    public String findAll(){
        List<Activity> activities = activityService.findAll();
        return  ObjectMapperUtils.writeValueAsString(activities);
    }

//
//    @RequestMapping(value = "/update")
//    public String update(String id,String userName,String password,String contact,Boolean enabled,String roleId){
//        accountService.save(userName,password,contact,enabled,roleId,id);
//        return  ObjectMapperUtils.writeValueAsString(new RestResponse("账号更新成功"));
//    }
//
//    @RequestMapping(value = "exportData", method = RequestMethod.GET)
//    public ModelAndView exportData(HttpServletRequest request){
//        ExcelView excelView = new ExcelView();
//        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
//        List<Account> accounts = accountService.finExportData(searchEntity.getParams());
//        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
//        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
//        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
//        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "userName", "注册账号"));
//        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "createdDate", "注册时间"));
//        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "lastLoginDate", "最近登录时间"));
//        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("账号列表", accounts, simpleExcelColumnList);
//        simpleExcelSheetList.add(simpleExcelSheet);
//        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"账号列表.xlsx",simpleExcelSheetList);
//
//        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
//    }

}
