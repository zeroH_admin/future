package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@RestController
@RequestMapping(value = "api/company")
public class CompanyWeb {
    @Autowired
    private CompanyService companyService;
    @Autowired
    private AccountService accountService;

    /**
     * 公司信息填写完全
     * @param request
     * @return
     */
    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<Company> page = companyService.findPage(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(String id){
        Company company = companyService.findOne(id);
        return ObjectMapperUtils.writeValueAsString( company);
    }

    /**
     * 企业列表
     * @param request
     * @return
     */
    @RequestMapping(value = "/companyList")
    public String companyList(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<Account> page = accountService.companyList(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/batchAuditCompany")
    public String batchAuditCompany(@RequestParam(value = "ids[]") String[] ids, String status){
        companyService.batchAuditCompany(ids,status);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }


    @RequestMapping(value = "/auditCompany")
    public String auditCompany(String id,String status,Boolean email,Boolean sms){
        companyService.auditCompany(id,status,email,sms);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }

    @RequestMapping(value = "/search")
    public String search(String key){
        List<Company> companyList = companyService.findByNameLike(key);
        return ObjectMapperUtils.writeValueAsString(companyList);
    }


    @RequestMapping(value = "exportData", method = RequestMethod.GET)
    public ModelAndView exportData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<Company> companyList = companyService.finExportData(searchEntity.getParams());
        SimpleExcelBook simpleExcelBook = exportExcel(companyList);

        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }

    @RequestMapping(value = "exportCompanyData", method = RequestMethod.GET)
    public ModelAndView exportCompanyData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<Company> companyList = companyService.finExportComanpanyData(searchEntity.getParams());
        SimpleExcelBook simpleExcelBook = exportExcel(companyList);
        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }

    private SimpleExcelBook exportExcel(List<Company> companyList){

        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "name", "公司全称"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.userName", "创建账号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "simpleName", "公司简称"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "financingStatus", "融资情况"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "property", "公司性质"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "size", "公司规模"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "address", "公司地址"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "description", "公司简介"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "logo", "企业logo"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.contact", "管理员姓名"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.position", "管理员身份"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.landLine", "管理员座机号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.mobilePhone", "管理员手机号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "createdDate", "注册时间"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.lastLoginDate", "最近登录时间"));
        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("企业列表", companyList, simpleExcelColumnList);
        simpleExcelSheetList.add(simpleExcelSheet);
        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"企业列表"+ LocalDateTime.now()+".xlsx",simpleExcelSheetList);
        return simpleExcelBook;
    }

}
