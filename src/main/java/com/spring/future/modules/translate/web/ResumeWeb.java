package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.lowagie.text.pdf.BaseFont;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.*;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.JobResume;
import com.spring.future.modules.ih.domain.Resume;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.CompanyService;
import com.spring.future.modules.ih.service.JobResumeService;
import com.spring.future.modules.ih.service.ResumeService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by zh on 2017/6/6.
 */
@RestController
@RequestMapping(value = "api/resume")
public class ResumeWeb {
    @Autowired
    private ResumeService resumeService;
    @Value("${root-path}")
    private String rootPath;
    @Autowired
    private JobResumeService jobResumeService;

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<Resume> page = resumeService.findPage(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findPushPage")
    public String findPushPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<JobResume> page = jobResumeService.findPushPage(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(HttpServletRequest request,String id){
        Resume resume = resumeService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(resume);
    }

    @RequestMapping(value = "/audit")
    public String auditCompany(String id,String status,Boolean email,Boolean sms){
        resumeService.audit(id,status,sms,email);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }

    @RequestMapping(value = "/setTag")
    public String setTag(String id,String tag){
        resumeService.setTag(id,tag);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("标记成功"));
    }

    @RequestMapping(value = "/push")
    public String push(String id,String companyId,String jobId,Boolean intern,Boolean company){
        resumeService.push(id,companyId,jobId,intern,company);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("推送成功"));
    }

    @RequestMapping(value = "/downLoadResume")
    public void dowmLoadResume(javax.servlet.http.HttpServletRequest request, HttpServletResponse response, String id) throws Exception{
        Resume resume = resumeService.downLoadResume(id);
        downLoadPdf(request,response,resume);
    }


    private void downLoadPdf(javax.servlet.http.HttpServletRequest request, HttpServletResponse response, Resume resume) throws Exception{
        String filePath =rootPath+"pdf/" + UUID.randomUUID().toString()+ ".pdf";
        OutputStream out = new FileOutputStream(filePath);
        Map<String, Object> map = Maps.newHashMap();
        map.put("resume", resume);

        String htmlStr = HtmlGenerator.generate(rootPath+"template/", "template", map);

        DocumentBuilder builder =  DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")));
        ITextRenderer renderer = new ITextRenderer();

        ITextFontResolver fontResolver = renderer.getFontResolver();
        fontResolver.addFont(rootPath+"/fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
        fontResolver.addFont(rootPath+"/fonts/simhei.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

        renderer.setDocument(doc, null);
        renderer.layout();
        renderer.createPDF(out);
        out.close();
        // 下载文件
        String fileName = resume.getName()+"简历.pdf";
        ExcelUtil.downloadFile(request, response, fileName, filePath);
    }



    @RequestMapping(value = "exportData", method = RequestMethod.GET)
    public ModelAndView exportData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<Resume> companyList = resumeService.finExportData(searchEntity.getParams());
        SimpleExcelBook simpleExcelBook = exportExcel(companyList);

        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }

    private SimpleExcelBook exportExcel(List<Resume> resumeList){

        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "name", "姓名"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "sex", "性别"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "age", "年龄"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "email", "邮箱"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "nationality", "国籍"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "mobilePhone", "手机号码"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "education", "学历"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "description", "公司简介"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "address", "居住地"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "advantage", "优点"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "description", "自我描述"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "qq", "QQ"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "wechat", "微信"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "createdDate", "注册时间"));
        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("简历列表", resumeList, simpleExcelColumnList);
        simpleExcelSheetList.add(simpleExcelSheet);
        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"简历列表"+ LocalDateTime.now()+".xlsx",simpleExcelSheetList);
        return simpleExcelBook;
    }

}
