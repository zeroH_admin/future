package com.spring.future.modules.translate.service;

import com.google.common.collect.Maps;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.domain.Permission;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.mapper.MenuMapper;
import com.spring.future.modules.translate.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class RoleService {
    @Autowired
    private RoleMapper roleMapper;

    public Page<Role> findPage(Pageable pageable, Map<String, Object> map){
        Page<Role> page = roleMapper.findPage(pageable,map);
        return page;
    }

    public Role findByName(String name){
        return roleMapper.findByName(name);
    }

    public Role findOne(String id){
        return roleMapper.findOne(id);
    }
}
