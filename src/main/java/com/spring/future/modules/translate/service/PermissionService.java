package com.spring.future.modules.translate.service;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.domain.Permission;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.mapper.MenuMapper;
import com.spring.future.modules.translate.mapper.PermissionMapper;
import com.spring.future.modules.translate.mapper.RoleMapper;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private MenuMapper menuMapper;

    public List<Permission> findByRole(String roleId){
        return permissionMapper.findPermissionByRole(roleId);
    }

    public Page<Permission> findPage(Pageable pageable, Map<String,Object> map){
        Page<Permission> page = permissionMapper.findPage(pageable,map);
        for(Permission permission:page.getContent()){
            if(StringUtils.isNoneBlank(permission.getMenuId())){
                permission.setMenu(menuMapper.findOne(permission.getMenuId()));
            }
        }
        return page;
    }

    public Permission findOne(String id){
        Permission permission = permissionMapper.findOne(id);
        permission.setMenu(menuMapper.findOne(permission.getMenuId()));
        return permission;
    }

    public void save(Permission permission){
        if(permission.isCreate()){
            permissionMapper.save(permission);
        }else{
            permissionMapper.update(permission);
        }
    }


}
