package com.spring.future.modules.translate.web;

import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.domain.Permission;
import com.spring.future.modules.translate.service.MenuService;
import com.spring.future.modules.translate.service.PermissionService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by zh on 2017/6/10.
 */
@RestController
@RequestMapping(value = "api/permission")
public class PermissionWeb {
    @Autowired
    private PermissionService permissionService;

    @ModelAttribute
    public Permission get(@RequestParam(required = false) String id) {
        return StringUtils.isBlank(id) ? new Permission() : permissionService.findOne(id);
    }

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<Permission> page = permissionService.findPage(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(HttpServletRequest request,String id){
        Permission permission= permissionService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(permission);
    }

//    @RequestMapping(value = "/findFirstMenu")
//    public String findFirstMenu(HttpServletRequest request){
//        List<Menu> menuList = menuService.findFirstMenu();
//        return ObjectMapperUtils.writeValueAsString(menuList);
//    }

    @RequestMapping(value = "/save")
    public String save(HttpServletRequest request,Permission permission){
        permissionService.save(permission);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("保存成功"));
    }

}