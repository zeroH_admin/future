package com.spring.future.modules.translate.service;

import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.AccountUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.Article;
import com.spring.future.modules.translate.mapper.ActivityMapper;
import com.spring.future.modules.translate.mapper.ArticleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class ArticleService {
    @Autowired
    private ArticleMapper articleMapper;


    public Page<Article> findPage(Pageable pageable, Map<String, Object> map){
        return articleMapper.findPage(pageable,map);
    }

    public Article findOne(String id){
        return articleMapper.findOne(id);
    }

    @Transactional
    public void save(Article article){
        article.setTitle( HtmlUtils.htmlUnescape(article.getTitle()));
        article.setContent( HtmlUtils.htmlUnescape(article.getContent()));
        article.setDescription( HtmlUtils.htmlUnescape(article.getDescription()));
        if(article.isCreate()){
            article.setCreatedById(AccountUtils.getAccountId());
            articleMapper.save(article);
        }else{
            articleMapper.update(article);
        }
    }

    public List<Article> findAll(){
        return articleMapper.findAll();
    }

    public List<Article> findByStatus(String status){
        return articleMapper.findByStatus(status);
    }

    @Transactional
    public void addViewNum(Article article){
        article.setViewNum(article.getViewNum()+1);
        articleMapper.update(article);
    }


}
