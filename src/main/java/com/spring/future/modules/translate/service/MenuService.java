package com.spring.future.modules.translate.service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.utils.AccountUtils;
import com.spring.future.common.utils.Collections3;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.mapper.MenuMapper;
import com.sun.org.apache.regexp.internal.RE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class MenuService {
    @Autowired
    private MenuMapper menuMapper;

    public List<Menu> getMenu(){
        List<Menu> menuList = menuMapper.findAll();
        Map<String,Menu> menuMap = Maps.newHashMap();
        for(Menu menu:menuList){
            if("主菜单".equals(menu.getType()) && menu.getEnabled()){
                menuMap.put(menu.getId(),menu);
            }
        }
        for(Menu menu:menuList){
            if(menuMap.containsKey(menu.getParentId()) && menu.getEnabled()){
                menuMap.get(menu.getParentId()).getChildMenu().add(menu);
            }
        }
        return new ArrayList<>(menuMap.values());
    }

    public List<Menu> findByParent(String parentId){
        return menuMapper.findByParent(parentId);
    }

    public Page<Menu> findPage(Pageable pageable,Map<String,Object> map){
        Page<Menu> page = menuMapper.findPage(pageable,map);
        for(Menu menu:page.getContent()){
            if(StringUtils.isNoneBlank(menu.getParentId())){
                menu.setParent(menuMapper.findOne(menu.getParentId()));
            }
        }
        return page;
    }

    public List<Menu> findFirstMenu(){
        return menuMapper.findByType("主菜单");
    }

    public Menu findOne(String id){
        Menu menu = menuMapper.findOne(id);
        return menu;
    }

    public List<Menu> findAll(){
        List<Menu> menus = menuMapper.findAll();
        return menus;
    }

    @Transactional
    public void save(Menu menu){
        if(menu.isCreate()){
            menuMapper.save(menu);
        }else{
            menuMapper.update(menu);
        }
    }

}
