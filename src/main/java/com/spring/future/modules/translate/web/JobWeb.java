package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.domain.Company;
import com.spring.future.modules.ih.domain.Job;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.ih.service.JobService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/job")
public class JobWeb {

    @Autowired
    private JobService jobService;

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Page<Job> page = jobService.findJobPage(searchEntity.getPageable(),map);
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(HttpServletRequest request,String id){
        Job job = jobService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(job);
    }

    @RequestMapping(value = "/batchAudit")
    public String batchAudit(String[] ids,String status,Boolean email,Boolean sms){
        jobService.batchAudit(ids,status,email,sms);
        return ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }


    @RequestMapping(value = "/audit")
    public String audit(String id,String status,Boolean email,Boolean sms){
        jobService.auditJob(id,status,email,sms);
        return ObjectMapperUtils.writeValueAsString(new RestResponse("审核成功"));
    }

    @RequestMapping(value = "/findByCompany")
    public String findByCompany(String companyId){
        List<Job> jobList = jobService.findByCompanyIdAndStatus(companyId,"招聘中");
        return ObjectMapperUtils.writeValueAsString(jobList);
    }

//    @RequestMapping(value = "/update")
//    public String update(String id,String userName,String password,String contact,Boolean enabled,String roleId){
//        accountService.save(userName,password,contact,enabled,roleId,id);
//        return  ObjectMapperUtils.writeValueAsString(new RestResponse("账号更新成功"));
//    }

    @RequestMapping(value = "exportData", method = RequestMethod.GET)
    public ModelAndView exportData(HttpServletRequest request){
        ExcelView excelView = new ExcelView();
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        List<Job> jobList = jobService.finExportData(searchEntity.getParams());
        Workbook workbook = new SXSSFWorkbook(Const.DEFAULT_PAGE_SIZE);
        List<SimpleExcelSheet> simpleExcelSheetList = Lists.newArrayList();
        List<SimpleExcelColumn> simpleExcelColumnList = Lists.newArrayList();
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "name", "职位名称"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "status", "职位状态"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.companyName", "公司名称"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.userName", "创建账号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.contact", "发布人姓名"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.position", "发布人身份"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.landLine", "发布人座机号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "extendMap.mobilePhone", "发布人手机号"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "createdDate", "创建时间"));
        simpleExcelColumnList.add(new SimpleExcelColumn(workbook, "lastModifiedDate", "更新时间"));
        SimpleExcelSheet simpleExcelSheet = new SimpleExcelSheet("职位列表", jobList, simpleExcelColumnList);
        simpleExcelSheetList.add(simpleExcelSheet);
        SimpleExcelBook simpleExcelBook = new SimpleExcelBook(workbook,"职位列表"+ LocalDateTime.now()+".xlsx",simpleExcelSheetList);

        return new ModelAndView(excelView, "simpleExcelBook", simpleExcelBook);
    }


}
