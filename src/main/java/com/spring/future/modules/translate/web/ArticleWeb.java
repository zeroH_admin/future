package com.spring.future.modules.translate.web;

import com.google.common.collect.Lists;
import com.spring.future.common.config.ExcelView;
import com.spring.future.common.domain.RestResponse;
import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.excel.SimpleExcelBook;
import com.spring.future.common.excel.SimpleExcelColumn;
import com.spring.future.common.excel.SimpleExcelSheet;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.Const;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.ih.service.AccountService;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.Article;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.service.ArticleService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/article")
public class ArticleWeb {

    @Autowired
    private ArticleService articleService;

    @ModelAttribute
    public Article get(@RequestParam(required = false) String id) {
        return StringUtils.isBlank(id) ? new Article() : articleService.findOne(id);
    }

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Map<String,Object> map = searchEntity.getParams();
        Page<Article> page = articleService.findPage(searchEntity.getPageable(),map);
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(String id){
        Article article = articleService.findOne(id);
        return ObjectMapperUtils.writeValueAsString(article);
    }

    @RequestMapping(value = "/save")
    public String add(Article article){
        articleService.save(article);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("文章添加成功"));
    }

    @RequestMapping(value = "/delete")
    public String delete(String id){
        Article article = articleService.findOne(id);
        article.setEnabled(false);
        articleService.save(article);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("文章删除成功"));
    }

    @RequestMapping(value = "/setStatus")
    public String setStatus(String id,String status){
        Article article = articleService.findOne(id);
        article.setStatus(status);
        articleService.save(article);
        return  ObjectMapperUtils.writeValueAsString(new RestResponse("操作成功"));
    }


}
