package com.spring.future.modules.translate.service;

import com.google.common.collect.Maps;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.mybatis.domain.Pageable;
import com.spring.future.common.mybatis.domain.Sort;
import com.spring.future.common.utils.AccountUtils;
import com.spring.future.common.utils.Collections3;
import com.spring.future.common.utils.StringUtils;
import com.spring.future.modules.translate.domain.Activity;
import com.spring.future.modules.translate.domain.ActivitySign;
import com.spring.future.modules.translate.domain.Article;
import com.spring.future.modules.translate.domain.Menu;
import com.spring.future.modules.translate.mapper.ActivityMapper;
import com.spring.future.modules.translate.mapper.ActivitySignMapper;
import com.spring.future.modules.translate.mapper.MenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by zh on 2017/6/6.
 */
@Service
public class ActivityService {
    @Autowired
    private ActivityMapper activityMapper;
    @Autowired
    private ActivitySignMapper activitySignMapper;

    public Page<Activity> findPage(Pageable pageable, Map<String, Object> map){
        Page<Activity> page = activityMapper.findPage(pageable,map);
        if(Collections3.isNotEmpty(page.getContent())){
            for(Activity activity:page.getContent()){
                List<ActivitySign> activitySigns = activitySignMapper.findByActivityId(activity.getId());
                if(Collections3.isNotEmpty(activitySigns)){
                    activity.setSignNum(activitySigns.size());
                }
            }
        }
        return page;
    }

    public Page<Activity> findIndexPage(Pageable pageable, Map<String, Object> map){
        map.put("status","发布");
        String tableName = (String) map.get("tableName");
        Page<Activity> page = activityMapper.findIndexPage(pageable,map);
        if("已报名活动".equals(tableName) && Collections3.isNotEmpty(page.getContent())){
            for (Activity activity:page.getContent()){
                ActivitySign activitySign = activitySignMapper.findByUserIdAndActivity(AccountUtils.getAccount().getUserId(),activity.getId());
                activity.setAuditStatus(activitySign.getStatus());
            }
        }
        return page;
    }


    public List<Activity> findByStatus(String status){
        return activityMapper.findByStatus(status);
    }

    public List<Activity> findAll(){
        return activityMapper.findAll();
    }

    public Activity findOne(String id){
        return activityMapper.findOne(id);
    }

    @Transactional
    public void save(Activity activity){
        activity.setName( HtmlUtils.htmlUnescape(activity.getName()));
        activity.setContent( HtmlUtils.htmlUnescape(activity.getContent()));
        activity.setDescription( HtmlUtils.htmlUnescape(activity.getDescription()));
        if(activity.isCreate()){
            activity.setCreatedById(AccountUtils.getAccountId());
            activityMapper.save(activity);
        }else{
            activityMapper.update(activity);
        }
    }

    @Transactional
    public void addViewNum(Activity activity){
        activity.setViewNum(activity.getViewNum()+1);
        activityMapper.update(activity);
    }


}
