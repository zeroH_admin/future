package com.spring.future.modules.translate.domain;

import com.google.common.collect.Lists;
import com.spring.future.common.domain.DataEntity;
import com.spring.future.common.mybatis.annotation.Entity;
import com.spring.future.common.mybatis.annotation.Table;
import com.spring.future.common.utils.StringUtils;

import java.util.List;

/**
 * Created by zh on 2017/6/6.
 */

@Entity
@Table(name="ih_menu")
public class Menu extends DataEntity<Menu>{
    private String name;
    private String url;
    private String parentId;
    private String type;
    private Integer sort;
    private String code;

    private Menu parent;

    private List<Menu> childMenu = Lists.newArrayList();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        if(StringUtils.isBlank(url)){
            url=code;
        }
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<Menu> getChildMenu() {
        return childMenu;
    }

    public void setChildMenu(List<Menu> childMenu) {
        this.childMenu = childMenu;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Menu getParent() {
        return parent;
    }

    public void setParent(Menu parent) {
        this.parent = parent;
    }
}
