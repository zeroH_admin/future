package com.spring.future.modules.translate.web;

import com.spring.future.common.domain.SearchEntity;
import com.spring.future.common.mybatis.domain.Page;
import com.spring.future.common.utils.ObjectMapperUtils;
import com.spring.future.common.utils.RequestUtils;
import com.spring.future.modules.ih.domain.Account;
import com.spring.future.modules.translate.domain.Role;
import com.spring.future.modules.translate.service.RoleService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zh on 2017/6/7.
 */
@RestController
@RequestMapping(value = "api/role")
public class RoleWeb {
    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/findPage")
    public String findPage(HttpServletRequest request){
        SearchEntity searchEntity = RequestUtils.getSearchEntity(request);
        Page<Role> page = roleService.findPage(searchEntity.getPageable(),searchEntity.getParams());
        return ObjectMapperUtils.writeValueAsString(page);
    }

    @RequestMapping(value = "/findOne")
    public String findOne(HttpServletRequest request,String id){
        return ObjectMapperUtils.writeValueAsString(roleService.findOne(id));
    }



}
