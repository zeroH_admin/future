var codeflag=false;
var opts = {
    "element": "graphic-verification", // 可以是验证码容器id，也可以是HTMLElement
    "captchaId": "YOUR_CAPTCHA_ID", // 这里填入申请到的验证码id
    "width": 390, // 验证码组件显示宽度
    "verifyCallback": function(ret){ // 用户只要有拖动/点击，就会触发这个回调
        if(ret['value']){ // true:验证通过 false:验证失败
            // 通过 ret["validate"] 可以获得二次校验数据
            codeflag=true;// 用户完成拖动之后再启用提交按钮
        }else{
            codeflag=false;
        }
    }
}
new NECaptcha(opts);

//图形验证码
$(".ncpt_slide_fg img,.ncpt_txt_status img").remove();
$(".ncpt_contents").eq(1).css("display","block");

