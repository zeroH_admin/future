//选项卡
$(".planning-type div").each(function () {
    var index=$(this).index();
    $(this).click(function () {
        $(".select-line div").removeClass("line-one").eq(index).addClass("line-one");
        $(".switch form").hide().eq(index).show();
    })
});

$(".add").click(function () {
    $(this).siblings(".id-list").toggle();
    if($(this).siblings(".id-list").css("display")=="block"){
        $(this).parent().css("border","1px solid #2d61e6");
    }else {
        $(this).siblings("input").blur();
    }

});
$(".id-list li").click(function () {
    $(this).parent().parent().css("border","1px solid #e6e6e6");
    $(this).parent().parent().siblings("p").hide();
});
