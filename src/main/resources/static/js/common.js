
$("input:not([type|=button]):not(.hasDatepicker),textarea").focus(function () {
    $(this).css("border","1px solid #2d61e6");
}).blur(function () {
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).siblings("p").show().text("不能为空");
    }else {
        $(this).css("border","1px solid #e6e6e6");
        $(this).siblings("p").hide();
    }
});
// function button(eve,verification) {
//     eve.click(function () {
//         console.log(verification)
//         eve.css("background","#0d3fbe");
//         var inputbox=eve.parent().find($("input:not([type|=button]):not([type|=hidden])"));
//         inputbox.not(eve.parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
//             if($(this).val()==""){
//                 $(this).css("border","1px solid #ff0000");
//                 $(this).siblings("p").show().text("不能为空");
//                 $(this).parent().siblings("p").show().text("不能为空");
//                 return false;
//             }
//         })
//     });
// }
//电话验证
function phone(tel,text) {
    if(!tel.val().match(/^1[34578]\d{9}$/)) {
        text.show().text("用户名格式错误");
        tel.css("border","1px solid #ff0000");
    }else{
        text.hide();
        phoneflag=true;
    }
}

//邮箱验证
function contains(arr, obj) {
    // var i = arr.length;
    for (var i=0;i<=arr.length;i++){
        if(arr[i]==obj){
            return true
        }
    }
    return false;
}
function email(mail,text) {
    if(mail.val()==""){
        text.show();
        text.text("用户名不能为空");
        mail.css("border","1px solid #ff0000");

    }else if(!mail.val().match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)) {
        text.show();
        text.text("用户名格式错误");
        mail.css("border","1px solid #ff0000");
    }else{
        text.hide();
        hrflag=true;
    }
}
//密码验证
function password(pwd,pwdtext) {
    if(pwd.val()==""||!pwd.val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/)){
        pwdtext.show();
        pwd.css("border","1px solid #ff0000");
    }else {
        pwdtext.hide();
    }
}
