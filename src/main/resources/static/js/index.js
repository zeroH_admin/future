$("#fullpage").fullpage({
    'fixedElements': '.header',
    // 'anchors':['page1','page2','page3','page4','page5'],
    navigation:true,
    // navigationTooltips:['首页','视觉','交互','皮肤','功能'],
    'scrollingSpeed':1000
})
var fpnav=$("#fp-nav");
$("span",fpnav).click(function () {
    $("span",fpnav).css({
        width: '12px',
        height: '12px',
        border: '1px solid #9b9b9b',
        background: 'rgba(0,0,0,0)',
        margin: 0
    }).filter(this).css({
        width: '12px',
        height: '12px',
        border: '2px solid #9b9b9b',
        background: 'rgba(0,0,0,0)',
        margin: 0
    })
})
$(".three li").hover(function () {
    $(this).children(".hover").show();
    $(this).children(".mask").hide();
},function () {
    $(this).children(".hover").hide();
    $(this).children(".mask").show();
})
$(".five-list").hover(function () {
    $(this).addClass("active-btn")
},function () {
    $(this).removeClass("active-btn")
});
$(".banner").width(($(".banner li").width()+28)*$(".banner li").length-28);
$(".btn-right").click(function () {
    $(".btn-left").show()
    if(Number(-$(".banner").css("left").slice(0,-2))>=($(".banner li").length-4)*226){
        $(this).hide();
    }else {
        $(this).show();
        var length=Number($(".banner").css("left").slice(0,-2));
        $(".banner").animate({left:length-226+"px"});
    }
})
$(".btn-left").click(function () {
    $(".btn-right").show()
    if(Number(-$(".banner").css("left").slice(0,-2))<=0){
        $(this).hide();
    }else {
        $(this).show();
        var length=Number($(".banner").css("left").slice(0,-2));
        $(".banner").animate({left:length+226+"px"});
    }
})
