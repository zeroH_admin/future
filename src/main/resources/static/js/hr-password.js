$("#word").blur(function () {
    if($(this).val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/)){
        $(this).css("border","1px solid #e6e6e6");
        $(".button").removeAttr ("disabled");
        $(".reset").hide();
    }else {
        $(this).css("border","1px solid #ff0000");
        $(".button").attr("disabled",true);
        $(".reset").show().text("密码格式错误");
    }
})
$("#word1").blur(function () {
    if($("#word").val()!=$("#word1").val()){
        $(".pwd").show().text("两次密码不一致");
        $("#word1").css("border","1px solid #ff0000");
        $(".button").attr("disabled",true);
    }else {
        $("#word1").css("border","1px solid #e6e6e6");
        $(".button").removeAttr ("disabled");
        $(".pwd").hide();
    }
})
$("#new").blur(function () {
    $.get(
        "/register/checkPassword",
        {userName:$(".user span").text(),
        password:$("#new").val()},
        function(data,state){
            if(data==true){
                $("#new").css("border","1px solid #e6e6e6");
                $(".new-pwd").hide();
                $(".button").removeAttr ("disabled");
            }else {
                $("#new").css("border","1px solid #ff0000");
                $(".new-pwd").show().text("原始密码不正确");
                $(".button").attr("disabled",true);
            }
            //这里显示返回的状态
        }
    )
})