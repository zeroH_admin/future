$(".contact-name input,.contact-landline input,.contact-phone input,.graphic-verification input,.phone-verification input").focus(function () {
    $(this).css("border","1px solid #2d61e6")
}).blur(function () {
    $(this).css("border","1px solid transparent");
});
// 刷新图片
function changeImg() {
    var imgSrc = $(".graphic");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", changeUrl(src));
}
//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function changeUrl(url) {
    var timestamp = (new Date()).valueOf();
    var index = url.indexOf("?",url);
    if (index > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//图形验证
$("#graphic").blur(function () {
    var imgCode = $("#graphic").val();
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(imgCode!==""){
        $.get(
            "/register/checkCode",
            {code:imgCode},
            function(data,state){
                if(data==true){
                    $("#graphic").css("border","1px solid #e6e6e6");
                    $("#graphic").parent().siblings("p").hide();
                }else {
                    $("#graphic").css("border","1px solid #ff0000");
                    $("#graphic").parent().siblings("p").show().text("验证码输入错误");
                    $(".reset-password").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )
    }else {
        $("#graphic").css("border","1px solid #ff0000");
        $("#graphic").parent().siblings("p").show().text("验证码不能为空");
    }
});
//手机验证码
var phoneflag=false;
$("#phone").blur(function () {
    $.get(
        "/register/checkMobileCode",
        {
            userName:$("#name").val(),
            code:$("#phone").val()
        },
        function(data,state){
            if(!data){
                $("#phone").css("border","1px solid #ff0000");
                $("#phone").siblings("p").show().text("验证码输入错误");
                phoneflag=false;
            }else {
                $("#phone").css("border","1px solid #e6e6e6");
                $("#phone").siblings("p").hide();
                phoneflag=true;
            }
        }
    )
})
//姓名
var contactname=$(".contact-name input");
var contactlandline=$(".contact-landline input");
var contactphone=$(".contact-phone input");
contactname.blur(function () {
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show();
    }else {
        $(this).next().hide();
    }
});
//固定电话
contactlandline.blur(function () {
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show().text("不能为空");
    }else if(!$(this).val().match(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/)){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show();
    }else {
        $(this).next().hide();
    }
});
//手机号
var contactflag=false;
contactphone.blur(function () {
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show().text("不能为空");
    }else if(!$(this).val().match(/^1[34578]\d{9}$/)){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show().text("格式错误");
    }else {
        $(this).next().hide();
        contactflag=true;
    }
});


var verification=2;
// button($(".contact-button"),verification);
if(verification==3){
    $(".graphic-verification").show();
}else {
    $(".graphic-verification").hide();
}
var contact=angular.module("contact",[]);
contact.controller('contactCtrl',function ($scope,$http,$window) {
    $scope.data={
        name:'',
        simpleName:'',
        financingStatus:'',
        property:'',
        size:'',
        address:'',
        description:'',
        contact:'',
        position:'',
        landLine:'',
        mobilePhone:''
    };

    $scope.pro=[];
    $scope.array=function (text) {
        $scope.pro.push(text);
        $scope.a="";
        for (var i=0;i<$scope.pro.length;i++){
            if($scope.a.indexOf($scope.pro[i])>0){
                $scope.a.replace($scope.pro[i],"");
            }else {
                $scope.a += " " + $scope.pro[i];
            }
        }
        $scope.data.property=$scope.a
    }

    $scope.next=function () {
        if($scope.data.name==""){
            $(".name-required-input").css("border","1px solid #ff0000");
            $(".name-required-input").siblings("p").show().text("不能为空");
        }else if($scope.data.simpleName==""){
            $(".abbreviation-required-input").css("border","1px solid #ff0000");
            $(".abbreviation-required-input").siblings("p").show().text("不能为空");
        }else if($scope.data.financingStatus==""){
            $(".finance").css("border","1px solid #ff0000");
            $(".finance").siblings("p").show().text("不能为空");
        }else if($scope.data.property==""){
            $(".nature").css("border","1px solid #ff0000");
            $(".nature").siblings("p").show().text("不能为空");
        }else if($scope.data.size==""){
            $(".scale").css("border","1px solid #ff0000");
            $(".scale").siblings("p").show().text("不能为空");
        }else if($scope.data.address==""){
            $(".address-required-input").css("border","1px solid #ff0000");
            $(".address-required-input").siblings("p").show().text("不能为空");
        }else if($scope.data.description==""){
            $(".description").css("border","1px solid #ff0000");
            // $(".address-required-input").siblings("p").show().text("不能为空");
        }else {
            $(".settled2").show();
            $(".not-settled").hide();
            $(".line1").addClass("step-two-line");
            $(".line1 p").addClass("step-two-circular");
            $(".line2").removeClass("step-two-line");
            $(".line2 p").removeClass("step-two-circular");
        }
    }
    //提交

    $scope.submit=function () {
        if($scope.data.contact==""){
            $(".contact-name input").css("border","1px solid #ff0000");
            $(".contact-name p").siblings("p").show().text("不能为空");
        }else if($scope.data.position==""){
            $(".position-box").css("border","1px solid #ff0000");
            $(".position-box").siblings("p").show().text("不能为空");
        }else if($scope.data.landLine==""||(!$scope.data.landLine.match(/^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/))){
            $(".contact-landline input").css("border","1px solid #ff0000");
            $(".contact-landline p").show().text("电话格式错误");
        }else if($scope.data.mobilePhone==""||(!$scope.data.mobilePhone.match(/^1[34578]\d{9}$/))){
            $(".contact-phone input").css("border","1px solid #ff0000");
            $(".contact-phone p").show().text("手机格式错误");
        }else if($("#phone").val()==""||!phoneflag){
            $("#phone").css("border","1px solid #ff0000");
            $("#phone").parent().siblings("p").show().text("验证码输入错误");
        }else {
            $http({
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: '/register/saveContact',
                data:$.param($scope.data)
            }).then(function successCallback(response) {
                // 请求成功执行代码
                alert("保存成功");
                window.location.href="/auditNotice";
                //跳转到公司首页
            }, function errorCallback(response) {
                // 请求失败执行代码
                alert("保存失败");
                //刷新本页面重新保存
            });
        }
    }

})

