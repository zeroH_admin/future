
$("textarea").focus(function () {
    $(this).css("border","1px solid #2d61e6");
});
$(".textarea-box textarea").focus(function () {
    $(this).css("border","1px solid transparent");
    $(this).parent().css("border","1px solid #2d61e6");
})
$(".resume-textarea textarea").focus(function () {
    $(this).css("border","1px solid transparent");
    $(this).parent().css("border","1px solid #2d61e6");
});
$(".textarea-box textarea").keyup(function () {
    $(".max-num span").text($(".textarea-box textarea").val().length);
    if($(".textarea-box textarea").val().length>=500){
        $(".max-num").css("color","#ff0000").animate({
            right:'25px'
        },100).animate({
            right:'15px'
        },100).animate({
            right:'20px'
        },100)
        $(".max-num span").css("color","#ff0000");
    }else {
        $(".max-num").css("color","#333");
        $(".max-num span").css("color","#333");
    };
});
$(".info-four input").keyup(function () {
    $(".input-max-num span").text($(".info-four input").val().length);
    if($(".info-four input").val().length>=30){
        $(".input-max-num").css("color","#ff0000").animate({
            right:'25px'
        },100).animate({
            right:'15px'
        },100).animate({
            right:'20px'
        },100)
        $(".input-max-num span").css("color","#ff0000");
    }else {
        $(".input-max-num").css("color","#333");
        $(".input-max-num span").css("color","#333");
    };
});
