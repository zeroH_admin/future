var height=$(".information-top-right").get(0).offsetHeight;
$(".line").css('height', height+"px");
//添加
$(".btnshow").click(function () {
    var index = $(this).siblings("a").attr("title");
    var className = '.' + 'index-' + index;
    $(className).removeClass("dn");
    $(className).find("form").removeClass("dn");
    // $(this).addClass("hide")
    $(".disa").attr("disabled","true");
    $(this).siblings("a").parent().siblings().find("span").css("color","#333");
    $(this).siblings("a").find("span").css("color","#2d61e6");
    $(this).siblings("a").parent().siblings().find("i").addClass("dn");
    $(this).siblings("a").find("i").removeClass("dn");
    var id = '.' + 'index-' + index;
    $("html,body").animate({
        scrollTop: $(id).offset().top
    });
});
//显示
$(".append").click(function(event) {
    var index = $(this).siblings("a").attr("title");
    $(this).hide();
    $(this).siblings(".delete").show();
    var className = '.' + 'index-' + index;
    $(className).removeClass("dn");
    $(this).siblings("a").parent().siblings().find("span").css("color","#333");
    $(this).siblings("a").find("span").css("color","#2d61e6");
    $(this).siblings("a").parent().siblings().find("i").addClass("dn");
    $(this).siblings("a").find("i").removeClass("dn");
    var id = '.' + 'index-' + index;
    $("html,body").animate({
        scrollTop: $(id).offset().top
    });
});
//隐藏
$(".delete").click(function () {
    var index = $(this).siblings("a").attr("title");
    $(this).hide();
    $(this).siblings(".append").show();
    var className = '.' + 'index-' + index;
    $(className).addClass("dn");
});
$(".add").parent().click(function () {
    // console.log($(this).children(".id-list").toggle())
    $(this).children(".id-list").toggle();
});
$(".add").click(function () {
    $(this).siblings(".id-list").toggle();
})
$(".id-list li").click(function () {
    $(this).parent().siblings("input").val($(this).text());
    // $(this).parent().hide()
})
$(".addition").click(function () {
    var index=$(this).parents(".parent").attr("class").slice(-1);
    $(".classify a").each(function (i) {
        if(i==index){
            $(this).children("span").css("color","#2d61e6");
            $(this).children("i").removeClass("dn");
            return false;
        }else {
            $(this).children("span").css("color","#333");
            $(this).children("i").addClass("dn");
        }
    })
});
$("input:not([type|=button])").focus(function () {
    if($(this).parent().attr("class").slice(-9)=="id-select"){
        // $(this).parent().css("border","1px solid #2d61e6");
    }else {
        $(this).css("border","1px solid #2d61e6");
    }
}).blur(function () {
    if($(this).parent().attr("class").slice(-9)=="id-select"){
        if($(this).val()==""){
            // $(this).parent().css("border","1px solid #ff0000");
            // $(this).parent().siblings("p").show().text("不能为空");
        }else {
            $(this).parent().css("border","1px solid #e6e6e6");
            $(this).parent().siblings("p").hide();
        }
    }else {
        if($(this).val()==""){
            $(this).css("border","1px solid #ff0000");
            $(this).siblings("p").show().text("不能为空");
        }else {
            $(this).css("border","1px solid #e6e6e6");
            $(this).siblings("p").hide();
        }
    }
});
//跳转
$(".classify li a").click(function() {
    $(this).parent().siblings().find("span").css("color","#333");
    $(this).find("span").css("color","#2d61e6");
    $(this).parent().siblings().find("i").addClass("dn");
    $(this).find("i").removeClass("dn");
    var index = this.title;
    var id = '.' + 'index-' + index;
    $("html,body").animate({
        scrollTop: $(id).offset().top
    });
});
// $(".classify li").click(function() {
//     $(this).siblings().find("span").css("color","#333");
//     $(this).find("span").css("color","#2d61e6");
//     $(this).siblings().find("i").addClass("dn");
//     $(this).find("i").removeClass("dn");
//     var index = $(this).children("a")[0].title;
//     var id = '.' + 'index-' + index;
//     $("html,body").animate({
//         scrollTop: $(id).offset().top
//     });
// });
$(".form_date img").click(function () {
    $(this).parent().css("border","1px solid #2d61e6");
})
var elm = $('.right');
var startPos = $(elm).offset().top;
$.event.add(window, "scroll", function() {
    var p = $(window).scrollTop();
    $(elm).css('position',((p) > (startPos+$(".information-bottom").outerHeight(true)-280)) ? 'fixed' : 'static');
    $(elm).css('top',((p) > (startPos+$(".information-bottom").outerHeight(true)-280)) ? '200px': '');
    $(elm).css('right',((p) > (startPos+$(".information-bottom").outerHeight(true)-280)) ? '50%' : '');
    $(elm).css('margin-right',((p) > (startPos+$(".information-bottom").outerHeight(true)-280)) ? '-376px' : '');
});
var resume= angular.module('resume', []);

resume.controller('resumeCtrl', function($scope,$http,$window){
    $http({
        url:'../json/nation.json',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.nationCandidate = result.data;
    });
    $scope.min='';
    $scope.max='';
    $scope.info={
        id:'',
        image:'',
        name:'',
        sex:'',
        birthday:'',
        mobilePhone:'',
        email:'',
        nationality:'',
        address:'',
        workYear:'',
        education:'',
        advantage:'',
        description:'',
        cardType:'',
        idcard:'',
        qq:'',
        wechat:'',
        educationJson:'',
        workExperienceJson:'',
        schoolExperienceJson:'',
        skillJson:'',
        languageJson:'',
        certificateJson:'',
        expectWorkJson:''
    };

    $scope.open=function () {
        $(".open").toggleClass("dn")
    };
    $http({
        url:'/intern/getAccountResume',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.info = result.data;
        $scope.educationList = JSON.parse(result.data.educationJson);
        $scope.workExperienceList = result.data.workExperienceJson==undefined?[]:JSON.parse(result.data.workExperienceJson);
        $scope.schoolExperienceList = result.data.schoolExperienceJson==undefined?[]:JSON.parse(result.data.schoolExperienceJson);
        $scope.skillList = result.data.skillJson==undefined?[]:JSON.parse(result.data.skillJson);
        $scope.languageList = result.data.languageJson==undefined?[]:JSON.parse(result.data.languageJson);
        $scope.certificateList = result.data.certificateJson==undefined?[]:JSON.parse(result.data.certificateJson);
        $scope.expectWorkList = JSON.parse(result.data.expectWorkJson);
        if($scope.expectWorkList[0].availableTime){
            $scope.full="元/日"
        }else {
            $scope.full="元/月"
        }
        // $(".show").removeClass("dn")
        $scope.country=$scope.info.address.split("-")[0]
        $scope.c_city=$scope.info.address.split("-")[1];
        $scope.min=$scope.expectWorkList[0].expectedSalary.split("-")[0];
        $scope.max=$scope.expectWorkList[0].expectedSalary.split("-")[1];
        $scope.flag1=true;
        $scope.flag2=true;
        $scope.flag3=true;
        $scope.flag4=true;
        $scope.flag5=true;
        $scope.flag6=true;
        $scope.$watch('alldata',function(){
            function isEmpty(value) {
                return (Array.isArray(value) && value.length === 0) || (Object.prototype.isPrototypeOf(value) && Object.keys(value).length === 0);
            }
            if((!isEmpty($scope.educationList))&&$scope.flag1){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+10+'%');
                $scope.flag1=false;
                $(".education").removeClass("dn")
            }
            if(isEmpty($scope.educationList)&&(!$scope.flag1)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-10+'%');
                $scope.flag1=true;
                $(".education").addClass("dn")
            }
            if((!isEmpty($scope.workExperienceList))&&$scope.flag2){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+40+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+20+'%');
                $scope.flag2=false;
                $(".work").removeClass("dn")
            }
            if(isEmpty($scope.workExperienceList)&&(!$scope.flag2)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-40+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-20+'%');
                $scope.flag2=true;
                $(".work").addClass("dn")
            }
            if((!isEmpty($scope.schoolExperienceList))&&$scope.flag3){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+10+'%');
                $scope.flag3=false;
                $(".school").removeClass("dn")
            }
            if(isEmpty($scope.schoolExperienceList)&&(!$scope.flag3)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-10+'%');
                $scope.flag3=true;
                $(".school").addClass("dn")
            }
            if((!isEmpty($scope.skillList))&&$scope.flag4){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+10+'%');
                $scope.flag4=false;
                $(".skill").removeClass("dn")
            }
            if(isEmpty($scope.skillList)&&(!$scope.flag4)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-10+'%');
                $scope.flag1=true;
                $(".skill").addClass("dn")
            }
            if((!isEmpty($scope.languageList))&&$scope.flag5){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+10+'%');
                $scope.flag5=false;
                $(".language").removeClass("dn");
            }
            if(isEmpty($scope.languageList)&&(!$scope.flag5)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-10+'%');
                $scope.flag5=true;
                $(".language").addClass("dn")
            }
            if((!isEmpty($scope.certificateList))&&$scope.flag6){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width+20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))+10+'%');
                $scope.flag6=false;
                $(".certificate").removeClass("dn")
            }
            if(isEmpty($scope.certificateList)&&(!$scope.flag6)){
                var width=Number($(".complete").css("width").slice(0,-2));
                $(".complete").css("width",width-20+'px');
                $(".percentage").text(Number($(".percentage").text().slice(0,-1))-10+'%');
                $scope.flag6=true;
                $(".certificate").addClass("dn")
            }
        },true);
    });

    //个人信息
    //国际
    $scope.f=function (data,event) {
        $scope.info.nationality=data;
        $(event.target).parent().parent().css("border","1px solid  #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    };
    //编辑
    $scope.compile=function (event) {
        $(event.target).parent().siblings(".form-box").removeClass("dn");
        $(event.target).parent().addClass("dn");
        $(".addition,.compile").attr("disabled",true);
        $(".right").hide();
    };
    //
    //编辑
    $scope.edit=function (event,index) {
        $scope.setAddIndex=index;
        $scope.isAdded = false;
        $(event.target).parent().parent().siblings(".form-box").removeClass("dn");
        // $(event.target).parent().parent().addClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.informationCancel=function () {
        $(".edu").addClass("dn");
        $(".information-top").removeClass("dn");
        $(".right").show();
        $(".addition,.compile").removeAttr("disabled");
    };

    $scope.PersonSave=function () {
        $scope.info.image=$("#result #img").attr("src");
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);
        if($scope.info.workYear==""){
            $(".year").css("border","1px solid #ff0000");
            $(".year").siblings("p").show().text("不能为空")
        }else {
            $(".year").css("border","1px solid #e6e6e6");
            $(".year").siblings("p").hide()
            $http({
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: '/intern/saveResume',
                data:$.param($scope.info)
            }).then(function successCallback(response) {
                $(".edu").addClass("dn");
                $(".information-top").removeClass("dn");
                $(".addition,.compile").removeAttr("disabled");
                $(".right").show();

            }, function errorCallback(response) {
                // 请求失败执行代码
                alert("保存失败");
                //刷新本页面重新保存

            });
        }

    }
    $scope.save=function (event,data) {
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);
        var a
        $(event.target).parents(".form-box").find("input:not([type|=button])").each(function () {
            if($(this).val()==""){
                if($(this).parent().attr("class").slice(-9)=="id-select"){
                    $(this).parent().css("border","1px solid #ff0000");
                    $(this).parent().siblings("p").show().text("不能为空");
                    a=false;
                    return false;
                }else {
                    $(this).css("border","1px solid #ff0000");
                    $(this).siblings("p").show().text("不能为空");
                    a=false;
                    return false;
                }
            }else{
                if($(this).parent().attr("class").slice(-9)=="id-select"){
                    $(this).parent().css("border","1px solid #e6e6e6");
                    $(this).parent().siblings("p").hide();
                    a=true;
                    return true;
                }else {
                    $(this).css("border","1px solid #e6e6e6");
                    $(this).siblings("p").hide();
                    a=true;
                    return true;
                }
            }
        });
        if (a){
            $http({
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: '/intern/saveResume',
                data:$.param($scope.info)
            }).then(function successCallback(response) {
                $(".eduExpForm").addClass("dn");
                $(".information-top").removeClass("dn");
                $(".addition,.compile").removeAttr("disabled");
                // $window.location.reload();
                $(".disa").removeAttr("disabled")
                $scope.alldata=$scope.educationList+$scope.workExperienceList+$scope.schoolExperienceList+$scope.skillList+$scope.languageList+$scope.certificateList
            }, function errorCallback(response) {
                // 请求失败执行代码
                alert("保存失败");
                //刷新本页面重新保存

            });
        }

    };
    //居住地
    $scope.country= $scope.info.address;
    $scope.c_city='';
    $scope.pro=[];
    $scope.Tline=function () {
        $http({
            url:'/json/province.json',
            method:'GET'
        }).then(function (result) {  //正确请求成功时处理
            $scope.line = result.data;
        });
    }
    $scope.lineClick=function (data,event) {
        $scope.country=data;
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+"-"+$scope.c_city;
    }
    $scope.Nline=function (data) {
        var arr=[];
        var rel=[];
        $http({
            url:'/json/province.json',
            method:'GET',
        }).then(function (result) {  //正确请求成功时处理
            angular.forEach(result.data, function(d,index,array){//data等价于array[index]
                if(d.province==data){
                    arr.push(d.citys)
                }
            });
            angular.forEach(arr, function(d,index,array){//data等价于array[index]
                rel=d;
            });
            $scope.con = rel;
        });
    }
    $scope.NameClick=function (data,event) {
        $scope.c_city=data;
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+"-"+$scope.c_city;
    };
    //期望工作编辑
    $scope.expectEdit=function () {
        $(".expectEdit").removeClass("dn");
        $(".expectShow").addClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.expectCancel=function () {
        $(".expectEdit").addClass("dn");
        $(".expectShow").removeClass("dn");
        $(".addition,.compile").removeAttr("disabled");
    };

    //添加
    $scope.addition=function (event,index) {
        $scope.setAddIndex=index;
        $scope.isAdded = true;
        $(event.target).parent().siblings().children(".eduExpForm").removeClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.btnshow=function (index) {
        $scope.setAddIndex=index;
        $scope.isAdded = true;
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.internSave=function () {
        $scope.info.image=$("#result #img").attr("src");
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);

        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/intern/saveResume',
            data:$.param($scope.info)
        }).then(function successCallback(response) {
            $(".edu").addClass("dn");
            $(".information-top").removeClass("dn");
            $(".addition,.compile").removeAttr("disabled");
            $(".right").show();
            $scope.full="元/日"
            $scope.alldata=$scope.educationList+$scope.workExperienceList+$scope.schoolExperienceList+$scope.skillList+$scope.languageList+$scope.certificateList
        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存

        });
    }
    $scope.expectSave=function () {
        $scope.expectWorkList[0].availableTime='';
        $scope.expectWorkList[0].expectedPeriod=''
        $scope.info.image=$("#result #img").attr("src");
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);

        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/intern/saveResume',
            data:$.param($scope.info)
        }).then(function successCallback(response) {
            $(".edu").addClass("dn");
            $(".information-top").removeClass("dn");
            $(".addition,.compile").removeAttr("disabled");
            $(".right").show();
            $scope.full="元/月"
            $scope.alldata=$scope.educationList+$scope.workExperienceList+$scope.schoolExperienceList+$scope.skillList+$scope.languageList+$scope.certificateList
        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存

        });
    }
    //删除
    $scope.delete=function (event,box,data) {
        data.splice($scope.setAddIndex,1);
        box=data;
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);
        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/intern/saveResume',
            data:$.param($scope.info)
        }).then(function successCallback(response) {
            // 请求成功执行代码
            // alert("保存成功");
            $(event.target).parents(".form-box").addClass("dn");
            $(".addition,.compile").removeAttr("disabled");
            $(event.target).parents(".form-box").siblings().removeClass("dn");
            // $window.location.reload();
            $scope.alldata=$scope.educationList+$scope.workExperienceList+$scope.schoolExperienceList+$scope.skillList+$scope.languageList+$scope.certificateList
        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存

        });
    };
    //保存


    //取消按钮
    $scope.cancel=function (event,data) {
        if($scope.isAdded){
            if (data.length == $scope.setAddIndex + 1) {
                data.pop();
            }
        }
        $(event.target).parent().parent().addClass("dn");
        $(event.target).parent().parent().siblings().removeClass("dn");
        $(".addition,.compile").removeAttr("disabled");
        // $window.location.reload();
        $scope.alldata=$scope.educationList+$scope.workExperienceList+$scope.schoolExperienceList+$scope.skillList+$scope.languageList+$scope.certificateList
    };
});
