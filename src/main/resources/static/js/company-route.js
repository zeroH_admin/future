$("header a").click(function () {
    $(this).parents("ul").children().removeClass("checked")
    $(this).parent().addClass("checked")
})

var myapp=angular.module('myapp',['ui.router'])
    myapp.config(function($stateProvider,$urlRouterProvider){
        $urlRouterProvider.otherwise('/index');
        $stateProvider.state('position',{
            url:'/position',
            templateUrl:'position-route.html'
        }).state('interview',{
            url:'/interview',
            templateUrl:'interview.html'
        }).state('index',{
            url:'/index',
            templateUrl:'hr-index.html'
        }).state('position.recruit',{
            url:'/recruit',
            templateUrl:'recruit.html'
        }).state('position.offline',{
            url:'/offline',
            templateUrl:'offline.html'
        }).state('position.examine',{
            url:'/examine',
            templateUrl:'examine.html'
        }).state('position.release',{
            url:'/release',
            templateUrl:'post-release.html'
        })
    });


