$(".select li").click(function () {
    $(this).parent().siblings("input").css("border","1px solid #e6e6e6");
    $(this).parent().siblings("p").hide();
});
var profileone= angular.module('profileone', []);
$("input:not([type|=button]):not('.border'):not('.id-select input'):not('.residence input')").focus(function () {
    $(this).css("border","1px solid #2d61e6");
});
$(".border").focus(function () {
    $(this).parent().css("border","1px solid #2d61e6");
})
//选项卡
$(".planning-type div").each(function () {
    var index=$(this).index();
    $(this).click(function () {
        $(".select-line div").removeClass("line-one").eq(index).addClass("line-one");
        $(".switch .change").hide().eq(index).show();
    })
});
profileone.controller('profileOneCtrl', function($scope,$http,$location,$window){
    $http({
        url:'/json/nation.json',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.nationCandidate = result.data;
    });
    //请求城市
    $scope.cityName=''
    $http({
        url:'/json/city.json',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.city = result.data;
    });
    $scope.type;
    if($(".intern").css("display")=="block"){
        $scope.type="实习";
    }else if($(".full").css("display")=="block"){
        $scope.type="全职"
    }
    $scope.info={
        image:'',
        name:'',
        sex:'',
        birthday:'',
        nationality:'',
        address:'',
        email:'',
        mobilePhone:'',
        education:'',
        school:'',
        major:'',
        entranceDate:'',
        graduationDate:'',
        expectIndustry:'',
        expectPosition:'',
        internTime:'',
        internPeriod:'',
        expectSalaryMin:'',
        expectSalaryMax:'',
        advantage:'',
        description:'',
        type:$scope.type
    };
    $http({
        url:'/register/getUserName',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        if(result.data.match("@")){
            $scope.info.email=result.data;
        }else {
            $scope.info.mobilePhone=result.data;
        }
    });
    $scope.country='';
    $scope.c_city='';
    $scope.pro=[];
    $scope.Tline=function () {
        $http({
            url:'/json/province.json',
            method:'GET'
        }).then(function (result) {  //正确请求成功时处理
            $scope.line = result.data;
            console.log($scope.line)
        });
    }
    $scope.lineClick=function (data,event) {
        $scope.country=data;
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+'-'+$scope.c_city;
    }
    $scope.Nline=function (data) {
        var arr=[];
        var rel=[];
        $http({
            url:'/json/province.json',
            method:'GET',
        }).then(function (result) {  //正确请求成功时处理
            angular.forEach(result.data, function(d,index,array){//data等价于array[index]
                if(d.province==data){
                    arr.push(d.citys)
                }
            });
            angular.forEach(arr, function(d,index,array){//data等价于array[index]
                rel=d;
            });
            $scope.con = rel;
        });
    }
    $scope.NameClick=function (data,event) {
        $scope.c_city=data;
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+'-'+$scope.c_city;
    };
    //国际
    $scope.f=function (data,event) {
        $scope.info.nationality=data;
        $(event.target).parent().parent().css("border","1px solid  #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    };
    $scope.show=function () {
        $(".select").toggle();
    };
    //获得焦点
    $scope.checkF=function (even) {
        $(even.target).css("border","1px solid #2d61e6");
    };
    $scope.checkT=function (date) {
        if(date==""|| date == null){
            $("#J-xl").css("border","1px solid #ff0000");
            $("#J-xl").siblings("p").show().text("不能为空");
            return false;
        }
        $("#J-xl").css("border","1px solid #e6e6e6");
        $("#J-xl").siblings("p").hide();
        return true;
    };
    $scope.checkN=function (date) {
        var val=$(date.target).val();
        if(val==""|| val == null){
            $(date.target).css("border","1px solid #ff0000");
            $(date.target).siblings("p").show().text("不能为空");
            return false;
        }
        $(date.target).css("border","1px solid #e6e6e6");
        $(date.target).siblings("p").hide();
        // if($(date.target).parent().attr("class").match("nation")=="nation"){
        //     $scope.info.nationality=$(date.target).val();
        // }
        return true;
    };
    $scope.checkPhone=function(mobile){
        if (mobile == "" || mobile== null) {
            $(".phone input").css("border","1px solid #ff0000");
            $(".phone p").show().text("不能为空");
            return false;
        } else {
            var myreg =/^1[34578]\d{9}$/;
            if (!myreg.test(mobile)) {
                $(".phone input").css("border","1px solid #ff0000");
                $(".phone p").show().text("格式错误");
                return false;
            }
        }
        $(".phone input").css("border","1px solid #e6e6e6");
        $(".phone p").hide();
        return true;
    };
    $scope.checkEmail = function(email) {
        if (email == "" || email == null) {
            $(".email input").css("border","1px solid #ff0000");
            $(".email p").show().text("不能为空");
            return false;
        } else {
            var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
            if (!myreg.test(email)) {
                $(".email input").css("border","1px solid #ff0000");
                $(".email p").show().text("格式错误");
                return false;
            }
        }
        $(".email input").css("border","1px solid #e6e6e6");
        $(".email p").hide();
        return true;
    };
    //下拉空验证
    $scope.checkS=function (event) {
        var val=$(event.target).val();
        if(val==""|| val == null){
            $(event.target).parent().css("border","1px solid #ff0000");
            $(event.target).parent().siblings("p").show().text("不能为空");
            return false;
        }
        $(event.target).parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().siblings("p").hide();
        return true;
    };
    $(".upload-result").click(function () {
        $(".file-btn .img").css("border","1px solid #e6e6e6");
    })
    $scope.onenextStep=function () {
        $scope.info.image=$("#result #img").attr("src");
        if($scope.info.image==undefined){
            $(".file-btn .img").css("border","1px solid #ff0000");
        }else if($scope.info.name==""){
            $(".name input").css("border","1px solid #ff0000");
            $(".name p").show().text("不能为空");
        }else if($scope.info.sex==""){
            $(".gender").show().text("必选")
        }else if(!$scope.checkT($scope.info.birthday)){
            $scope.checkT($scope.info.birthday)
        }else if($scope.info.nationality==""){
            $(".lity .id-select").css("border","1px solid #ff0000");
            $(".lity p").show().text("不能为空");
        }else if($scope.info.address==""){
            $(".city .residence-select").css("border","1px solid #ff0000");
            $(".city p").show().text("不能为空");
        }else if(!$scope.checkEmail($scope.info.email)){
            $scope.checkEmail($scope.info.email)
        }else if(!$scope.checkPhone($scope.info.mobilePhone)){
            $scope.checkPhone($scope.info.mobilePhone)
        }else {
            $(".form").hide();
            $(".sec2").show();
            $(".step1").removeClass("step-checked");
            $(".step2").addClass("step-checked");
        }
    }
//    2
    //入学日期
    $scope.checkR=function (event,date) {
        date=$(event.target).value;
        $scope.info.entranceDate=date;
        if($scope.info.entranceDate==""){
            $("#J-xl2").parent().css("border","1px solid #ff0000");
            $("#J-xl2").parent().siblings("p").show().text("不能为空");
            return false;
        }
        $("#J-xl2").parent().css("border","1px solid #e6e6e6");
        $("#J-xl2").parent().siblings("p").hide();
        return true;
    };
    //毕业日期
    $scope.checkD=function (event,date) {
        date=$(event.target).value;
        $scope.info.graduationDate=date
        if(date==""){
            $("#jx").parent().css("border","1px solid #ff0000");
            $("#jx").parent().siblings("p").show().text("不能为空");
            return false;
        }
        $("#jx").parent().css("border","1px solid #e6e6e6");
        $("#jx").parent().siblings("p").hide();
        return true;
    };
    $scope.twoprev=function () {
        $(".form").hide();
        $(".sec1").show();
        $(".step2").removeClass("step-checked");
        $(".step1").addClass("step-checked");
    }
    $scope.twonextStep=function () {
        $scope.info.entranceDate=$(".enter input").val();
        $scope.info.graduationDate=$(".end input").val();
        if($scope.info.education==""){
            $(".qualification .id-select").css("border","1px solid #ff0000");
            $(".qualification p").show().text("不能为空");
        }else if($scope.info.school==""){
            $(".school input").css("border","1px solid #ff0000");
            $(".school p").show().text("不能为空");
        }else if($scope.info.major==""){
            $(".major input").css("border","1px solid #ff0000");
            $(".major p").show().text("不能为空");
        }else if($scope.info.entranceDate==""){
            $('.enter').css("border","1px solid #ff0000");
            $(".enter").siblings("p").show().text("不能为空");
        }else if($scope.info.graduationDate==""){
            $('.end').css("border","1px solid #ff0000");
            $(".end").siblings("p").show().text("不能为空");
        }else {
            $(".form").hide();
            $(".sec3").show();
            $(".step2").removeClass("step-checked");
            $(".step3").addClass("step-checked");
        }
    }

//    3
    $scope.threenext=function () {
        if($scope.info.expectIndustry==""){
            $(".hope .id-select").css("border","1px solid #ff0000");
            $(".hope p").show().text("不能为空");
        }else if($scope.info.expectPosition==""){
            $(".occupation input").css("border","1px solid #ff0000");
            $(".occupation p").show().text("不能为空");
        }else if($scope.info.internTime==""){
            $(".time .id-select").css("border","1px solid #ff0000");
            $(".time p").show().text("不能为空");
        }else if($scope.info.internPeriod==""){
            $(".period .id-select").css("border","1px solid #ff0000");
            $(".period p").show().text("不能为空");
        }else if($scope.info.expectSalaryMin==""||$scope.info.expectSalaryMax==""){
            $(".salary input").css("border","1px solid #ff0000");
            $(".salary p").show().text("不能为空");
        }else {
            $(".form").hide();
            $(".sec4").show();
            $(".step3").removeClass("step-checked");
            $(".step4").addClass("step-checked");
        }
    }
    $scope.threeprev=function () {
        $(".form").hide();
        $(".sec2").show();
        $(".step3").removeClass("step-checked");
        $(".step2").addClass("step-checked");
    };
    $scope.threenextStep=function () {
        if($scope.info.expectIndustry==""){
            $(".vacotion .id-select").css("border","1px solid #ff0000");
            $(".vacotion p").show().text("不能为空");
        }else if($scope.info.expectPosition==""){
            $(".occupa input").css("border","1px solid #ff0000");
            $(".occupa p").show().text("不能为空");
        }else if($scope.info.expectSalaryMin==""||$scope.info.expectSalaryMax==""){
            $(".salary input").css("border","1px solid #ff0000");
            $(".salary p").show().text("不能为空");
        }else {
            $(".form").hide();
            $(".sec4").show();
            $(".step3").removeClass("step-checked");
            $(".step4").addClass("step-checked");
        }
    }
//4
    $scope.empty=function (data) {
        if($scope.info.advantage==""){
            $(data.target).css("border","1px solid #ff0000");
            $(data.target).siblings(".prompt").show().text("不能为空");
            return false;
        }
        $(data.target).css("border","1px solid #e6e6e6");
        $(data.target).siblings(".prompt").hide();
        return true;
    }
    $scope.areaEmpty=function (data) {
        if($scope.info.description==""){
            $(data.target).parent().css("border","1px solid #ff0000");
            $(data.target).parent().siblings(".prompt").show().text("不能为空");
            return false;
        }
        $(data.target).parent().css("border","1px solid #e6e6e6");
        $(data.target).parent().siblings(".prompt").hide();
        return true;
    }
    $scope.fourprev=function () {
        $(".form").hide();
        $(".sec3").show();
        $(".step4").removeClass("step-checked");
        $(".step3").addClass("step-checked");
    };
    $scope.submit=function () {
        if($scope.info.advantage==""){
            $(".info-four input").css("border","1px solid #ff0000");
            $(".info-four input").siblings(".prompt").show().text("不能为空");
        }else if($scope.info.description==""){
            $(".info-four textarea").parent().css("border","1px solid #ff0000");
            $(".info-four textarea").parent().siblings(".prompt").show().text("不能为空");
        }else {
            console.log($scope.info)
            $http({
                method: 'POST',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                url: '/register/saveUser',
                data:$.param($scope.info)
            }).then(function successCallback(response) {
                // 请求成功执行代码
                //跳转到公司首页
                $(".able").attr("disabled","true")
                window.location.href="/intern/intern-index";
            }, function errorCallback(response) {
                // 请求失败执行代码
                alert("保存失败");
                //刷新本页面重新保存
                // window.location.href("/index.html");
            });
        }

    }
});

