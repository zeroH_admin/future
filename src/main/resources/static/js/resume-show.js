var resume= angular.module('resume', []);
resume.controller('resumeCtrl', function($scope,$http,$window){
    $http({
        url:'../json/nation.json',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.nationCandidate = result.data;
    });
    $scope.min='';
    $scope.max='';
    $scope.info={
        id:'',
        image:'',
        name:'',
        sex:'',
        birthday:'',
        mobilePhone:'',
        email:'',
        nationality:'',
        address:'',
        workYear:'',
        education:'',
        advantage:'',
        description:'',
        cardType:'',
        idcard:'',
        qq:'',
        wechat:'',
        educationJson:'',
        workExperienceJson:'',
        schoolExperienceJson:'',
        skillJson:'',
        languageJson:'',
        certificateJson:'',
        expectWorkJson:''
    };

    $scope.open=function () {
        $(".open").toggleClass("dn")
    };
    $http({
        url:'/intern/getAccountResume',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.info = result.data;
        $scope.info.workYear='';
        $scope.educationList = JSON.parse(result.data.educationJson);
        $scope.workExperienceList = result.data.workExperienceJson==undefined?[]:JSON.parse(result.data.workExperienceJson);
        $scope.schoolExperienceList = result.data.schoolExperienceJson==undefined?[]:JSON.parse(result.data.schoolExperienceJson);
        $scope.skillList = result.data.skillJson==undefined?[]:JSON.parse(result.data.skillJson);
        $scope.languageList = result.data.languageJson==undefined?[]:JSON.parse(result.data.languageJson);
        $scope.certificateList = result.data.certificateJson==undefined?[]:JSON.parse(result.data.certificateJson);
        $scope.expectWorkList = JSON.parse(result.data.expectWorkJson);
        if($scope.expectWorkList[0].availableTime){
            $scope.full="元/日"
        }else {
            $scope.full="元/月"
        }
        $(".show").removeClass("dn")
        $scope.country=$scope.info.address.split("-")[0]
        $scope.c_city=$scope.info.address.split("-")[1];
        $scope.min=$scope.expectWorkList[0].expectedSalary.split("-")[0];
        $scope.max=$scope.expectWorkList[0].expectedSalary.split("-")[1];
    });
});
