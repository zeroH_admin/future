
$(".position-box").click(function () {
    $(".id-list").toggle();
    if($("id-list").css("display")=="block"){
        $('.position').css("border","1px solid #2d61e6");
        $(".position + .prompt").hide();
    }else {
        if($(".id-select input").val()==""){
            $('.position').css("border","1px solid #ff0000");
            $(".position + .prompt").show();
        }else {
            $('.position').css("border","1px solid #d9d9d9");
            $(".position + .prompt").hide();
        }
    }
});
$(".id-list li").click(function () {
    $(".position + .prompt").hide();
    $(".position").css("border","1px solid #d9d9d9");
    $(".position-select").val($(this).text())
});
$("input:not([type|=button]):not('.position input')").focus(function () {
    $(this).css("border","1px solid #2d61e6");
})