var app=angular.module('app',['ngRoute','ctrl']);
app.config(['$routeProvider',function($r) {
    $r.when('/', {
        templateUrl: 'tpls/home.html',
        controller: 'home'
    }).when('/edit/:id', {
        templateUrl: 'tpls/edit.html',
        controller: 'edit',
    }).when('/show/:id', {
        templateUrl: 'tpls/show.html',
        controller: 'show'
    })
    var ctrl = angular.module("ctrl", []);
    ctrl.controller("home", function ($scope, $http) {
        // $http({
        //     method:'get',
        //     url:'data/data.json',
        // }).then(function(res){
        //     $scope.list=res.data;
        // })
        $scope.list = data
    })
    ctrl.controller("show", function ($scope, $routeParams) {
        var l = data;
        for (var i = 0; i < l.length; i++) {
            if (l[i].id == $routeParams.id) {
                $scope.one = l[i];
                return;
            }
        }
        ;
    })
    ctrl.controller("edit", function ($scope, $location, $routeParams) {
        var l = data;
        for (var i = 0; i < l.length; i++) {
            if (l[i].id == $routeParams.id) {
                $scope.sub = function () {
                    $location.url('/');
                }
                $scope.one = l[i];
                return;
            }
        }
        ;
    })
    ctrl.controller("add", function ($scope, $location, $routeParams) {
        var id = data[data.length - 1].id + 1;
        $scope.t = '';
        $scope.c = '';
        $scope.list = data;
        $scope.submit = function () {
            var n = {
                id: id,
                title: $scope.t,
                content: $scope.c,
                time: new Date().getTime(),
                url: '#/show/' + id
            }
            $scope.list.push(n)
            $location.url(n.url.slice(1))
        }
    })
}])