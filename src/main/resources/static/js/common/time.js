//获取验证码
var timenum=60;
var returndata=false;
function getCode(text) {
    $.get(
        "/register/getMobileCode",
        {userName:text},
        function(data,state){
            returndata=data;
        }
    )
}
var timenum=60;
$(".intern-refresh-send").click(function () {
    $(this).parent().siblings(".register-phone").blur();
    if(phoneflag) {
        getCode($("#register-name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true);
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".intern-graphic-verification").show()
        }else {
            $(".intern-graphic-verification").hide()
        }
    }
});
$(".hr-refresh-send").click(function () {
    $(this).parent().siblings(".register-phone").blur();
    if(hrflag) {
        getCode($("#hr-name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true)
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".hr-graphic-verification").show()
        }else {
            $(".hr-graphic-verification").hide()
        }
    }
});
$(".reset-send").click(function () {
    $(this).parent().siblings(".register-phone").blur();
    if(phoneflag){
        getCode($("#name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true)
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".hr-graphic-verification").show()
        }else {
            $(".hr-graphic-verification").hide()
        }
    }
});

$(".contact-send").click(function () {
    $(this).parent().parent().siblings(".contact-phone").blur();
    if(contactflag){
        getCode($("#name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true)
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".graphic-verification").show()
        }else {
            $(".graphic-verification").hide()
        }
    }
});
function time(){
    timenum--;
    that.val("已发送("+timenum+"s)");
    if(timenum==0){
        clearInterval(changetime);
        that.val("重新发送");
        that.css({"backgroundColor":"transparent","border":"1px solid #2d61e6","color":"#2d61e6"});
        that.removeAttr("disabled");
        timenum=60;
    }

}