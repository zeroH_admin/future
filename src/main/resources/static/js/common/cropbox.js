var cropboxOnClick = function () {
  //头像上传弹层控件
  $(".tc").click(function () {
    $("#gray").show();
    $("#popup").show(); //查找ID为popup的DIV show()显示#gray
  });
  //点击关闭按钮
  $(".close_btn,.Btnsty_peyton_ok").click(function () {
    $("#gray").hide();
    $("#popup").hide(); //查找ID为popup的DIV hide()隐藏
  });
}
cropboxOnClick();
$(document).ready(function () {
  $(".top_nav").mousedown(function (e) {
    $(this).css("cursor", "move"); //改变鼠标指针的形状 
    var offset = $(this).offset(); //DIV在页面的位置 
    var x = e.pageX - offset.left; //获得鼠标指针离DIV元素左边界的距离 
    var y = e.pageY - offset.top; //获得鼠标指针离DIV元素上边界的距离 
    $(document).bind("mousemove", function (ev) { //绑定鼠标的移动事件，因为光标在DIV元素外面也要有效果，所以要用doucment的事件，而不用DIV元素的事件 
      $(".popup").stop(); //加上这个之后 
      var _x = ev.pageX - x; //获得X轴方向移动的值 
      var _y = ev.pageY - y; //获得Y轴方向移动的值 
      $(".popup").animate({
        left: _x + "px", 
        top : _y + "px"
      }, 10);
    });
  });
  $(document).mouseup(function () {
    $(".popup").css("cursor", "default");
    $(this).unbind("mousemove");
  });
})

//cropbox
var base64Data = null;
$(window).on('load', function() {
  var options = {
    thumbBox: '.thumbBox',
    spinner: '.spinner',
    imgSrc: ''
  }
  var cropper = $('.imageBox').cropbox(options);
  var img = "";
  $('#upload-file').on('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
      options.imgSrc = e.target.result;
      cropper = $('.imageBox').cropbox(options);
      getImg();
    }
    reader.readAsDataURL(this.files[0]);
  })

  function getImg() {
    img = cropper.getAvatar();
      base64Data = img.replace(/^data:image\/png;base64,/, "");
      $('.cropped').html('');
      $('.cropped').append('<img class="head-portrait tc" src="' + img + '" align="absmiddle" style="width:160px;margin-top:4px;border-radius:160px;box-shadow:0px 0px 12px #7E7E7E;">');
      cropboxOnClick();
    }
    $(".Btnsty_peyton_ok").on("mouseup", function () {
      $(".head-portrait").hide();
      getImg();
    });
    $('#btnZoomIn').on('click', function () {
      cropper.zoomIn();
    })
    $('#btnZoomOut').on('click', function () {
      cropper.zoomOut();
    })

});