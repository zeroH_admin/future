
//intern 部分验证
//账号验证
var hrfindtext=$(".hr-find p");
var hrfind=$(".hr-find input");

var phoneflag=false;
hrfind.blur(function () {
    if(hrfind.val()==""){
        hrfindtext.show().text("用户名不能为空");
        hrfind.css("border","1px solid #ff0000");
    }else if(!(hrfind.val().match(/^1[34578]\d{9}$/)||hrfind.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))) {
        hrfindtext.show().text("用户名格式错误");
        hrfind.css("border","1px solid #ff0000");
    }else{
        $.get(
            "/register/checkUserName",
            {userName:hrfind.val()},
            function(data,state){
                if(data==true){
                    hrfind.css("border","1px solid #ff0000");
                    hrfindtext.show().text("用户名不存在");
                    $(".reset-password").attr("disabled",true);
                    $(".reset-send").attr("disabled",true);
                }else {
                    hrfind.css("border","1px solid #e6e6e6");
                    $(".reset-send").removeAttr("disabled");
                    phoneflag=true;
                    hrfindtext.hide();
                }
                //这里显示返回的状态
            }
        )
        hrfindtext.hide();

    }
});
//图形

//密码格式验证
var internpwdtext=$(".register-pwd p")
var internpwd=$(".register-pwd input")
internpwd.blur(function () {
    if(internpwd.val()==""||!internpwd.val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/)){
        internpwdtext.show().text("密码格式错误，需满足最少8位且数字与字母结合");
        internpwd.css("border","1px solid #ff0000");
    }else {
        internpwdtext.hide();
    }
})
$("#phone-graphic").blur(function () {
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).siblings("p").show().text("不能为空")
    }else {
        $(this).css("border","1px solid #e6e6e6");
        $(this).siblings("p").hide();
    }
})
//重复密码
$(".repeat-pwd input").blur(function () {
    if($(this).val()==$(".register-pwd input").val()){
        $(this).css("border","1px solid #e6e6e6");
        $(this).siblings("p").hide()
    }else {
        $(this).css("border","1px solid #ff0000");
        $(this).siblings("p").show().text("两次密码不一致")
    }
})
$(".register-button").click(function () {
    $(this).css("background","#0d3fbe");
    var inputbox=$(this).parent().find($("input:not([type|=button]):not([type|=hidden])"));
    if($(".graphic-verification").attr("hidden")!="hidden"){
        $(this).parent().find($("input:not([type|=button]):not([type|=hidden])")).not($(".graphic-verification input")).each(function () {
            if($(this).val()==""){
                $(".register-button").attr("disabled","true")
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                return true;
            }else {
                $(".register-button").click(function () {
                    $(".resetForm").submit();
                })
            }
        })
    }else {
        inputbox.not($(this).parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                $(this).parent().siblings(".prompt").show().text("不能为空");
                return false;
            }else {
                $(".register-button").click(function () {
                    $(".resetForm").submit();
                })
            }
        })
    }
});

//同意协议
$(".checked").click(function () {
    $(".img-checked").toggle()
})
//弹窗
$(".register-read-yes a").click(function(){
    $(".pop-agreement").show();
});
//所有弹窗关闭效果
$(".closeBtn").click(function(){
    $(this).parent(".close-trigger").hide();
});
// 刷新图片
function changeImg() {
    var imgSrc = $(".graphic");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", changeUrl(src));
}
//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function changeUrl(url) {
    var timestamp = (new Date()).valueOf();
    var index = url.indexOf("?",url);
    if (index > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//图形验证
$("#graphic").blur(function () {
    var imgCode = $("#graphic").val();
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(imgCode!==""){
        $.get(
            "/register/checkCode",
            {code:imgCode},
            function(data,state){
                if(data==true){
                    $("#graphic").css("border","1px solid #e6e6e6");
                    $("#graphic").parent().siblings("p").hide();
                }else {
                    $("#graphic").css("border","1px solid #ff0000");
                    $("#graphic").parent().siblings("p").show().text("验证码输入错误");
                    $(".reset-password").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )
    }else {
        $("#graphic").css("border","1px solid #ff0000");
        $("#graphic").parent().siblings("p").show().text("验证码不能为空");
    }
});
//手机验证码
$("#phone-graphic").blur(function () {
    $.get(
        "/register/checkMobileCode",
        {
            userName:$("#name").val(),
            code:$("#phone-graphic").val()
        },
        function(data,state){
            if(!data){
                $("#phone-graphic").css("border","1px solid #ff0000");
                $("#phone-graphic").siblings("p").show().text("验证码输入错误");
            }else {
                $("#phone-graphic").css("border","1px solid #e6e6e6");
                $("#phone-graphic").siblings("p").hide();
            }
        }
    )
})






