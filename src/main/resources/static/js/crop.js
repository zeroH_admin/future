
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
                // $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 150,
            height: 150,
            type: 'square'
        },
        boundary: {
            width: 250,
            height: 250
        }
    });

    $('#upload').on('change', function (e) {
        if(e.target.files[0].size>2097152){
            alert("图片大于2M，请重新上传");
            return
        }else{
            $(".crop").show();
            readFile(this);
        }

    });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', 'canvas').then(function (resp) {
            popupResult({
                src: resp
            });
        });
        $(".crop").css("display","none");
    });

    function popupResult(result) {
        var html;
        if (result.html) {
            html = result.html;
        }
        var fileSize;
        var str = result.src;
        if(str.indexOf('=')>0)
        {
            var indexOf=str.indexOf('=');
            str=str.substring(0,indexOf);//把末尾的’=‘号去掉
        }

        fileSize=parseInt(str.length-(str.length/8)*2);
        //判断压缩后的文件大于2M的
        if(fileSize>2097152){
            alert("图片大于2M，请重新上传");
            return;
        }else{
            $.ajax({
                type:"GET",
                url:"/ih/folderFile/getToken",
                success: function(data){
                    putb64(result.src,data)
                }
            })
        }

    }

    /*picBase是base64图片带头部的完整编码*/
    function putb64(picBase,token){
        /*picUrl用来存储返回来的url*/
        var picUrl;
        /*把头部的data:image/png;base64,去掉。（注意：base64后面的逗号也去掉）*/
        picBase=picBase.substring(22);
        /*通过base64编码字符流计算文件流大小函数*/
        function fileSize(str){
            var fileSize;
            if(str.indexOf('=')>0)
            {
                var indexOf=str.indexOf('=');
                str=str.substring(0,indexOf);//把末尾的’=‘号去掉
            }

            fileSize=parseInt(str.length-(str.length/8)*2);
            return fileSize;
        }

        /*把字符串转换成json*/
        function strToJson(str)
        {
            var json = eval('(' + str + ')');
            return json;
        }

        var url = "http://upload.qiniu.com/putb64/-1";
        var xhr = new XMLHttpRequest();
        var xhrget = new XMLHttpRequest();
        xhr.onreadystatechange=function(){
            if (xhr.readyState==4){
                var keyText=xhr.responseText;
                /*返回的key是字符串，需要装换成json*/
                keyText=strToJson(keyText);

                picUrl="http://opo2ue3fr.bkt.clouddn.com/"+keyText.key;
                html = '<img id="img" src="' + picUrl + '" />';
                $("#result").html(html);
            }
        }
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/octet-stream");
        xhr.setRequestHeader("Authorization", token);
        xhr.send(picBase);

    }

