// $(".job ,.cycle ,.opportunity ,.track-line,.track-name").click(function () {
//     $(this).children(".down").siblings(".select").toggle();
//     if($(this).children(".down").siblings(".select").css("display")=="block"){
//         $(this).children(".down").siblings("input").focus();
//     }else {
//         $(this).children(".down").siblings("input").blur();
//     }
//
// });
$(".job ,.cycle ,.opportunity ,.track-line,.track-name").click(function () {
    $(this).children(".select").toggle();
    if($(this).children(".select").css("display")=="block"){
        $(this).children("input").focus();
    }else {
        $(this).children("input").blur();
    }

});

function button(eve) {
    eve.click(function () {
        eve.css("background","#0d3fbe");
        var inputbox=eve.parent().find($("input:not([type|=button]):not([type|=hidden])"));
        inputbox.not(eve.parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings("p").show().text("不能为空");
                $(this).parent().siblings("p").show().text("不能为空");
                return false;
            }else {

            }
        })
    });
}
button($(".button"));
//勾选
$(".checked").click(function () {
    $(".img-checked").toggle();
    if($(".img-checked").css("display")=="none"){
        $(".button").attr("disabled",true);
    }else {
        $(".button").removeAttr("disabled");
    }
});
var release=angular.module("release",[]);
release.controller('releaseCtrl',function ($scope,$http,$window) {
    $scope.salary_min='';
    $scope.day_min='';
    $scope.time_min='';
    $scope.salary_max='';
    $scope.day_max='';
    $scope.time_max='';
    $scope.trackLine='';
    $scope.trackName='';

    //请求数据
    $http({
        url:'/hr/jobData?id='+id,
        method:'GET'
    }).then(function (result) {
        $scope.job = result.data;
        if (result.data.property=="实习"){
            $scope.day_min=Number(result.data.internDaySalary.split("-")[0]);
            $scope.day_max=Number(result.data.internDaySalary.split("-")[1]);
            $scope.time_min=Number(result.data.workTimes.split("-")[0]);
            $scope.time_max=Number(result.data.workTimes.split("-")[1]);
        }else if(result.data.property=="全职"){
            $scope.salary_min=Number(result.data.salary.split("-")[0]);
            $scope.salary_max=Number(result.data.salary.split("-")[1]);
        }
        $scope.trackLine=result.data.nearSubway.split("-")[0];
        $scope.trackName=result.data.nearSubway.split("-")[1];
    });
    $scope.Tline=function () {
        $http({
            url:'/json/metroLine.json',
            method:'GET'
        }).then(function (result) {  //正确请求成功时处理
            $scope.line = result.data;
        });
    }
    $scope.lineClick=function (data,event) {
        $scope.trackLine=data;
        $scope.job.nearSubway=$scope.trackLine+"-"+$scope.trackName;
        $(event.target).parent().siblings('input').css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    }
    $scope.Nline=function (data) {
        var arr=[];
        var rel=[];
        $http({
            url:'/json/metroLine.json',
            method:'GET',
        }).then(function (result) {  //正确请求成功时处理
            angular.forEach(result.data, function(d,index,array){//data等价于array[index]
                console.log(d.line==data)
                if(d.line==data){
                    arr.push(d.station)
                }
            });
            angular.forEach(arr, function(d,index,array){//data等价于array[index]
                rel=d;
            });
            $scope.con = rel;
        });
    }
    $scope.NameClick=function (data,event) {
        $scope.trackName=data;
        $scope.job.nearSubway=$scope.trackLine+"-"+$scope.trackName;
        $(event.target).parent().siblings('input').css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    }
    $scope.tel=function (event) {
        $http.get(
            "/register/checkUserName",
            {userName:$scope.job.mobilePhone},
            function(data,state){
                if(data==true){
                    $(event.target).css("border","1px solid #ff0000");
                    $(event.target).siblings("p").show().text("用户名不存在");
                }else {
                    $(event.target).css("border","1px solid #e6e6e6");
                    $(event.target).siblings("p").hide();
                }
                //这里显示返回的状态
            }
        )
    }

    $scope.submit=function () {
        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/hr/saveJob',
            data:$.param($scope.job)
        }).then(function successCallback(response) {
            // 请求成功执行代码
            alert("保存成功");
            var url = "/hr/job-list?status="+response.data.code+"";
            window.location.href=url;
            //跳转到公司首页
        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存
        });
    }
    var watch = $scope.$watch('job.property',function(newValue,oldValue, scope){
        if (newValue=="实习"){
            $(".internPeriod,.workTimes,.internDaySalary,.option").show();
            $(".salary").hide();
        }else if(newValue=="全职"){
            $(".salary").show();
            $(".internPeriod,.workTimes,.internDaySalary,.option").hide();
        }
    });

})

