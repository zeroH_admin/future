//获取验证码
var timenum=60;
var returndata=false;
function getCode(text) {
    $.get(
        "/register/getMobileCode",
        {userName:text},
        function(data,state){
            returndata=data;
        }
    )
}
var timenum=60;
$("#register-send").click(function () {
    $(this).parent().siblings(".register-phone").blur();
    if(userflag) {
        getCode($("#register-name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true);
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".register-graph").show()
        }else {
            $(".register-graph").hide()
        }
    }
});
$("#reset-send").click(function () {
    $(this).parent().siblings(".register-phone").blur();
    if(phoneflag){
        getCode($("#register-name").val())
        $(this).val("已发送(60s)");
        $(this).css({"backgroundColor": "#999", "border": "1px solid transparent", "color": "#fff"});
        $(this).attr("disabled", true)
        that = $(this);
        changetime = setInterval(function () {
            time();
        }, 1000);
        if(returndata){
            $(".register-graph").show()
        }else {
            $(".register-graph").hide()
        }
    }
});
function time(){
    timenum--;
    that.val("已发送("+timenum+"s)");
    if(timenum==0){
        clearInterval(changetime);
        that.val("重新发送");
        that.css({"backgroundColor":"transparent","border":"1px solid #2d61e6","color":"#2d61e6"});
        that.removeAttr("disabled");
        timenum=60;
    }

}