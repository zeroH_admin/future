

$(".register-read-yes a").click(function(){
    $(".pop-agreement").show();
    $(".mask").show();
    $(".close-trigger").show()
});
//所有弹窗关闭效果
$(".closeBtn").click(function(){
    $(this).parent(".close-trigger").hide();
    $(".mask").hide();
});
//同意协议
$(".checked").click(function () {
    $(".img-checked").toggle();
    if($(".img-checked").css("display")=="none"){
        $(".register-button").attr("disabled",true);
    }else {
        $(".register-button").removeAttr("disabled");
    }
});
//电话验证
var hrfindtext=$(".register-phone p");
var hrfind=$(".register-phone input");

var phoneflag=false;
hrfind.blur(function () {
    if(hrfind.val()==""){
        hrfindtext.show().text("用户名不能为空");
        hrfind.css("border","1px solid #ff0000");
    }else if(!(hrfind.val().match(/^1[34578]\d{9}$/)||hrfind.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))) {
        hrfindtext.show().text("用户名格式错误");
        hrfind.css("border","1px solid #ff0000");
    }else{
        $.get(
            "/register/checkUserName",
            {userName:hrfind.val()},
            function(data,state){
                if(data==true){
                    hrfind.css("border","1px solid #ff0000");
                    hrfindtext.show().text("用户名不存在");
                    $(".reset-send").attr("disabled",true);
                    // $(".reset-password").attr("disabled",true);
                }else {
                    hrfind.css("border","1px solid #e6e6e6");
                    hrfindtext.hide();
                    $(".reset-send").removeAttr("disabled");
                    phoneflag=true;
                    hrfindtext.hide();
                }
                //这里显示返回的状态
            }
        )
        hrfindtext.hide();

    }
});

//密码格式验证
var internpwdtext=$(".register-pwd p")
var internpwd=$(".register-pwd input")
internpwd.blur(function () {
    password(internpwd,internpwdtext)
})
//重复密码验证
var pwdfalg;
$(".repeat-register-pwd input").blur(function () {
    if(internpwd.val()!=$(".repeat-register-pwd input").val()){
        $(".repeat-register-pwd p").show();
        $(".repeat-register-pwd input").css("border","1px solid #ff0000");
        pwdfalg=false;
    }else {
        $(".repeat-register-pwd p").hide();
        pwdfalg=true;
    }
});

// 刷新图片
function changeImg() {
    var imgSrc = $(".register-img img");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", changeUrl(src));
}
//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function changeUrl(url) {
    var timestamp = (new Date()).valueOf();
    var index = url.indexOf("?",url);
    if (index > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
var imgfalg;
//图形验证
$("#graphic").blur(function () {
    var imgCode = $("#graphic").val();
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(imgCode!==""){
        $.get(
            "/register/checkCode",
            {code:imgCode},
            function(data,state){
                if(data==true){
                    $("#graphic").css("border","1px solid #e6e6e6");
                    $("#graphic").siblings("p").hide();
                    imgfalg=true;
                }else {
                    $("#graphic").css("border","1px solid #ff0000");
                    $("#graphic").siblings("p").show().text("验证码输入错误");
                    imgfalg=false;
                }
                //这里显示返回的状态
            }
        )
    }else {
        $("#graphic").css("border","1px solid #ff0000");
        $("#graphic").siblings("p").show().text("验证码不能为空");
    }
});
//手机验证码
var telfalg;
$("#phone-graphic").blur(function () {
    if($("#phone-graphic").val()!=''){
        $.get(
            "/register/checkMobileCode",
            {
                userName:$("#register-name").val(),
                code:$("#phone-graphic").val()
            },
            function(data,state){
                if(!data){
                    $("#phone-graphic").css("border","1px solid #ff0000");
                    $("#phone-graphic").siblings("p").show().text("验证码输入错误");
                    telfalg=false;
                }else {
                    $("#phone-graphic").css("border","1px solid #e6e6e6");
                    $("#phone-graphic").siblings("p").hide();
                    telfalg=true;
                }
            }
        )
    }

})

$("#resetSubmit").click(function () {
    $(this).css("background","#0d3fbe");
    if($(".register-graph").attr("hidden")=="hidden"){
        if($("#register-name").val()==""||(!$("#register-name").val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))) {
            $("#register-name").siblings("p").show().text("用户名格式错误");
            $("#register-name").css("border", "1px solid #ff0000");
        }else if($("#phone-graphic").val()==""||!telfalg){
            $("#phone-graphic").siblings("p").show().text("验证码输入错误");
            $("#phone-graphic").css("border","1px solid #ff0000");
        }else if($("#pwd").val()==""||(!$("#pwd").val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/))){
            $("#pwd").siblings("p").show().text("密码格式错误");
            $("#pwd").css("border","1px solid #ff0000");
        }else if(!pwdfalg){
            $("#resetpwd").siblings("p").show().text("两次密码不一致");
            $("#resetpwd").css("border","1px solid #ff0000");
        }else {
            $("#resetDataForm").submit();
        }
    }else {
        if($("#register-name").val()==""||(!$("#register-name").val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))){
            $("#register-name").siblings("p").show().text("用户名格式错误");
            $("#register-name").css("border","1px solid #ff0000");
        }else if($("#graphic").val()==""||!imgfalg){
            $("#graphic").siblings("p").show().text("验证码输入错误");
            $("#graphic").css("border","1px solid #ff0000");
        }else if($("#phone-graphic").val()==""||!telfalg){
            $("#phone-graphic").siblings("p").show().text("验证码输入错误");
            $("#phone-graphic").css("border","1px solid #ff0000");
        }else if($("#pwd").val()==""||(!$("#pwd").val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/))){
            $("#pwd").siblings("p").show().text("密码格式错误");
            $("#pwd").css("border","1px solid #ff0000");
        }else if(!pwdfalg){
            $("#resetpwd").siblings("p").show().text("两次密码不一致");
            $("#resetpwd").css("border","1px solid #ff0000");
        }else {
            $("#resetDataForm").submit();
        }
    }
});
