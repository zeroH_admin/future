$(".information input").focus(function () {
    $(this).css("border","1px solid #2d61e6")
}).blur(function () {
    // if($(this).val()==""){
    //     $(this).css("border","1px solid #ff0000");
    //     $(this).siblings('p').show().text("不能为空")
    // }else {
        $(this).css("border","1px solid #d9d9d9");
        $(this).siblings('p').hide()
    // }
})
$("textarea").focus(function () {
    $(this).css("border","1px solid #2d61e6")
}).blur(function () {
    // if($(this).val()==""){
    //     $(this).css("border","1px solid #ff0000");
    //     $(this).siblings('p').show().text("不能为空")
    // }else {
    $(this).css("border","1px solid #d9d9d9");
    $(this).siblings('p').hide()
    // }
})
$("#mobilePhone").blur(function () {
    if($("#mobilePhone").val().match(/^1[34578]\d{9}$/)){
        $(this).css("border","1px solid #d9d9d9");
        $(this).siblings('p').hide()
    }else {
        $("#mobilePhone").css("border","1px solid #ff0000");
        $("#mobilePhone").siblings('p').show().text("请输入正确的手机号");
    }
})
$("#email").blur(function () {
    if($("#email").val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)){
        $(this).css("border","1px solid #d9d9d9");
        $(this).siblings('p').hide()
    }else {
        $("#email").css("border","1px solid #ff0000");
        $("#email").siblings('p').show().text("请输入正确的邮箱");
    }
})
$("#nextone").click(function () {
    $("#birth").val(formateDate($("#birth").val()))
    if($("#view span").text()!=""){
        $(".img").css("border","1px solid #ff0000");
    }else if($("#name").val()==""){
        $("#name").css("border","1px solid #ff0000");
        $("#name").siblings('p').show().text("不能为空")
    }else if($("#birth").val()==""){
        $("#birth").css("border","1px solid #ff0000");
        $("#birth").siblings('p').show().text("不能为空")
    }else if($("#demo2").val()==""){
        $("#demo2").css("border","1px solid #ff0000");
        $("#demo2").siblings('p').show().text("不能为空")
    }else if($("#address").val()==""||!cityfalg){
        $("#address").css("border","1px solid #ff0000");
        $("#address").siblings('p').show().text("居住地输入错误")
    }else if($("#mobilePhone").val()==""||(!$("#mobilePhone").val().match(/^1[34578]\d{9}$/))){
        $("#mobilePhone").css("border","1px solid #ff0000");
        $("#mobilePhone").siblings('p').show().text("请输入正确的手机号");
    }else if($("#email").val()==""||(!$("#email").val().match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))){
        $("#email").css("border","1px solid #ff0000");
        $("#email").siblings('p').show().text("请输入正确的邮箱");
    }else {
        $(".step1").hide();
        $(".step2").show()
    }
})
$("#prevtwo").click(function () {
    $(".step2").hide();
    $(".step1").show();
})
$("#nexttwo").click(function () {
    if($("#edu").val()==""){
        $("#edu").css("border","1px solid #ff0000");
        $("#edu").siblings('p').show().text("不能为空")
    }else if($("#school").val()==""){
        $("#school").css("border","1px solid #ff0000");
        $("#school").siblings('p').show().text("不能为空")
    }else if($("#major").val()==""){
        $("#major").css("border","1px solid #ff0000");
        $("#major").siblings('p').show().text("不能为空")
    }else if($("#start").val()==""){
        $("#start").css("border","1px solid #ff0000");
        $("#start").siblings('p').show().text("不能为空")
    }else if($("#end").val()==""){
        $("#end").css("border","1px solid #ff0000");
        $("#end").siblings('p').show().text("不能为空")
    }else {
        $(".step2").hide();
        $(".step3").show();
    }
})
$("#full-prevthree").click(function () {
    $(".step3").hide();
    $(".step2").show();
})
$("#full-nextthree").click(function () {
    if($("#full-industry").val()==""){
        $("#full-industry").css("border","1px solid #ff0000");
        $("#full-industry").siblings('p').show().text("不能为空")
    }else if($("#full-occupation").val()==""){
        $("#full-occupation").css("border","1px solid #ff0000");
        $("#full-occupation").siblings('p').show().text("不能为空")
    }else if($("#full-min").val()==""){
        $("#full-min").css("border","1px solid #ff0000");
        $("#full-min").siblings('p').show().text("不能为空")
    }else if($("#full-max").val()==""){
        $("#full-max").css("border","1px solid #ff0000");
        $("#full-max").siblings('p').show().text("不能为空")
    }else {
        $(".step3").hide();
        $(".step4").show()
    }
})
$("#prevthree").click(function () {
    $(".step3").hide();
    $(".step2").show();
})
$("#nextthree").click(function () {
    if($("#industry").val()==""){
        $("#industry").css("border","1px solid #ff0000");
        $("#industry").siblings('p').show().text("不能为空")
    }else if($("#occupation").val()==""){
        $("#occupation").css("border","1px solid #ff0000");
        $("#occupation").siblings('p').show().text("不能为空")
    }else if($("#time").val()==""){
        $("#time").css("border","1px solid #ff0000");
        $("#time").siblings('p').show().text("不能为空")
    }else if($("#period").val()==""){
        $("#period").css("border","1px solid #ff0000");
        $("#period").siblings('p').show().text("不能为空")
    }else if($("#min").val()==""){
        $("#min").css("border","1px solid #ff0000");
        $("#min").siblings('p').show().text("不能为空")
    }else if($("#max").val()==""){
        $("#max").css("border","1px solid #ff0000");
        $("#max").siblings('p').show().text("不能为空")
    }else {
        $(".step3").hide();
        $(".step4").show()
    }
})
$("#prevfour").click(function () {
    $(".step4").hide();
    $(".step3").show();
})
$("#nextfour").click(function () {
    if($("#advantage").val()==""){
        $("#advantage").css("border","1px solid #ff0000");
        $("#advantage").siblings('p').show().text("不能为空")
    }else if($("textarea").val()==""){
        $("textarea").css("border","1px solid #ff0000");
        $("textarea").siblings('p').show().text("不能为空")
    }else {
        var type = $("#typeValue").val();
        var salar;
        var exportPosition;
        var industry;
        if(type=='实习'){
            var min = $("#min").val();
            var max = $("#max").val();
            salar = min+'-'+max;
            exportPosition = $("#occupation").val();
            industry = $("#internIndustry").val();

        }else{
            var min = $("#full-min").val();
            var max = $("#full-max").val();
            salar = min+'-'+max;
            exportPosition = $("#full-occupation").val();
            industry = $("#full-industry").val();
        }
        $("#expectPosition").val(exportPosition)
        $("#industry").val(industry)
        $("#birth").val(formateDate($("#birth").val()))
        $("#start").val(formateDate($("#start").val()))
        $("#end").val(formateDate($("#end").val()))
        $("#expectSalary").val(salar);
        $("#userForm").submit();
    }
})
$("#advantage").on("input",function () {
    $(".input-num").text($(this).val().length)
    if($(this).val().length==30){
        $(".input-red,.input-num").css("color","#ff0000")
    }else {
        $(".input-red,.input-num").css("color","#333")
    }
})
$("textarea").on("input",function () {
    $(".text-num").text($(this).val().length)
    if($(this).val().length==500){
        $(".text-red,.text-num").css("color","#ff0000")
    }else {
        $(".text-red,.text-num").css("color","#333")
    }
})
//选项卡
$(".planning-type div").each(function () {
    var index=$(this).index();
    $(this).click(function () {
        $(".select-line div").removeClass("line-one").eq(index).addClass("line-one");
        $(".switch .change").hide().eq(index).show();
        $("#typeValue").val($(this).text())
    })
});
$(".labelauty").click(function () {
    var sex = $(this).data("sex");
    console.log(sex)
    $("#sexValue").val(sex)
});
$("#address").blur(function () {
    console.log($(this).val())
});

function formateDate(data) {
    var d1=data.split("-")[0];
    var d2=data.split("-")[1];
    var d3=data.split("-")[2];
    if(d2<10 && d2.indexOf("0")==-1){
        d2 = "0"+d2;
    }
    if(d3<10 && d3.indexOf("0")==-1){
        d3 = "0"+d3;
    }
    console.log(d1+"-"+d2+"-"+d3)
    return d1+"-"+d2+"-"+d3

}