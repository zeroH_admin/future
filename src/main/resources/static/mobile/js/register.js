
$(".register-read-yes a").click(function(){
    $(".pop-agreement").show();
    $(".mask").show();
    $(".close-trigger").show()
});
//所有弹窗关闭效果
$(".closeBtn").click(function(){
    $(this).parent(".close-trigger").hide();
    $(".mask").hide();
});
//同意协议
$(".checked").click(function () {
    $(".img-checked").toggle();
    if($(".img-checked").css("display")=="none"){
        $(".register-button").attr("disabled",true);
    }else {
        $(".register-button").removeAttr("disabled");
    }
});
//电话验证
var userflag=false;
var internphonetext=$(".register-phone p");
var internphone=$(".register-phone input");
internphone.blur(function () {
    if((!internphone.val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))) {
        internphonetext.show().text("用户名格式错误");
        internphone.css("border","1px solid #ff0000");
    }else{
        internphone.css("border","1px solid #e6e6e6");
        internphonetext.hide();
        $.get(
            "/register/checkUserName",
            {userName:internphone.val()},
            function(data,state){
                if(data==true){
                    internphone.css("border","1px solid #e6e6e6");
                    internphonetext.hide();
                    $(".intern-refresh-send").removeAttr("disabled");
                    userflag=true;
                }else {
                    internphone.css("border","1px solid #ff0000");
                    internphonetext.show().text("用户名已存在");
                    $("#register-save").attr("disabled",true);
                    $(".intern-refresh-send").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )

    }
});

//密码格式验证
var internpwdtext=$(".register-pwd p")
var internpwd=$(".register-pwd input")
internpwd.blur(function () {
    password(internpwd,internpwdtext);
})
// 刷新图片
function changeImg() {
    var imgSrc = $(".graphic");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", changeUrl(src));
}
//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function changeUrl(url) {
    var timestamp = (new Date()).valueOf();
    var index = url.indexOf("?",url);
    if (index > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//图形验证
var imgfalg;
$("#verificationImg").blur(function () {
    var imgCode = $("#verificationImg").val();
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(imgCode!==""){
        $.get(
            "/register/checkCode",
            {code:imgCode},
            function(data,state){
                if(data==true){
                    $("#verificationImg").css("border","1px solid #e6e6e6");
                    $("#verificationImg").siblings("p").hide();
                    $("#register-button").removeAttr("disabled");
                    imgfalg=true;
                }else {
                    $("#verificationImg").css("border","1px solid #ff0000");
                    $("#verificationImg").siblings("p").show().text("验证码输入错误");
                    $("#register-button").attr("disabled",true);
                    imgfalg=false;
                }
                //这里显示返回的状态
            }
        )
    }
});
//手机验证码
var telfalg;
$("#intern-refresh-send").blur(function () {
    $.get(
        "/register/checkMobileCode",
        {
            userName:$("#register-name").val(),
            code:$("#intern-refresh-send").val()
        },
        function(data,state){
            if(!data){
                $("#intern-refresh-send").css("border","1px solid #ff0000");
                $("#intern-refresh-send").siblings("p").show().text("验证码输入错误");
                telfalg=false;
            }else {
                $("#intern-refresh-send").css("border","1px solid #e6e6e6");
                $("#intern-refresh-send").siblings("p").hide();
                telfalg=true;
            }
        }
    )
})
$(".button").click(function () {
    $(this).css("background","#0d3fbe");
    if($(".register-graph").attr("hidden")=="hidden"){
        if($("#register-name").val()==""||(!$("#register-name").val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))) {
            $("#register-name").siblings("p").show().text("用户名格式错误");
            $("#register-name").css("border", "1px solid #ff0000");
        }else if($("#register-send").val()==""||!telfalg){
            $("#register-send").siblings("p").show().text("验证码输入错误");
            $("#register-send").css("border","1px solid #ff0000");
        }else if($("#pwd").val()==""||(!$("#pwd").val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/))){
            $("#pwd").siblings("p").show().text("密码格式错误");
            $("#pwd").css("border","1px solid #ff0000");
        }else {
            $(".find-job").submit();
        }
    }else {
        if($("#register-name").val()==""||(!$("#register-name").val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))){
            $("#register-name").siblings("p").show().text("用户名格式错误");
            $("#register-name").css("border","1px solid #ff0000");
        }else if($("#verificationImg").val()==""||!imgfalg){
            $("#register-name").siblings("p").show().text("验证码输入错误");
            $("#register-name").css("border","1px solid #ff0000");
        }else if($("#register-send").val()==""||!telfalg){
            $("#register-send").siblings("p").show().text("验证码输入错误");
            $("#register-send").css("border","1px solid #ff0000");
        }else if($("#pwd").val()==""||(!$("#pwd").val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/))){
            $("#pwd").siblings("p").show().text("密码格式错误");
            $("#pwd").css("border","1px solid #ff0000");
        }else {
            $(".find-job").submit();
        }
    }
});