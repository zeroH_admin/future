$("input:not([type|=button])").focus(function () {
    $(this).css("border","1px solid #2d61e6");
}).blur(function () {
    $(this).css("border","1px solid #e6e6e6");
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).siblings("p").show();
    }
});
$(".button").click(function () {
    $(this).css("background","#0d3fbe");
    if($(".register-graph").attr("hidden")=="hidden"){
        $("input:not([type|=button]):not([type|=hidden])").not($("input:not([type|=button]):not([type|=hidden])").eq(1)).each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings("p").show();
                return false;
            }
        })
    }else {
        $("input:not([type|=button]):not([type|=hidden])").each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings("p").show();
                return false;
            }
        })
    }
});
// function username() {
//     if(!tel.val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([.a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)) {
//         text.show().text("用户名格式错误");
//         tel.css("border","1px solid #ff0000");
//     }else{
//         text.hide();
//         userflag=true;
//     }
// }
function phone(tel,text) {
    if(!tel.val().match(/^1[34578]\d{9}$/)) {
        text.show().text("用户名格式错误");
        tel.css("border","1px solid #ff0000");
    }else{
        text.hide();
        phoneflag=true;
    }
}
function password(pwd,pwdtext) {
    if(pwd.val()==""||!pwd.val().match(/^(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9]{8,}$/)){
        pwdtext.show();
        pwd.css("border","1px solid #ff0000");
    }else {
        pwdtext.hide();
    }
}
