
    var $uploadCrop;

    function readFile(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('.upload-demo').addClass('ready');
                // $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            alert("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 150,
            height: 150,
            type: 'square'
        },
        boundary: {
            width: 250,
            height: 250
        }
    });

    $('#upload').on('change', function () {
        $(".crop").show();
        readFile(this);
    });
    $('.upload-result').on('click', function (ev) {
        $uploadCrop.croppie('result', 'canvas').then(function (resp) {
            popupResult({
                src: resp
            });
        });
        $(".crop").css("display","none");
    });

    function popupResult(result) {
        var html;
        if (result.html) {
            html = result.html;
        }
        if (result.src) {
            html = '<img id="img" src="' + result.src + '" />';
        }
        $("#result").html(html);
    }

