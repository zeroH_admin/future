
//全选
$("#selectAll").click(function () {
    $(".table input").prop("checked", true);
});
//全不选
// $("#unSelect").click(function () {
//     $(".table input").prop("checked", false);
// });
//反选
$("#reverse").click(function () {
    $(".table input:checkbox").each(function () {
        $(this).prop("checked", !$(this).prop("checked"));
    });
    allchk();
});

//设置全选复选框
$(".table input:checkbox").click(function(){
    allchk();
});

//获取选中选项的值
// $("#getValue").click(function(){
//     var valArr = new Array;
//     $("#list :checkbox[checked]").each(function(i){
//         valArr[i] = $(this).val();
//     });
//     var vals = valArr.join(',');
//     alert(vals);
// });

function allchk(){
    var chknum = $(".table input:checkbox").size();//选项总个数
    var chk = 0;
    $(".table input:checkbox").each(function () {
        if($(this).prop("checked")==true){
            chk++;
        }
    });
    if(chknum==chk){//全选
        $(".table input").prop("checked",true);
    }else{//不全选
        $(".table input").prop("checked",false);
    }
};
$(".del").click(function () {
    $(".input-check").show();
});
$("#cancel").click(function () {
    $(".input-check").hide();
})