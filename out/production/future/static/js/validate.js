
$(".name-required input,.abbreviation-required input,.address-required input").focus(function () {
    $(this).css("border","1px solid #2d61e6")
}).blur(function () {
    $(this).css("border","1px solid transparent");
    if($(this).val()==""){
        $(this).css("border","1px solid #ff0000");
        $(this).next().show();
    }else {
        $(this).next().hide();
    }
});
$(".website-required-input").focus(function () {
    $(this).css("border","1px solid #2d61e6")
}).blur(function () {
    $(this).css("border","1px solid transparent");
});
$(".textarea-box textarea").focus(function () {
    $(".textarea-box").css("border","1px solid #2d61e6")
}).blur(function () {
    $(".textarea-box").css("border","1px solid #d9d9d9");
    if($(".textarea-box textarea").val()==""){
        $(".textarea-box").css("border","1px solid #ff0000");
    }

});
//文本字数
$(".textarea-box textarea").keyup(function () {
    $(".max-num span").text($(".textarea-box textarea").val().length);
    if($(".textarea-box textarea").val().length==500){
        $(".max-num").css("color","#ff0000").animate({
            right:'25px'
        },100).animate({
            right:'15px'
        },100).animate({
            right:'20px'
        },100)
        $(".max-num span").css("color","#ff0000");
    }else {
        $(".max-num").css("color","#333");
        $(".max-num span").css("color","#333");
    };
});
$(".information input").keyup(function () {
    $(".input-max-num span").text($(".information input").val().length);
    if($(".information input").val().length==30){
        $(".input-max-num").css("color","#ff0000").animate({
            right:'25px'
        },100).animate({
            right:'15px'
        },100).animate({
            right:'20px'
        },100)
        $(".input-max-num span").css("color","#ff0000");
    }else {
        $(".input-max-num").css("color","#333");
        $(".input-max-num span").css("color","#333");
    };
});
//
var namerequired= $(".name-required input");
var abbreviation=$("abbreviation-required input");
var address=$(".address-required input")
var finance=$(".finance span");
var nature=$(".nature span");
var scale=$(".scale span");
// $(".next").focus(function () {
//     if(namerequired.val()==""){
//         namerequired.css("border","1px solid #ff0000");
//         namerequired.next().show();
//     }else if(abbreviation.val()==""){
//         abbreviation.css("border","1px solid #ff0000");
//         abbreviation.next().show();
//     }else if(finance.text()==""){
//         finance.parent().css("border","1px solid #ff0000");
//         finance.parent().siblings("p").show();
//     }else if(nature.text()==""){
//         nature.parent().css("border","1px solid #ff0000");
//         nature.parent().siblings("p").show();
//     }else if(scale.text()==""){
//         scale.parent().css("border","1px solid #ff0000");
//         scale.parent().siblings("p").show();
//     }else if(address.val()==""){
//         address.css("border","1px solid #ff0000");
//         address.next().show();
//     }else if($(".textarea-box textarea").val()==""){
//         $(".textarea-box").css("border","1px solid #ff0000");
//     }
// });
console.log(nature)
//身份选择
$(".finance,.scale").click(function () {
    $(this).find(".id-list").toggle();
    $(this).find("li").click(function () {
        var text=$(this).html();
        $(this).parent().siblings("span").text(text);
        $(this).parent().parent().css("border","1px solid transparent");
        $(this).parent().parent().next().hide();
    });
});
$(".nature").click(function () {
    $(this).find(".list-nature").toggle();
    $(this).find("li").click(function () {
        // var text=$(this).html();
        // $(this).parent().siblings("span").text(text);
        // $(this).parent().parent().css("border","1px solid transparent");
        // $(this).parent().parent().next().hide();
    });
})