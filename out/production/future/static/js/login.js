//选项卡
$(".register-type div").each(function () {
    var index=$(this).index();
    $(this).click(function () {
        $(".select-line div").removeClass("register-line-one").eq(index).addClass("register-line-one");
        $(".register-change form").hide().eq(index).show()
    })
});
//intern登陆验证
$(".register-phone input,.graphic-verification input,.register-pwd input").focus(function () {
    $(this).css("border","1px solid #2d61e6");
}).blur(function () {
    $(this).css("border","1px solid transparent");
})
//intern数据验证
var interninformation=$(".intern-information input");
var interninformationtext=$(".intern-information p");
interninformation.blur(function () {
    if(interninformation.val()==""){
        interninformationtext.show();
        interninformationtext.text("用户名不能为空");
        interninformation.css("border","1px solid #ff0000");
    }else if(!(interninformation.val().match(/^1[34578]\d{9}$/)||interninformation.val().match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))){
        interninformationtext.show();
        interninformation.css("border","1px solid #ff0000");
    }else{
        interninformationtext.hide();
    }
});
$(".intern-button").click(function () {
    $(this).css("background-color","#0d3fbe");
    if(interninformation.val()==""){
        interninformation.css("border","1px solid #ff0000");
        interninformationtext.show().text("用户名不能为空");
    }else if($("intern-pwd input").val()==""){
        $("intern-pwd input").css("border","1px solid #ff0000");
        $("intern-pwd p").show().text("密码不能为空");
    }else {
        $(".find-job").submit();
    }
});
//hr登录数据验证
var hrinformation=$(".hr-information input");
var hrinformationtext=$(".hr-information p");
hrinformation.blur(function () {
    if(hrinformation.val()==""){
        hrinformationtext.show();
        hrinformationtext.text("用户名不能为空");
        hrinformation.css("border","1px solid #ff0000");
    }else if(!(hrinformation.val().match(/^[1][3][0-9]{9}$/)||hrinformation.val().match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/))){
        hrinformationtext.show();
        hrinformation.css("border","1px solid #ff0000");
    }else{
        hrinformationtext.hide();
    }
});
//登录按钮
$(".hr-button").focus(function () {
    $(this).css("background-color","#0d3fbe");
    if(hrinformation.val()==""){
        hrinformation.css("border","1px solid #ff0000");
        hrinformationtext.show();
        hrinformationtext.text("用户名不能为空");
    }else if($("intern-pwd input").val()==""){
        $("intern-pwd input").css("border","1px solid #ff0000");
        $("intern-pwd p").show().text("密码不能为空");
    }else {
        $(".find-person").submit();
    }
});
