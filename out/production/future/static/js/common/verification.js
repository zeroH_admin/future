function shake(obj) {
    var $panel = $(obj);
    if($panel.parent().attr("class").slice(-9)=="id-select"){
        $panel.parent().css("border","1px solid #ff0000");
    }else {
        $panel.focus(function(){
            $(this).css("border","1px solid #2d61e6");
        }).css("border","1px solid #ff0000");
    }

}

var checkText = function(id) {
        if ($(id).val() == "" || $(id).val() == null) {
            shake(id);
            // $(id).val("").focus(function() { $(this).val(""); });
            return false;
        }
        return true;
    }
var checkPhone = function(id) {
    var myreg = /^1[34578]\d{9}$/;
    if (!myreg.test($(id).val())) {
        shake(id);
        $(id).val("").focus(function() { $(this).val(""); });
        return false;
    }
    return true;
}

/*
 * 检查邮箱是否为空和正确
 */
var checkEmail = function(id) {
    var email = $(id).val();
    if (email == "" || email == null) {
        shake(id);
        $(id).val("邮箱不能为空").focus(function() { $(this).val(""); });
        return false;
    } else {
        var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if (!myreg.test(email)) {
            shake(id);
            $(id).val("邮箱格式不正确").focus(function() { $(this).val(""); });
            return false;
        }
    }
    return true;
}

/*
 * 检查密码的长度至少为8位
 */
var checkPasswordLength = function(id) {
    var pwd = $(id).val();
    if (pwd == "" || pwd == null) {
        shake(id);
        $(id).val("密码为8-20位").focus(function() { $(this).val(""); });
      return false;
    } else if(pwd.length < 8 || pwd.length > 20) {
        shake(id);
        $(id).val("密码为8-20位").focus(function() { $(this).val(""); });
        return false;
    }
    return true;
}
/*
 * 检查是否同意我们的使用协议
 */
var checkAgree = function(id) {
    if(!$(id).is(':checked')) {
        alert("请同意我们的用户协议");
        return false;
    }
    return true;
}
