

//密码可见
$(".register-pwd div").click(function () {
    $(this).toggleClass("eye")
    if($(this).hasClass("eye")){
        $(".register-pwd input").attr("type","text");
    }else{
        $(".register-pwd input").attr("type","password");
    }
});
//intern 部分验证
//电话验证
var phoneflag=false;
var internphonetext=$(".intern-register-phone p");
var internphone=$(".intern-register-phone input");
internphone.blur(function () {
    if((!internphone.val().match(/^1[34578]\d{9}$/)&&(!internphone.val().match(/^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/)))) {
        internphonetext.show().text("用户名格式错误");
        internphone.css("border","1px solid #ff0000");
    }else{
        internphonetext.hide();
        $.get(
            "/register/checkUserName",
            {userName:internphone.val()},
            function(data,state){
                if(data==true){
                    internphone.css("border","1px solid #e6e6e6");
                    internphonetext.hide();
                }else {
                    internphone.css("border","1px solid #ff0000");
                    internphonetext.show().text("用户名已存在");
                    $("#register-save").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )
        phoneflag=true;
    }
});

//密码格式验证
var internpwdtext=$(".intern-register-pwd p")
var internpwd=$(".intern-register-pwd input")
internpwd.blur(function () {
    password(internpwd,internpwdtext);
});
//注册选中状态
// 刷新图片
function changeImg() {
    var imgSrc = $(".graphic");
    var src = imgSrc.attr("src");
    imgSrc.attr("src", changeUrl(src));
}
//为了使每次生成图片不一致，即不让浏览器读缓存，所以需要加上时间戳
function changeUrl(url) {
    var timestamp = (new Date()).valueOf();
    var index = url.indexOf("?",url);
    if (index > 0) {
        url = url.substring(0, url.indexOf("?"));
    }
    if ((url.indexOf("&") >= 0)) {
        url = url + "×tamp=" + timestamp;
    } else {
        url = url + "?timestamp=" + timestamp;
    }
    return url;
}
//图形验证
$("#verificationImg").blur(function () {
    var imgCode = $("#verificationImg").val();
    function getCookie(name)
    {
        var arr,reg=new RegExp("(^| )"+name+"=([^;]*)(;|$)");
        if(arr=document.cookie.match(reg))
            return unescape(arr[2]);
        else
            return null;
    }
    if(imgCode!==""){
        $.get(
            "/register/checkCode",
            {code:imgCode},
            function(data,state){
                if(data==true){
                    $("#verificationImg").css("border","1px solid #e6e6e6");
                    $("#verificationImg").parent().siblings("p").hide();
                }else {
                    $("#verificationImg").css("border","1px solid #ff0000");
                    $("#verificationImg").parent().siblings("p").show().text("验证码输入错误");
                    $("#register-save").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )
    }
});
//手机验证码
$("#intern-refresh-send").blur(function () {
    $.get(
        "/register/checkMobileCode",
        {
            userName:$("#register-name").val(),
            code:$("#intern-refresh-send").val()
        },
        function(data,state){
            if(!data){
                $("#intern-refresh-send").css("border","1px solid #ff0000");
                $("#intern-refresh-send").siblings("p").show().text("验证码输入错误");
            }else {
                $("#intern-refresh-send").css("border","1px solid #e6e6e6");
                $("#intern-refresh-send").siblings("p").hide();
            }
        }
    )
})

// button($(".intern-register-button"),$(".intern-graphic-verification"));

$(".intern-register-button").click(function () {
    $(this).css("background","#0d3fbe");
    var inputbox=$(this).parent().find($("input:not([type|=button]):not([type|=hidden])"));
    if($(".intern-graphic-verification").attr("hidden")!="hidden"){
        $(this).parent().find($("input:not([type|=button]):not([type|=hidden])")).not($(".intern-graphic-verification input")).each(function () {
            if($(this).val()==""){
                $(".intern-register-button").attr("disabled","true")
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                return true;
            }else {
                $("#register-save").click(function () {
                    $(".find-job").submit();
                })
            }
        })
    }else {
        inputbox.not($(this).parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                $(this).parent().siblings(".prompt").show().text("不能为空");
                return false;
            }else {
                $("#register-save").click(function () {
                    $(".find-job").submit();
                })
            }
        })
    }
});
//弹窗
$(".register-read-yes a").click(function(){
    $(".pop-agreement").show();
});
//所有弹窗关闭效果
$(".closeBtn").click(function(){
    $(this).parent(".close-trigger").hide();
});
//hr身份选择
$(".id-select").click(function () {
    $(".id-list").toggle()
});
$(".id-list li").click(function () {
    var text=$(this).html();
    $(".id-select span").text(text)
    $(".positionName").val(text)
});

//hr部分的验证
//邮箱验证

var hrphonetext=$(".hr-register-phone p");
var hrphone=$(".hr-register-phone input");
var hrflag=false;
hrphone.blur(function () {
    if(hrphone.val()!==""){
        $.get(
            "/register/checkUserName",
            {userName:hrphone.val()},
            function(data,state){
                if(data==true){
                    hrphone.css("border","1px solid #e6e6e6");
                    hrphonetext.hide();
                }else {
                    hrphone.css("border","1px solid #ff0000");
                    hrphonetext.show().text("用户名已存在");
                    $(".hr-register-button").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        )
    }
    email(hrphone,hrphonetext);
});
//身份选择
$(".id-select").blur(function(){
    if($(".id-select span")==""){
        $(".identity p").show();
    }else{
        $(".identity p").hide();
    }
});
//手机验证码
$("#hr-refresh-send").blur(function () {
    $.get(
        "/register/checkMobileCode",
        {
            userName:$("#hr-name").val(),
            code:$("#hr-refresh-send").val()
        },
        function(data,state){
            if(!data){
                $("#hr-name").css("border","1px solid #e6e6e6");
                $("#hr-name").siblings("p").show().text("验证码输入错误");
            }else {
                $("#hr-refresh-send").css("border","1px solid #ff0000");
                $("#hr-refresh-send").siblings("p").hide();
            }
        }
    )
})
//密码格式验证
var hrpwdtext=$(".hr-register-pwd p")
var hrpwd=$(".hr-register-pwd input")
hrpwd.blur(function () {
    password(hrpwd,hrpwdtext)
});
//图形验证
$(".hr-graphic-verification").blur(function () {
    var hrImgCode = $("#hr-verification").val();
    if(hrImgCode!==""){
        $.ajax({
            type:"get",
            url:"/register/checkCode",
            data:{code:hrImgCode},
            success:function(data,state){
                if(data==true){
                    $("#hr-verification").css("border","1px solid #e6e6e6");
                    $("#hr-verification").parent().siblings("p").hide();
                }else {
                    $("#hr-verification").css("border","1px solid #ff0000");
                    $("#hr-verification").parent().siblings("p").show().text("验证码输入错误");
                    $(".hr-register-button").attr("disabled",true);
                }
                //这里显示返回的状态
            }
        })
    }
});
//注册选中状态

$(".hr-register-button").click(function () {
    $(this).css("background","#0d3fbe");
    var inputbox=$(this).parent().find($("input:not([type|=button]):not([type|=hidden])"));
    if($(".hr-graphic-verification").attr("hidden")!="hidden"){
        $(this).parent().find($("input:not([type|=button]):not([type|=hidden])")).not($(".hr-graphic-verification input")).each(function () {
            if($(this).val()==""){
                $(".hr-register-button").attr("disabled","true")
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                return true;
            }else {
                $(".hr-register-button").click(function () {
                    $(".find-person").submit();
                })
            }
        })
    }else {
        inputbox.not($(this).parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings(".prompt").show().text("不能为空");
                $(this).parent().siblings(".prompt").show().text("不能为空");
                return false;
            }else {
                $(".hr-register-button").click(function () {
                    $(".find-person").submit();
                })
            }
        })
    }
});
//同意协议
$(".checked").click(function () {
    $(".img-checked").toggle();
    if($(".img-checked").css("display")=="none"){
        $(".register-button").attr("disabled",true);
    }else {
        $(".register-button").removeAttr("disabled");
    }
});
//选项卡
$(".register-type div").each(function () {
    var index=$(this).index();
    $(this).click(function () {
        $(".select-line div").removeClass("register-line-one").eq(index).addClass("register-line-one");
        $(".register-change form").hide().eq(index).show();
        hrflag=false;
        phoneflag=false;
    })
});

//hr




