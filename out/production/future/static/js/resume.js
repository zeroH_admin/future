//显示隐藏
$(".append").click(function () {
    var index = $(this).siblings("a").attr("title");
    var className = '.' + 'index-' + index;
    $(className).removeClass("dn");
});
$(".delete").click(function () {
    var index = $(this).siblings("a").attr("title");
    var className = '.' + 'index-' + index;
    $(className).addClass("dn")
});
$(".box").on('click','.compile',function() {
    alert(1)
});
$(".add").click(function () {
    $(this).siblings(".id-list").toggle();
});
$(".addition").click(function () {
    var index=$(this).parents(".parent").attr("class").slice(-1);
    $(".classify a").each(function (i) {
        if(i==index){
            $(this).children("span").css("color","#2d61e6");
            $(this).children("i").removeClass("dn");
            return false;
        }else {
            $(this).children("span").css("color","#333");
            $(this).children("i").addClass("dn");
        }
    })
});
$("input:not([type|=button])").focus(function () {
    if($(this).parent().attr("class").slice(-9)=="id-select"){
        // $(this).parent().css("border","1px solid #2d61e6");
    }else {
        $(this).css("border","1px solid #2d61e6");
    }
}).blur(function () {
    if($(this).parent().attr("class").slice(-9)=="id-select"){
        if($(this).val()==""){
            // $(this).parent().css("border","1px solid #ff0000");
            // $(this).parent().siblings("p").show().text("不能为空");
        }else {
            $(this).parent().css("border","1px solid #e6e6e6");
            $(this).parent().siblings("p").hide();
        }
    }else {
        if($(this).val()==""){
            $(this).css("border","1px solid #ff0000");
            $(this).siblings("p").show().text("不能为空");
        }else {
            $(this).css("border","1px solid #e6e6e6");
            $(this).siblings("p").hide();
        }
    }
});
//跳转
$(".classify li a").click(function() {
    $(this).parent().siblings().find("span").css("color","#333");
    $(this).find("span").css("color","#2d61e6");
    $(this).parent().siblings().find("i").addClass("dn");
    $(this).find("i").removeClass("dn");
    var index = this.title;
    var id = '.' + 'index-' + index;
    $("html,body").animate({
        scrollTop: $(id).offset().top
    });
});

var resume= angular.module('resume', []);

resume.controller('resumeCtrl', function($scope,$http,$window){
    $http({
        url:'../json/nation.json',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.nationCandidate = result.data;
    });
    $scope.min='';
    $scope.max='';
    $scope.info={
        id:'',
        name:'',
        sex:'',
        birthday:'',
        mobilePhone:'',
        email:'',
        nationality:'',
        address:'',
        education:'',
        advantage:'',
        description:'',
        cardType:'',
        idcard:'',
        qq:'',
        wechat:'',
        educationJson:'',
        workExperienceJson:'',
        schoolExperienceJson:'',
        skillJson:'',
        languageJson:'',
        certificateJson:'',
        expectWorkJson:''
    };
    //  $scope.educationList='';
    $scope.workExperienceList=[{
        companyName:'',
        positionName:'',
        workDateStart:'',
        workDateEnd:'',
        workContent:''
    }];
    $scope.schoolExperienceList=[{
        itemName:'',
        itemAddress:'',
        schoolDateStart:'',
        schoolDateEnd:'',
        itemContent:''
    }];
    $scope.skillList=[{
        name:'',
        level:''
    }];
    $scope.languageList=[{
        name:'',
        level:''
    }];
    $scope.certificateList=[{
        name:'',
        date:''
    }];

    $scope.open=function () {
        $(".open").toggleClass("dn")
    };
    $http({
        url:'/intern/getAccountResume',
        method:'GET'
    }).then(function (result) {  //正确请求成功时处理
        $scope.info.id = result.data==null?'':result.data.id;
        if($scope.info.id==""){
            $http({
                url:'/intern/getUserInfo',
                method:'GET'
            }).then(function (result) {  //正确请求成功时处理
                $scope.info={
                    name:result.data.name,
                    sex:result.data.sex,
                    birthday:result.data.birthday,
                    mobilePhone:result.data.mobilePhone,
                    email:result.data.email,
                    nationality:result.data.nationality,
                    address:result.data.address,
                    education:result.data.education,
                    advantage:result.data.advantage,
                    description:result.data.description,
                    cardType:'',
                    idcard:'',
                    qq:'',
                    wechat:''
                }
                $scope.educationList = [{
                    school:result.data.school,
                    major:result.data.major,
                    level:result.data.education,
                    eduDateStart:result.data.entranceDate,
                    eduDateEnd:result.data.graduationDate
                }];
                $scope.expectWorkList =[{
                    expectPosition:result.data.expectPosition,
                    expectIndustry:result.data.expectIndustry,
                    availableTime:result.data.internTime,
                    expectedPeriod:result.data.internPeriod,
                    expectedSalary:result.data.expectSalary
                }];
                $scope.country=$scope.info.address.split(/市|省|自治区/)[0];
                $scope.c_city=$scope.info.address.split(/市|省|自治区/)[1];
                $scope.min=$scope.expectWorkList[0].expectedSalary.split("-")[0]
                $scope.max=$scope.expectWorkList[0].expectedSalary.split("-")[1]
            });
        }else {
            $http({
                url:'/intern/getAccountResume',
                method:'GET'
            }).then(function (result) {  //正确请求成功时处理
                $scope.info = result.data;
                $scope.educationList = JSON.parse(result.data.educationJson);
                $scope.workExperienceList = result.data.workExperienceJson==undefined?[]:JSON.parse(result.data.workExperienceJson);
                $scope.schoolExperienceList = result.data.schoolExperienceJson==undefined?[]:JSON.parse(result.data.schoolExperienceJson);
                $scope.skillList = result.data.skillJson==undefined?[]:JSON.parse(result.data.skillJson);
                $scope.languageList = result.data.languageJson==undefined?[]:JSON.parse(result.data.languageJson);
                $scope.certificateList = result.data.certificateJson==undefined?[]:JSON.parse(result.data.certificateJson);
                $scope.expectWorkList = JSON.parse(result.data.expectWorkJson);
                $(".show").removeClass("dn")
                $scope.country=$scope.info.address.split(/市|省|自治区/)[0]
                $scope.c_city=$scope.info.address.split(/市|省|自治区/)[1];
                $scope.min=$scope.expectWorkList[0].expectedSalary.split("-")[0]
                $scope.max=$scope.expectWorkList[0].expectedSalary.split("-")[1]
            });
        }

    });

    //个人信息
    //国际
    $scope.f=function (data,event) {
        $scope.info.nationality=data;
        $(event.target).parent().hide();
        $(event.target).parent().parent().css("border","1px solid  #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    };
    //编辑
    $scope.compile=function (event) {
        $(event.target).parent().siblings("form").removeClass("dn");
        $(event.target).parent().addClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    //
    //编辑
    $scope.edit=function (event,index) {
        $scope.setAddIndex=index;
        $scope.isAdded = false;
        $(event.target).parent().parent().siblings("form").removeClass("dn");
        $(event.target).parent().parent().addClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.informationCancel=function () {
        $(".edu").addClass("dn");
        $(".information-top").removeClass("dn");
        $(".addition,.compile").removeAttr("disabled");
    };
    $scope.save=function () {
        alert(1)
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);
        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/intern/saveResume',
            data:$.param($scope.info)
        }).then(function successCallback(response) {
            $(".eduExpForm").addClass("dn");
            $(".information-top").removeClass("dn");
            $(".addition,.compile").removeAttr("disabled");
            $window.location.reload();

        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存

        });
    };
    //居住地
    $scope.country= $scope.info.address;
    $scope.c_city='';
    $scope.pro=[];
    $scope.Tline=function () {
        $http({
            url:'/json/province.json',
            method:'GET'
        }).then(function (result) {  //正确请求成功时处理
            $scope.line = result.data;
        });
    }
    $scope.lineClick=function (data,event) {
        $scope.country=data;
        $(event.target).parent().hide();
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+$scope.c_city;
    }
    $scope.Nline=function (data) {
        var arr=[];
        var rel=[];
        $http({
            url:'/json/province.json',
            method:'GET',
        }).then(function (result) {  //正确请求成功时处理
            angular.forEach(result.data, function(d,index,array){//data等价于array[index]
                if(d.province==data){
                    arr.push(d.citys)
                }
            });
            angular.forEach(arr, function(d,index,array){//data等价于array[index]
                rel=d;
            });
            $scope.con = rel;
        });
    }
    $scope.NameClick=function (data,event) {
        $scope.c_city=data;
        $(event.target).parent().hide();
        $(event.target).parent().parent().css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
        $scope.info.address=$scope.country+$scope.c_city;
    };
    //期望工作编辑
    $scope.expectEdit=function () {
        $(".expectEdit").removeClass("dn");
        $(".expectShow").addClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };
    $scope.expectCancel=function () {
        $(".expectEdit").addClass("dn");
        $(".expectShow").removeClass("dn");
        $(".addition,.compile").removeAttr("disabled");
    };
    $scope.expectSave=function () {
        $(".expectEdit").addClass("dn");
        $(".addition,.compile").removeAttr("disabled");
        $(".expectShow").removeClass("dn");

    };

    //添加
    $scope.addition=function (event,index) {
        $scope.setAddIndex=index;
        $scope.isAdded = true;
        $(event.target).parent().siblings().children(".eduExpForm").removeClass("dn");
        $(".addition,.compile").attr("disabled",true);
    };

    //删除
    $scope.delete=function (event,box,data) {
        data.splice($scope.setAddIndex,1);
        box=data;
        $scope.info.educationJson = JSON.stringify($scope.educationList);
        $scope.info.workExperienceJson = JSON.stringify($scope.workExperienceList);
        $scope.info.schoolExperienceJson = JSON.stringify($scope.schoolExperienceList);
        $scope.info.skillJson = JSON.stringify($scope.skillList);
        $scope.info.languageJson = JSON.stringify($scope.languageList);
        $scope.info.certificateJson = JSON.stringify($scope.certificateList);
        $scope.info.expectWorkJson = JSON.stringify($scope.expectWorkList);
        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/intern/saveResume',
            data:$.param($scope.info)
        }).then(function successCallback(response) {
            // 请求成功执行代码
            alert("保存成功");
            $(event.target).parents("form").addClass("dn");
            $(".addition,.compile").removeAttr("disabled");
            $(event.target).parents("form").siblings().removeClass("dn");
            $window.location.reload();

        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存

        });
    };


    //取消按钮
    $scope.cancel=function (event,data) {
        if($scope.isAdded){
            if (data.length == $scope.setAddIndex + 1) {
                data.pop();
            }
        }
        $(event.target).parent().parent().addClass("dn");
        $(event.target).parent().parent().siblings().removeClass("dn");
        $(".addition,.compile").removeAttr("disabled");
    };
});