$(".job .down,.cycle .down,.opportunity .down,.track-line .down,.track-name .down").click(function () {
    $(this).siblings(".select").toggle();
    if($(this).siblings(".select").css("display")=="block"){
        $(this).siblings("input").focus();
    }else {
        $(this).siblings("input").blur();
    }

});
$(".select li").click(function () {
    $(this).parent().siblings("input").val($(this).text());
    $(this).parent().toggle();
    $(this).parent().siblings("input").blur();
});
function button(eve) {
    eve.click(function () {
        eve.css("background","#0d3fbe");
        var inputbox=eve.parent().find($("input:not([type|=button]):not([type|=hidden])"));
        inputbox.not(eve.parent().find($("input:not([type|=button]):not([type|=hidden])"))).prevObject.each(function () {
            if($(this).val()==""){
                $(this).css("border","1px solid #ff0000");
                $(this).siblings("p").show().text("不能为空");
                $(this).parent().siblings("p").show().text("不能为空");
                return false;
            }else {

            }
        })
    });
}
button($(".button"));
//勾选
$(".checked").click(function () {
    $(".img-checked").toggle();
    if($(".img-checked").css("display")=="none"){
        $(".button").attr("disabled",true);
    }else {
        $(".button").removeAttr("disabled");
    }
});
var release=angular.module("release",[]);
release.controller('releaseCtrl',function ($scope,$http) {
    $scope.salary_min='';
    $scope.day_min='';
    $scope.time_min='';
    $scope.salary_max='';
    $scope.day_max='';
    $scope.time_max='';
    $scope.trackLine='';
    $scope.trackName='';
    $scope.job={
        name:'',
        type:'',
        property:'',
        experience:'',
        eduRequest:'',
        salary:'',
        internDaySalary:'',
        workTimes:'',
        internPeriod:'',
        regular:'',
        jobDescription:'',
        jobRequest:'',
        jobAddress:'',
        nearSubway:'',
        contact:'',
        mobilePhone:''
    }
    $scope.Tline=function () {
        $http({
            url:'/json/metroLine.json',
            method:'GET'
        }).then(function (result) {  //正确请求成功时处理
            $scope.line = result.data;
        });
    }
    $scope.lineClick=function (data,event) {
        $scope.trackLine=data;
        $scope.job.nearSubway=$scope.trackLine+$scope.trackName;
        $(event.target).parent().hide();
        $(event.target).parent().siblings('input').css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    }
    $scope.Nline=function (data) {
        var arr=[];
        var rel=[];
        $http({
            url:'/json/metroLine.json',
            method:'GET',
        }).then(function (result) {  //正确请求成功时处理
            angular.forEach(result.data, function(d,index,array){//data等价于array[index]
                console.log(d.line==data)
                if(d.line==data){
                    arr.push(d.station)
                }
            });
            angular.forEach(arr, function(d,index,array){//data等价于array[index]
                rel=d;
            });
            $scope.con = rel;
        });
    }
    $scope.NameClick=function (data,event) {
        $scope.trackName=data;
        $scope.job.nearSubway=$scope.trackLine+$scope.trackName;
        $(event.target).parent().hide();
        $(event.target).parent().siblings('input').css("border","1px solid #e6e6e6");
        $(event.target).parent().parent().siblings("p").hide();
    }
    $scope.tel=function (event) {
        $http.get(
            "http://localhost:8081/register/checkUserName",
            {userName:$scope.job.mobilePhone},
            function(data,state){
                if(data==true){
                    $(event.target).css("border","1px solid #ff0000");
                    $(event.target).siblings("p").show().text("用户名不存在");
                }else {
                    $(event.target).css("border","1px solid #e6e6e6");
                    $(event.target).siblings("p").hide();
                }
                //这里显示返回的状态
            }
        )
    }

    $scope.submit=function () {
        $http({
            method: 'POST',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: '/hr/saveJob',
            data:$.param($scope.job)
        }).then(function successCallback(response) {
            // 请求成功执行代码
            alert("保存成功");
            //跳转到公司首页
        }, function errorCallback(response) {
            // 请求失败执行代码
            alert("保存失败");
            //刷新本页面重新保存
        });
    }
    var watch = $scope.$watch('job.property',function(newValue,oldValue, scope){
        if (newValue=="实习"){
            $(".internPeriod,.workTimes,.internDaySalary").show();
            $(".salary").hide();
        }else if(newValue=="全职"){
            $(".salary").show();
            $(".internPeriod,.workTimes,.internDaySalary").hide();
        }
    });

})

